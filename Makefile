#Include directory for header files that come with the DOPE-AMINE
IDIR =./include
BIN_DIR=./bin
SRC_DIR=./src

#directory for .o files
ODIR=./obj

#compilers to use
CC=gcc
CPP=g++

# define any compile-time flags
CFLAGS = -O2 -I$(IDIR) -Wall -g

# define any directories containing header files other than /usr/include
#
INCLUDES = -I/usr/include/hdf5/serial


# define library paths in addition to /usr/lib
#   if I wanted to include libraries not in /usr/lib I'd specify
#   their path using -Lpath, something like:
LFLAGS = -L/usr/lib/x86_64-linux-gnu/hdf5/serial -L/usr/local/lib


# define any libraries to link into executable:
#   if I want to link in libraries (libx.so or libx.a) I use the -llibname 
#   option, something like (this will link in libmylib.so and libm.so:
LIBS = -lhdf5 -lCGAL -lgmp -llapack -lblas -lm


_DEPS = DA_Diff_IO_utility.h DA_Diff_Solver_fct_shared.h DA_Diff_Solver_fct_1_comp.h DA_Diff_Solver_fct_mult_comp.h DA_Diff_alloc_shared.h #DA_Diff_macros_struct.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_DEPS_CPP = TreeNode.h BinTree.h Make_Axon_utility_fcts.h
DEPS_CPP = $(patsubst %,$(IDIR)/%,$(_DEPS_CPP))

_OBJ = DA_Diff_IO_utility.o DA_Diff_Rea_3D_MM_Bio_synapse_hdf5_suite.o DA_Diff_Rea_3D_MM_Bio_synapse_hdf5_suite_1_comp.o DA_Diff_Solver_fct_shared.o DA_Diff_Solver_fct_1_comp.o DA_Diff_Solver_fct_mult_comp.o DA_Diff_alloc_shared.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

_OBJ_CPP = TreeNode.o BinTree.o Make_Axon_utility_fcts.o
OBJ_CPP = $(patsubst %,$(ODIR)/%,$(_OBJ_CPP))



$(ODIR)/%.o: $(SRC_DIR)/%.c $(DEPS)
	@mkdir -p $(@D)
	$(CC) -c -o $@ $< $(CFLAGS) $(INCLUDES)

$(ODIR)/%.o: $(SRC_DIR)/%.cpp $(DEPS_CPP)
	@mkdir -p $(@D)
	$(CPP) -c -o $@ $< $(CFLAGS) $(INCLUDES)



DA_Diff: $(OBJ) $(OBJ_CPP) $(SRC_DIR)/Make_Axons_uptake_dist_suite_v3.cpp
	@mkdir -p $(BIN_DIR)
	$(CC) -o $(BIN_DIR)/DA_diff $(ODIR)/DA_Diff_Rea_3D_MM_Bio_synapse_hdf5_suite.o $(ODIR)/DA_Diff_IO_utility.o $(ODIR)/DA_Diff_Solver_fct_shared.o $(ODIR)/DA_Diff_Solver_fct_mult_comp.o $(ODIR)/DA_Diff_alloc_shared.o $(CFLAGS) $(LFLAGS) $(LIBS)
	$(CC) -o $(BIN_DIR)/DA_diff_1 $(ODIR)/DA_Diff_Rea_3D_MM_Bio_synapse_hdf5_suite_1_comp.o $(ODIR)/DA_Diff_IO_utility.o $(ODIR)/DA_Diff_Solver_fct_shared.o $(ODIR)/DA_Diff_Solver_fct_1_comp.o $(ODIR)/DA_Diff_alloc_shared.o $(CFLAGS) $(LFLAGS) $(LIBS)
	$(CPP) $(CFLAGS) $(INCLUDES) -o $(BIN_DIR)/Make_axon_dist.exe $(OBJ_CPP)  $(SRC_DIR)/Make_Axons_uptake_dist_suite_v3.cpp $(LFLAGS) $(LIBS)

.PHONY: clean print_vars install directories

clean:
	rm -f $(BIN_DIR)/* $(ODIR)/*.o *~ core $(INCDIR)/*~ 

print_vars:
	echo $(OBJ)
	
	





	
