/*
 * 
 * 
 * This file DA_Diff_Solver_fct_shared.c is the file containing all functions that are used by both the 1 component and the multi-component
 * solver used by the diffusion code DOPE-AMINE.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */

#include "DA_Diff_macros_structs_shared.h"
#include "DA_Diff_Solver_fct_shared.h"
#include <math.h>
#include <stdlib.h>


int Calc_stencil(int N_comp, int Nx_calc, int Ny_calc, int Nz_calc,double **** Conc_now, double **** RHS_1,struct Diff_coeff * Dcoeff){
        /* Calculates the RHS of equation 38 by using the Conc_array. The RHS is written into the RHS_1 array */
        int i,j,k,n,ix,iy,iz;
        double dz,dy,dx,dxy,dyz,dxz,dxyz;
        
        double* ax = (Dcoeff->ax);
        double* ay = (Dcoeff->ay);
        double* az = (Dcoeff->az);
        double* axy = (Dcoeff->axy);
        double* ayz = (Dcoeff->ayz);
        double* axz = (Dcoeff->axz);
        double* axyz = (Dcoeff->axyz);
        
        for(n=0;n<N_comp;n++){
            for(i=0;i<Nx_calc;i++){   //This assumes that the Conc_array is already wrapped around ... meaning that (Conc[0] has Conc[Nx-1]) and (Conc[Nx] has Conc[1]) -> So to say Conc has to be periodic somewhat 
                for(j=0;j<Ny_calc;j++){
                    for(k=0;k<Nz_calc;k++){
                       //LAPACK could solve the system slap wise, if this is too slow... think about it... will be more complicated with RHS and such
                        //the conc arrays are shifted by 1 with respect to the RHS arrays... so in acessing them every index has to get a +1 Conc[n][1]... == RHS[n][0]...
                        ix=i+1; 
                        iy=j+1;
                        iz=k+1;
                        dz=Conc_now[n][ix][iy][iz-1]-2.0*Conc_now[n][ix][iy][iz]+Conc_now[n][ix][iy][iz+1];
                        dy=Conc_now[n][ix][iy-1][iz]-2.0*Conc_now[n][ix][iy][iz]+Conc_now[n][ix][iy+1][iz];
                        dx=Conc_now[n][ix-1][iy][iz]-2.0*Conc_now[n][ix][iy][iz]+Conc_now[n][ix+1][iy][iz];
                        
                        dxy=4.0*Conc_now[n][ix][iy][iz]-2.0*(Conc_now[n][ix-1][iy][iz]+Conc_now[n][ix+1][iy][iz]+Conc_now[n][ix][iy-1][iz]+Conc_now[n][ix][iy+1][iz])+Conc_now[n][ix+1][iy+1][iz]+Conc_now[n][ix-1][iy-1][iz]+Conc_now[n][ix+1][iy-1][iz]+Conc_now[n][ix-1][iy+1][iz];
                        dyz=4.0*Conc_now[n][ix][iy][iz]-2.0*(Conc_now[n][ix][iy-1][iz]+Conc_now[n][ix][iy+1][iz]+Conc_now[n][ix][iy][iz-1]+Conc_now[n][ix][iy][iz+1])+Conc_now[n][ix][iy+1][iz+1]+Conc_now[n][ix][iy-1][iz-1]+Conc_now[n][ix][iy-1][iz+1]+Conc_now[n][ix][iy+1][iz-1];
                        dxz=4.0*Conc_now[n][ix][iy][iz]-2.0*(Conc_now[n][ix+1][iy][iz]+Conc_now[n][ix-1][iy][iz]+Conc_now[n][ix][iy][iz-1]+Conc_now[n][ix][iy][iz+1])+Conc_now[n][ix+1][iy][iz+1]+Conc_now[n][ix-1][iy][iz-1]+Conc_now[n][ix-1][iy][iz+1]+Conc_now[n][ix+1][iy][iz-1];
                        
                        dxyz=-8.0*Conc_now[n][ix][iy][iz]+4.0*(Conc_now[n][ix+1][iy][iz]+Conc_now[n][ix-1][iy][iz]+Conc_now[n][ix][iy+1][iz]+Conc_now[n][ix][iy-1][iz]+Conc_now[n][ix][iy][iz+1]+Conc_now[n][ix][iy][iz-1])-2.0*(Conc_now[n][ix-1][iy-1][iz]+Conc_now[n][ix+1][iy+1][iz]+Conc_now[n][ix+1][iy-1][iz]+Conc_now[n][ix-1][iy+1][iz]+Conc_now[n][ix-1][iy][iz-1]+Conc_now[n][ix+1][iy][iz+1]+Conc_now[n][ix+1][iy][iz-1]+Conc_now[n][ix-1][iy][iz+1]+Conc_now[n][ix][iy-1][iz-1]+Conc_now[n][ix][iy+1][iz+1]+Conc_now[n][ix][iy-1][iz+1]+Conc_now[n][ix][iy+1][iz-1])+Conc_now[n][ix-1][iy-1][iz-1]+Conc_now[n][ix+1][iy+1][iz+1]+Conc_now[n][ix+1][iy-1][iz-1]+Conc_now[n][ix-1][iy+1][iz-1]+Conc_now[n][ix-1][iy-1][iz+1]+Conc_now[n][ix-1][iy+1][iz+1]+Conc_now[n][ix+1][iy-1][iz+1]+Conc_now[n][ix+1][iy+1][iz-1];
                        
                        RHS_1[n][i][j][k]=Conc_now[n][ix][iy][iz]+az[n]*(dz)+ay[n]*dy+ax[n]*dx+ayz[n]*dyz+axy[n]*dxy+axz[n]*dxz+axyz[n]*dxyz;
                        
                        
                        }
                    }
                }
           }
    return 0;
}



int Add_reaction_to_RHS(int N_comp,int Nx_calc,int Ny_calc,int Nz_calc,double **** RHS_1,double ****fct_grid,struct Diff_coeff * f_coeff,double dt){
    int i,j,k,n;
    int ix,iy,iz;
    
    double dz,dy,dx,dxy,dyz,dxz,dxyz;
        
    double* fx = (f_coeff->ax);
    double* fy = (f_coeff->ay);
    double* fz = (f_coeff->az);
    double* fxy = (f_coeff->axy);
    double* fyz = (f_coeff->ayz);
    double* fxz = (f_coeff->axz);
    double* fxyz = (f_coeff->axyz);
        
        
        
    for(n=0;n<N_comp;n++){
		
        for(i=0;i<Nx_calc;i++){ //since Conc is periodic the fct_grid should also be periodic .... so with the same structure as the Calc_stencil function this fills the array correctly for periodic boundaries
            for(j=0;j<Ny_calc;j++){
                for(k=0;k<Nz_calc;k++){
                        //the fct_grid arrays are shifted by 1 with respect to the RHS arrays... so in acessing them every index has to get a +1 fct[n][1]... == RHS[n][0]...
                        ix=i+1; 
                        iy=j+1;
                        iz=k+1;
                        dz=fct_grid[n][ix][iy][iz-1]-2.0*fct_grid[n][ix][iy][iz]+fct_grid[n][ix][iy][iz+1];
                        dy=fct_grid[n][ix][iy-1][iz]-2.0*fct_grid[n][ix][iy][iz]+fct_grid[n][ix][iy+1][iz];
                        dx=fct_grid[n][ix-1][iy][iz]-2.0*fct_grid[n][ix][iy][iz]+fct_grid[n][ix+1][iy][iz];
                        
                        dxy=4.0*fct_grid[n][ix][iy][iz]-2.0*(fct_grid[n][ix-1][iy][iz]+fct_grid[n][ix+1][iy][iz]+fct_grid[n][ix][iy-1][iz]+fct_grid[n][ix][iy+1][iz])+fct_grid[n][ix+1][iy+1][iz]+fct_grid[n][ix-1][iy-1][iz]+fct_grid[n][ix+1][iy-1][iz]+fct_grid[n][ix-1][iy+1][iz];
                        dyz=4.0*fct_grid[n][ix][iy][iz]-2.0*(fct_grid[n][ix][iy-1][iz]+fct_grid[n][ix][iy+1][iz]+fct_grid[n][ix][iy][iz-1]+fct_grid[n][ix][iy][iz+1])+fct_grid[n][ix][iy+1][iz+1]+fct_grid[n][ix][iy-1][iz-1]+fct_grid[n][ix][iy-1][iz+1]+fct_grid[n][ix][iy+1][iz-1];
                        dxz=4.0*fct_grid[n][ix][iy][iz]-2.0*(fct_grid[n][ix+1][iy][iz]+fct_grid[n][ix-1][iy][iz]+fct_grid[n][ix][iy][iz-1]+fct_grid[n][ix][iy][iz+1])+fct_grid[n][ix+1][iy][iz+1]+fct_grid[n][ix-1][iy][iz-1]+fct_grid[n][ix-1][iy][iz+1]+fct_grid[n][ix+1][iy][iz-1];
                        
                        dxyz=-8.0*fct_grid[n][ix][iy][iz]+4.0*(fct_grid[n][ix+1][iy][iz]+fct_grid[n][ix-1][iy][iz]+fct_grid[n][ix][iy+1][iz]+fct_grid[n][ix][iy-1][iz]+fct_grid[n][ix][iy][iz+1]+fct_grid[n][ix][iy][iz-1])-2.0*(fct_grid[n][ix-1][iy-1][iz]+fct_grid[n][ix+1][iy+1][iz]+fct_grid[n][ix+1][iy-1][iz]+fct_grid[n][ix-1][iy+1][iz]+fct_grid[n][ix-1][iy][iz-1]+fct_grid[n][ix+1][iy][iz+1]+fct_grid[n][ix+1][iy][iz-1]+fct_grid[n][ix-1][iy][iz+1]+fct_grid[n][ix][iy-1][iz-1]+fct_grid[n][ix][iy+1][iz+1]+fct_grid[n][ix][iy-1][iz+1]+fct_grid[n][ix][iy+1][iz-1])+fct_grid[n][ix-1][iy-1][iz-1]+fct_grid[n][ix+1][iy+1][iz+1]+fct_grid[n][ix+1][iy-1][iz-1]+fct_grid[n][ix-1][iy+1][iz-1]+fct_grid[n][ix-1][iy-1][iz+1]+fct_grid[n][ix-1][iy+1][iz+1]+fct_grid[n][ix+1][iy-1][iz+1]+fct_grid[n][ix+1][iy+1][iz-1];
                        
                        RHS_1[n][i][j][k]+=dt*0.5*(fct_grid[n][ix][iy][iz]+fz[n]*dz+fy[n]*dy+fx[n]*dx+fyz[n]*dyz+fxz[n]*dxz+fxy[n]*dxy+fxyz[n]*dxyz);
                        
                        
                        }
                    }
                }
            }
        return 0;
}


double Box_Mueller_1_nr(){ /* returns a normally distributed number between 0 and 1 */
		double a,temp1,temp2;

		temp1=drand48();
		temp2=drand48();	
		
		a= sqrt((-2)*log(temp1))*cos(2*M_PI*temp2);
		//b= sqrt((-2)*log(temp1))*sin(2*PI*temp2);
		return a;

}


int Fill_Diff_Mat(double ** Diff_Mat,int N, double r,double h)
{
    int i;
    double a,b;
    
    a=1.0-(1.0/(6.0))+(r);
    b=(1.0/(12.0))-((r)/(2.0));
    
    //Fill the Matrix with its tridiagonal entries!
    //First fill Main Diagonal
    for(i=0;i<N;i++){     
        Diff_Mat[DIAG][i]=a;
        }
    
    for(i=0;i<N-1;i++){ //Fill Side Diagonals     
        Diff_Mat[DIAG_L][i]=b;
        Diff_Mat[DIAG_U][i]=b;
    }
                
    return 0;
}
    
    



double PNext(double Rate){
    return -logf(1.0 - drand48()) / Rate;   //with rate in 1/second
}

/* For the release there should be an empty placeholder function*/
double PPF_NAC_VM_factor(double ISI_since_last_spike){
    return 1.0+2.0*exp(-(ISI_since_last_spike/50.0))-0.9*exp(-(ISI_since_last_spike/5000.0));
    /*PPF NAC VM with hardcoded t_PPF1=50.0 and T_PPF_PPD1=5000.0*/
}

double PPF_PUT_DL_factor(double ISI_since_last_spike){
    return 1.0+0.25*exp(-(ISI_since_last_spike/50.0))-1.0*exp(-(ISI_since_last_spike/5000.0));
    /*PPF Put dl with hardcoded t_PPF1=50.0 and T_PPF_PPD1=5000.0*/
}
