/*
 * 
 * 
 * This file TreeNode.cpp is the file containing the implmentation for the TreeNode class. 
 * The TreeNode class is basically just a container.
 * The TreeNode class is used to save TreeNodes that are used by the BinTree class. 
 * BinTree is used by the Make_Axons code to create synthethical axon geometries that are used by the diffusion code DOPE-AMIN
 * to calculate spatially varying uptake and synapse positions. However the BinTree and TreeNode class can also be used independently 
 * of the Diffusion code.  
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */



#include <vector>
#include "TreeNode.h"




// Constructor for TreeNode
TreeNode::TreeNode(int par, int id,int sib)
{
    strahl_segment_not_counted=true;	//for the strahl_level counter
    parent=par;
    my_id=id;
    sibling=sib;
    strahl_level=-1;
    level=-1;
    isleave=false;
    length_nr=1;
    length=1;
    children[0]=-1;
    children[1]=-1;
    branching_angle[0]=-1000;  //filled with a nonsensical value until it is filled for real
    branching_angle[1]=-1000;
    branch_turning_angle=-1000;
    
    
}
