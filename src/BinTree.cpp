/*
 * 
 * 
 * This file BinTree.h is the header file containing the implementation for the BinTree class.  
 * BinTree is used by the Make_Axons code to create synthethical axon geometries and to 
 * calculate spatially varying uptake and synapse positions. This uptake and synapse positions can then be used by the diffusion code
 * DOPE-AMIN, however the BinTree and TreeNode class can also be used independently 
 * of the Diffusion code.  
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */
 
 

#include "BinTree.h"
#include "Make_Axon_utility_fcts.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <math.h>


 
 
int BinTree::addnode(int par, int id,int sib)
{   //creates a Treenode with par,id and sibling and adds it to the end of the nodes vector of the bianry tree
    
    TreeNode* node=new TreeNode(par,id,sib);
    nodes.push_back(node);
    
    return 0;
}


int BinTree::Calc_levels(){
    std::vector<int> descending_nodes,descending_nodes_new;
    int level;
    unsigned int i;
    
    descending_nodes.push_back(0);
    level=0;
    
    while(descending_nodes.size()>0){ //terminate climbing down the tree when there are no more child nodes to be reached
        descending_nodes_new.clear();
        for (i = 0 ; i<descending_nodes.size(); i++){
            nodes[descending_nodes[i]]->level=level;
            if (nodes[descending_nodes[i]]->children[0]!=-1){   //If the node has children (children[]!=-1) then add the children to the descending nodes
                descending_nodes_new.push_back(nodes[descending_nodes[i]]->children[0]);
                descending_nodes_new.push_back(nodes[descending_nodes[i]]->children[1]);
                
                }
            else nodes[descending_nodes[i]]->isleave=true;
            }
            level++;
            descending_nodes=descending_nodes_new;
    }
    height=level;
    return 0;
}
    
int BinTree::Calc_strahl(){
    //This calculates the strahl level for each node, this assumes that all the nodes already got their isleave parameter set to true if they are a leave ... this is done in Calc_levels which should preclude this routine
    std::vector<int>::iterator endLocation;
    std::vector<int> ascending_nodes,ascending_nodes_new;
    int child_id1,child_id2;
    unsigned int i;
 
    
    for (i = 0 ; i<nodes.size(); i++){  //write all the leave node id's into the beginning ascending nodes list
        if(nodes[i]->isleave){
            ascending_nodes.push_back(nodes[i]->parent);    //put all the parent nodes of the leave nodes into the ascending list (since we already visited the leaves)
            nodes[i]->strahl_level=1;
            
            }
        if(nodes[i]->length > max_length){
            max_length=nodes[i]->length;
            }
            
    }

    
    

    //remove duplicates from ascending nodes vector
    std::sort(ascending_nodes.begin(),ascending_nodes.end());       
    endLocation=std::unique(ascending_nodes.begin(),ascending_nodes.end());
    ascending_nodes.erase(endLocation, ascending_nodes.end());
    
    while(ascending_nodes.size()>0){ //Terminate ascending the tree when we reached the root node
        ascending_nodes_new.clear();

        for (i = 0 ; i<ascending_nodes.size(); i++){
            //extract children id's
            child_id1=nodes[ascending_nodes[i]]->children[0];
            child_id2=nodes[ascending_nodes[i]]->children[1];
            if (nodes[child_id1]->strahl_level==nodes[child_id2]->strahl_level) nodes[ascending_nodes[i]]->strahl_level=(nodes[child_id1]->strahl_level)+1; //if both child nodes have the same strahl level ... then the strahl level of this node is child_level+1
                    
            else nodes[ascending_nodes[i]]->strahl_level=std::max(nodes[child_id1]->strahl_level,nodes[child_id2]->strahl_level); //If children don't have the same strahl level then the strahl-level is the max of the childs levels
            
            if (nodes[ascending_nodes[i]]->parent !=-1) ascending_nodes_new.push_back(nodes[ascending_nodes[i]]->parent);   //if the current node is not the root node, push the parent node into the ascending nodes list
            
            
            
            }
            
            //remove duplicatesfrom ascending nodes new
            std::sort(ascending_nodes_new.begin(),ascending_nodes_new.end());       
            endLocation=std::unique(ascending_nodes_new.begin(),ascending_nodes_new.end());
            ascending_nodes_new.erase(endLocation, ascending_nodes_new.end());
            
            ascending_nodes=ascending_nodes_new;
     
         
         
         
         
         }
        
        max_strahl_level=nodes[0]->strahl_level;
        

        
        
        return 0;
    }
    
int BinTree::Instrument(){
    // Instruments the given tree by calling Calc_levels and Calc_stahl_level
    if (nr_branch_points>0){
        Calc_levels();
        Calc_strahl();
        Calc_bifurcation();
        fill_length_hist();
    }
    
    
    
    return 0;
    }

int BinTree::Calc_bifurcation(){
    // Calcs the nr of segments with the same strahl level and the bifurcation ratio for all levels... this is somewhat inefficient but should be fast enough
    std::vector<int> ascending_nodes,ascending_nodes_new;
    std::vector<int>::iterator endLocation;
    double avg;
    unsigned int i;
    int child_id1,child_id2;
    int nr_strahl_segments_1;
    
    nr_strahl_segments_1=0;
    
    for(i=0; i<max_strahl_level+1; i++) nr_strahl_segments.push_back(0);    //fill the strahl_level vector with 0's
    
    for (i = 0 ; i<nodes.size(); i++){  //write all the leave node id's into the beginning ascending nodes list
        if(nodes[i]->isleave){
            ascending_nodes.push_back(nodes[i]->parent);    //put all the parent nodes of the leave nodes into the ascending list (since we already visited the leaves)
            nodes[i]->strahl_level=1;
             nr_strahl_segments_1++;
            }
            
    }
    
    
    
    while(ascending_nodes.size()>0){ //Terminate ascending the tree when we reached the root node
        ascending_nodes_new.clear();
        for (i = 0 ; i<ascending_nodes.size(); i++){
            //extract children id's
            child_id1=nodes[ascending_nodes[i]]->children[0];
            child_id2=nodes[ascending_nodes[i]]->children[1];
            
            if (nodes[child_id1]->strahl_level==nodes[child_id2]->strahl_level){
                if (nodes[ascending_nodes[i]]->strahl_segment_not_counted) {
                    nr_strahl_segments[nodes[ascending_nodes[i]]->strahl_level]++;
                    nodes[ascending_nodes[i]]->strahl_segment_not_counted=false;
                }
            }
            
            if (nodes[ascending_nodes[i]]->parent !=-1) ascending_nodes_new.push_back(nodes[ascending_nodes[i]]->parent);   //if the current node is not the root node, push the parent node into the ascending nodes list
            
            
            
            }
            
            //remove duplicatesfrom ascending nodes new
            std::sort(ascending_nodes_new.begin(),ascending_nodes_new.end());       
            endLocation=std::unique(ascending_nodes_new.begin(),ascending_nodes_new.end());
            ascending_nodes_new.erase(endLocation, ascending_nodes_new.end());
            
            ascending_nodes=ascending_nodes_new;
     
         
         
         
         
         }
    
    nr_strahl_segments[1]=nr_strahl_segments_1;
    nr_strahl_segments[nodes[0]->strahl_level]=1;
    
    avg=0;
    for(i=1;i<(nodes[0]->strahl_level);i++){
        bifurcation.push_back(float(nr_strahl_segments[i])/nr_strahl_segments[i+1]);
        avg+=float(nr_strahl_segments[i])/nr_strahl_segments[i+1];
        }
    avg_bifurcation_ratio=avg/(nodes[0]->strahl_level-1);
    
        
 
    return 0;
        
}



BinTree::BinTree(double p_br,double p_el,int max_nr_branches){
    std::vector<int> growing_nodes,growing_nodes_new;
    int node_number;
    unsigned int i;   
    double rnd;
    //BinTree Tree;
    //nodes.clear();
    nr_branch_points=0;
    magnitude=0;
    avg_bifurcation_ratio=0;
    max_length=0;
    height=1;    
    
    addnode(-1,0,-1);


    
    growing_nodes.push_back(0);
    node_number=0;


    while(growing_nodes.size()>0)
    {
        growing_nodes_new.clear(); //empty the list of new growing nodes

        for (i = 0 ; i<growing_nodes.size(); i++)
        {
            rnd=drand48();
            if (rnd<p_el)   //since p_el is probably the largest probability it is checked first
            {
                
                nodes[growing_nodes[i]]->length_nr+=1;   //make this node longer
                nodes[growing_nodes[i]]->length+=1;
                //growing_nodes_new.push_back(node_number);   //Write the node in the list of nodes that will be growing next turn
                growing_nodes_new.push_back(growing_nodes[i]);   //Write the node in the list of nodes that will be growing next turn
            }
            
            else if(rnd<(p_br+p_el))
            {
                nodes[growing_nodes[i]]->children[0]=node_number+1;
                nodes[growing_nodes[i]]->children[1]=node_number+2;
                addnode(growing_nodes[i],node_number+1,node_number+2);
                addnode(growing_nodes[i],node_number+2,node_number+1);
                nr_branch_points+=1;
                growing_nodes_new.push_back(node_number+1); //adds new nodes to list that has still growing nodes
                growing_nodes_new.push_back(node_number+2);
                node_number+=2;
                
                }
            } 
            
            growing_nodes=growing_nodes_new;    //the growing list is the list of nodes added this turn
            if (nr_branch_points>max_nr_branches) p_br=0;

    
    }

    }
    
BinTree::~BinTree(){
        
    std::vector<TreeNode*>::iterator it;
    for (it = nodes.begin() ; it != nodes.end(); ++it)
        {
        delete *it; 
        }
            
             
    In_cube_nodes.clear();
    In_cube_segment.clear();
    nodes.clear();
    bifurcation.clear();
    nr_strahl_segments.clear();
    length_hist.clear();
    
    Synapse_x_pos.clear();  
    Synapse_y_pos.clear();
    Synapse_z_pos.clear();
    
    
    }
    
int BinTree::print_nodes(char* fn){
    
    std::vector<TreeNode*>::iterator it;
    std::ofstream f;
    f.open(fn);
    
    f << " Tree height: "<< height <<" branch points " << nr_branch_points << '\n';
    f << "node_id   nr_segments     parent  sibling     child_left      child_right     level   strahl_level    \n";
    for (it = nodes.begin() ; it != nodes.end(); ++it)
        {
            f << (*it)->my_id << "  " << (*it)->length_nr << "  " << (*it)->parent << "    " << (*it)->sibling << "    " << (*it)->children[0] << "    " << (*it)->children[1] << "    " << (*it)->level << "    " << (*it)->strahl_level << '\n';
        }
            
    f.close();
    return 0;         
    
}
    
    
int BinTree::print_3D_pos(){
    
    std::vector<TreeNode*>::iterator it;
    unsigned int i;
    double length;
    int last_length_prob_node_id,last_length_prob_seg_id;
    double last_prob_length;
    bool length_prob;
    
    length_prob=false;
    
    std::cout << "-------------------- Position data -------------------------------------" << '\n';
    for (it = nodes.begin() ; it != nodes.end(); ++it)
        {
            for(i=0;i<(*it)->x_pos.size();i++){
            if (i>0){
                length=((*it)->x_pos[i]-(*it)->x_pos[i-1])*((*it)->x_pos[i]-(*it)->x_pos[i-1]);
                length+=((*it)->y_pos[i]-(*it)->y_pos[i-1])*((*it)->y_pos[i]-(*it)->y_pos[i-1]);
                length+=((*it)->z_pos[i]-(*it)->z_pos[i-1])*((*it)->z_pos[i]-(*it)->z_pos[i-1]);
                //we don't calc sqrt since length should be 1 anyway
                    if ((length>1.0000000001) || (length<0.9999999999)) {    //This is a test
                        length_prob=true;
                        last_length_prob_node_id=(*it)->my_id;
                        last_length_prob_seg_id=i;
                        last_prob_length=length;
                    }
                }
            else length=1; 
            
            std::cout <<" my_id: " << (*it)->my_id << " parent " << (*it)->parent << " length-node " << i << " x_pos: " << (*it)->x_pos[i] << " y_pos: " << (*it)->y_pos[i] << " z_pos: " << (*it)->z_pos[i] << " length " << length <<'\n';
            }
        }
        
    if (length_prob) {
        
        std::cout << "THERE WERE LENGTH PROBLEMS" <<" my_id: " << last_length_prob_node_id << " problem segment " << last_length_prob_seg_id << " last prob length " << last_prob_length <<'\n';
        }
            
    return 0;         
}
    
    
int BinTree::print_3D_pos_py_readable(char* fn){
    
    std::vector<TreeNode*>::iterator it;
    unsigned int i;
    double length;
    int last_length_prob_node_id,last_length_prob_seg_id;
    double last_prob_length;
    bool length_prob;
    
    length_prob=false;
    
    std::ofstream f;
    f.open(fn);
    
    f << "node_id   parent_id   segment_nr  x_seg   y_seg   z_seg   nr_segments_on_node   L_Child_id   R_Child_id   sibling_id" << '\n';
    f << "-------------------- Position data -------------------------------------" << '\n';
    for (it = nodes.begin() ; it != nodes.end(); ++it)
        {
            for(i=0;i<(*it)->x_pos.size();i++){
            if (i>0){
                length=((*it)->x_pos[i]-(*it)->x_pos[i-1])*((*it)->x_pos[i]-(*it)->x_pos[i-1]);
                length+=((*it)->y_pos[i]-(*it)->y_pos[i-1])*((*it)->y_pos[i]-(*it)->y_pos[i-1]);
                length+=((*it)->z_pos[i]-(*it)->z_pos[i-1])*((*it)->z_pos[i]-(*it)->z_pos[i-1]);
                //we don't calc sqrt since length should be 1 anyway
                    if ((length>1.0000000001) || (length<0.9999999999)) {    //This is a test
                        length_prob=true;
                        last_length_prob_node_id=(*it)->my_id;
                        last_length_prob_seg_id=i;
                        last_prob_length=length;
                    }
                }
            else length=1; 
            
            f << (*it)->my_id << "  " << (*it)->parent << " " << i << "     " << (*it)->x_pos[i] << "   " << (*it)->y_pos[i] << "   " << (*it)->z_pos[i] << "   " << (*it)->length_nr << "  " << (*it)->children[0] << "    " <<(*it)->children[1]<<"   "<< (*it)->sibling << '\n';
            }
        }
        
        if (length_prob) {
        
        f << "THERE WERE LENGTH PROBLEMS" <<" my_id: " << last_length_prob_node_id << " problem segment " << last_length_prob_seg_id << " last prob length " << last_prob_length <<'\n';
        }
            
    f.close();
    
    return 0;         
}
    
    
    
int BinTree::Remake(double p_br,double p_el,int max_nr_branches){
    std::vector<int> growing_nodes,growing_nodes_new;
    std::vector<TreeNode*>::iterator it;
    int node_number;   
    unsigned int i;
    double rnd;
    
    for (it = nodes.begin() ; it != nodes.end(); ++it)
        {
        delete *it; 
        }
    
    nodes.clear();
    bifurcation.clear();
    nr_strahl_segments.clear();
    length_hist.clear();
    
    In_cube_nodes.clear();      //This clears the vectors that are used later for the distribution of synapses. New tree means they should be emptied
    In_cube_segment.clear();
    
    Synapse_x_pos.clear();  //New tree also means we need new synapse positions
    Synapse_y_pos.clear();
    Synapse_z_pos.clear();
    
    
    nr_branch_points=0;
    magnitude=0;
    avg_bifurcation_ratio=0;
    max_length=0;
    height=1;   
    max_strahl_level=0;
    avg_collateral_length=0;
    total_collateral_Length=0; 
    length_in_cube=0;
    
    addnode(-1,0,-1);


    
    growing_nodes.push_back(0);
    node_number=0;


    while(growing_nodes.size()>0)
    {
        growing_nodes_new.clear(); //empty the list of new growing nodes

        for (i = 0 ; i<growing_nodes.size(); i++)
        {
            rnd=drand48();
            if (rnd<p_el)   //since p_el is probably the largest probability it is checked first
            {
                
                nodes[growing_nodes[i]]->length_nr+=1;   //make this node longer
                nodes[growing_nodes[i]]->length+=1;
                //growing_nodes_new.push_back(node_number);   //Write the node in the list of nodes that will be growing next turn
                growing_nodes_new.push_back(growing_nodes[i]);   //Write the node in the list of nodes that will be growing next turn
            }
            
            else if(rnd<(p_br+p_el))
            {
                nodes[growing_nodes[i]]->children[0]=node_number+1;
                nodes[growing_nodes[i]]->children[1]=node_number+2;
                addnode(growing_nodes[i],node_number+1,node_number+2);
                addnode(growing_nodes[i],node_number+2,node_number+1);
                nr_branch_points+=1;
                growing_nodes_new.push_back(node_number+1); //adds new nodes to list that has still growing nodes
                growing_nodes_new.push_back(node_number+2);
                node_number+=2;
                
                }
            } 
            
            growing_nodes=growing_nodes_new;    //the growing list is the list of nodes added this turn
            if (nr_branch_points>max_nr_branches) p_br=0;

    
    }
return 0;
}


int BinTree::fill_length_hist(){
    //fills the length_hist array but Calc_Strahl needs to be run before else it will get confused with max_length
    int size_length_hist, i,idx;
    double length_sum;
    if (max_length!=0){
        length_sum=0;
        size_length_hist=std::ceil(max_length);
        for (i = 0 ; i<=size_length_hist+1; i++) length_hist.push_back(0); //make the length hist vector long enough
            
        for (i = 0 ; i<nodes.size(); i++){
            length_sum+=nodes[i]->length;
            idx=std::floor(nodes[i]->length);
            length_hist[idx]+=1;
            
        }
        total_collateral_Length=length_sum;
        avg_collateral_length=length_sum/nodes.size();
            
            
    }
    
    else std::cout << "fill_length_hist has been called before Calc_strahl the max_length parameter is 0... that means fill_length_hist doesn't know what to do" << '\n';
    
    return 0;
}
    
    
int BinTree::Make_3D_tree(struct  Spatial_Tree_coeff * coeff){
    std::vector<double> dir,pos,dir_new;    //vector in which the direction of the growth goes
    std::vector<int> descending_nodes,descending_nodes_new; //descending nodes, like in the calc-level function
    double rnd,angle,rnd_nr;
    int id_branch;
    unsigned int i,j;
    double angle_full,angle_left,angle_right,L_angle;
    
    double branching_angle_mean = (coeff->branching_angle_mean);
    double branching_angle_std= (coeff->branching_angle_std);
    double kink_angle_mean = (coeff->kink_angle_mean);
    double kink_angle_std = (coeff->kink_angle_std);
    double p_change_dir = (coeff->p_change_dir);
    double L_angle_mean = (coeff->L_angle_mean);
    double L_angle_std= (coeff->L_angle_std);
    
    /*branching_angle_mean=70.0;  //mean of the branching angle on branch bifurcation
    branching_angle_std=30.5;   // sigma of branching angle on branch bifurcation
    kink_angle_mean=17.0;   // This if rom the "How straight do axons grow paper, but the range there is 0... 110 so perhaps check that againb and don't allow negative values"
    kink_angle_std=20.0;
    
    p_change_dir=0.8;*/
    
    //--------------------------- set up the loop by doing the growth for the parent node --------------------------------------------------------//
    dir.push_back(0);  //set up the direction vector as 001, this is the direction in which the tree starts to grow. 
    dir.push_back(0);
    dir.push_back(1);
    
    pos.push_back(0);  //That is the current position vector 
    pos.push_back(0);
    pos.push_back(1);
    
    
    nodes[0]->x_pos.push_back(0);   //also the node has already coordinates 0,0,1
    nodes[0]->y_pos.push_back(0);
    nodes[0]->z_pos.push_back(1);
    
    
    //First thing is for the parent node, after that we enter the loop, that saves us a few if's in the loop and might make it faster
    
    if (nodes[0]->children[0]!=-1){   //If the node has children (children[]!=-1) then add the children to the descending nodes
        //Here we also calculate the branching angle and distribution of branching angles for the following branch points at the end of the collateral, this seems out of order
        //but it only needs to be done when there are children, so we do that here.
                descending_nodes.push_back(nodes[0]->children[0]);
                descending_nodes.push_back(nodes[0]->children[1]);
                
                //here we need to choose the angles the turning of the directions is done when we grow out the child nodes!
                rnd_nr=Box_Mueller_1_nr(); //normally distributed random number
                angle_full=(rnd_nr*branching_angle_std)+branching_angle_mean;
                
                if (angle_full<0) angle_full=360.0+angle_full;
                //std::cout << angle_full <<"\n";
                
                //Here we put the new splitting procedure in
                rnd=Box_Mueller_1_nr(); //Pull a normally distributed number
                L_angle=(rnd*L_angle_std)+L_angle_mean;             //angle between new axon and old growth direction see Katz 1985()
                
                //We make a if distinction here in case L_angle_mean is not symetric, so that the larger angle is not preferentially on one side.
                if(drand48()>0.5){  
                    
                    angle_right=180.0-L_angle;
                    angle_left=angle_right-angle_full;
                    
                    }
                else{
                    angle_left=L_angle-180.0;
                    angle_right=angle_full+angle_left; //+ angle_left since angle_left is usually counted negative
                    
                    
                    }
                
                
                
                nodes[0]->branching_angle[0]=angle_left;   //angle to the left branch shoulds be random
                nodes[0]->branching_angle[1]=angle_right;  // angle to the right branch 
                nodes[0]->branch_turning_angle=drand48()*360.0;
                
        }
    
    //Here we physically grow the current node out to its length, this is done for all nodes so it does not be contained in an if     
    for (j = 0 ; j<(nodes[0]->length_nr-1); j++){
    //previous grows give us dir_vector+coordinate of endpoint of last grow from here we go on-> calculate new direction vector from old and then grow out till final point
    //calculate new direction of direction vector with probability of p_change_dir else it grows on in the old direction
    rnd=drand48();
    if (rnd<p_change_dir){ 
        
        rnd_nr=Box_Mueller_1_nr(); //normally distributed random number
        angle=(rnd_nr*kink_angle_std)+kink_angle_mean; //This could be negative but that is not a problem
        //angle=17.0; //this should be randomized
        
        
        
        
        dir_new=shear_out(dir,angle);   //shears out the axon from it's old direction
        angle=drand48()*360.0;
        dir_new=Rotate_vec(dir_new,dir,angle);  //turns the sheared out axon around the old growing direction by somewhere between 0-360°
        dir=dir_new;    //the new direction is the sheared out and rotated old direction
    
    }        
    
    //before the axon grows into the direction of the direction vector the direction is saved with the old node ... that is for further use in later parts of the program
    nodes[0]->x_dir.push_back(dir[0]);
    nodes[0]->y_dir.push_back(dir[1]);
    nodes[0]->z_dir.push_back(dir[2]);
    
    pos[0]+=dir[0]; //new position is reached by growing the axon in direction of the direcxtion vector... assuming all elements have a length of 1
    pos[1]+=dir[1];
    pos[2]+=dir[2];
    
    
    
    
    nodes[0]->x_pos.push_back(pos[0]);  //at this point the position vector is 1 longer than the direction vector that is because the last position gets it's final direction assigned when the next node starts to grow. so we can't save it yet.
    nodes[0]->y_pos.push_back(pos[1]);
    nodes[0]->z_pos.push_back(pos[2]);
    }
    
    nodes[0]->last_dir.push_back(dir[0]);   //fill the last_dir of the node with the last growing direction (important for the branching )
    nodes[0]->last_dir.push_back(dir[1]);
    nodes[0]->last_dir.push_back(dir[2]);
    
    
    //--------------------------- end of set up for the parent node --------------------------------------------------------//
    
    
    
    while(descending_nodes.size()>0){ //terminate climbing down the tree when there are no more child nodes to be reached
        descending_nodes_new.clear();
        for (i = 0 ; i<descending_nodes.size(); i++){
            dir.clear();
            dir_new.clear();
            if (nodes[descending_nodes[i]]->children[0]!=-1){   //If the node has children (children[]!=-1) then add the children to the descending nodes
            //Here we also calculate the branching angle and distribution of branching angles for the following branch points at the end of the collateral, this seems out of order
            //but it only needs to be done when there are children, so we do that here.
                descending_nodes_new.push_back(nodes[descending_nodes[i]]->children[0]);
                descending_nodes_new.push_back(nodes[descending_nodes[i]]->children[1]);
                //Choosing angles
                rnd_nr=Box_Mueller_1_nr(); //normally distributed random number
                angle_full=(rnd_nr*branching_angle_std)+branching_angle_mean;
                
               if (angle_full<0) angle_full=360.0+angle_full;
                
                rnd=drand48();


                //Here we put the new splitting procedure in
                rnd=Box_Mueller_1_nr(); //Pull a normally distributed number
                L_angle=(rnd*L_angle_std)+L_angle_mean;             //angle between new axon and old growth direction see Katz 1985()
                
                //We make a if distinction here in case L_angle_mean is not symetric, so that the larger angle is not preferentially on one side.
                if(drand48()>0.5){  
                    
                    angle_right=180.0-L_angle;
                    angle_left=angle_right-angle_full;
                    
                    }
                else{
                    angle_left=L_angle-180.0;
                    angle_right=angle_full+angle_left;  //+ angler_left since left is counted generally negatively 
                    
                    
                    }
                
                nodes[descending_nodes[i]]->branching_angle[0]=angle_left;   //angle to the left branch shoulds be random
                nodes[descending_nodes[i]]->branching_angle[1]=angle_right;  // angle to the right branch 
                nodes[descending_nodes[i]]->branch_turning_angle=drand48()*360.0;
                
                }
                
                
                pos[0]=nodes[nodes[descending_nodes[i]]->parent]->x_pos.back(); //position is the last position of the parent node, from here we keep on growing.
                pos[1]=nodes[nodes[descending_nodes[i]]->parent]->y_pos.back();
                pos[2]=nodes[nodes[descending_nodes[i]]->parent]->z_pos.back();
                
                
                dir=nodes[nodes[descending_nodes[i]]->parent]->last_dir;    //dir starts out as the last direction of the parent node but needs to be turned by the branching angles 
                
                
                
                
                
                (nodes[nodes[descending_nodes[i]]->parent]->children[0]==nodes[descending_nodes[i]]->my_id) ? id_branch=0 : id_branch=1; //This statement checks if the branch is the left or right branch and chooses the correct angle (set id_branch) accordingly
                
               // std::cout << "before_shear+rot: "<< dir[0] << " "<< dir[1] <<" " << dir[2]<< " id_branch: "<< id_branch <<"\n";
                
                
                angle=nodes[nodes[descending_nodes[i]]->parent]->branching_angle[id_branch];   //in these commands we rotate dir since it changes direction on the branching
                //std::cout << "branching_angle:" << angle << "\n";

                dir_new=shear_out(dir,angle);
                angle=nodes[nodes[descending_nodes[i]]->parent]->branch_turning_angle;
                //std::cout << "turning_angle:" << angle << "\n";
                dir_new=Rotate_vec(dir_new,dir,angle);  
                
                dir=dir_new;    
                
                //std::cout << "after_shear+rot: " << dir[0] << " "<< dir[1] <<" " << dir[2]<< " id_branch: "<< id_branch <<"\n" <<"\n";
                
                /* Here we push the position that is the last position of the parent node back once. This is so that all the nodes are connected and so that the 
                 * starting point of the next node is the same as the ending point of the last node */
                nodes[descending_nodes[i]]->x_pos.push_back(pos[0]);   
                nodes[descending_nodes[i]]->y_pos.push_back(pos[1]);
                nodes[descending_nodes[i]]->z_pos.push_back(pos[2]);
                
                
                
                //pushing back the direction into the direction vector
                nodes[descending_nodes[i]]->x_dir.push_back(dir[0]);
                nodes[descending_nodes[i]]->y_dir.push_back(dir[1]);
                nodes[descending_nodes[i]]->z_dir.push_back(dir[2]);
                
                
                //here we grow out the node once in its direction after branching before entering the loop
                pos[0]+=dir[0]; //new position is reached by growing the axon in direction of the direcxtion vector... assuming all elements have a length of 1
                pos[1]+=dir[1];
                pos[2]+=dir[2];
    
    
                //Position after the first growth-> especially after the branching.
                nodes[descending_nodes[i]]->x_pos.push_back(pos[0]);   
                nodes[descending_nodes[i]]->y_pos.push_back(pos[1]);
                nodes[descending_nodes[i]]->z_pos.push_back(pos[2]);
                
                
                
                
                //Here we physically grow the current node out to its length, this is done for all nodes so it does not be contained in an if     
                for (j = 0 ; j<(nodes[descending_nodes[i]]->length_nr-1); j++){   //do this one less than the length since we already grew once away from the branch
                //previous grows give us dir_vector+coordinate of endpoint of last grow from here we go on-> calculate new direction vector from old and then grow out till final point
                //calculate new direction of direction vector with probability of p_change_dir else it grows on in the old direction
                rnd=drand48();
                if (rnd<p_change_dir){ 
                    rnd_nr=Box_Mueller_1_nr(); //normally distributed random number
                    angle=(rnd_nr*kink_angle_std)+kink_angle_mean; //This could be negative but that is not a problem
                    
                    
                    //angle=17.0; //this should be randomized
                    dir_new=shear_out(dir,angle);   //shears out the axon from it's old direction
                    angle=drand48()*360.0;
                    dir_new=Rotate_vec(dir_new,dir,angle);  //turns the sheared out axon around the old growing direction by somewhere between 0-360°
                    dir=dir_new;    //the new direction is the sheared out and rotated old direction
                
                }
                
                //trying to push back the direction into the direction vector
                nodes[descending_nodes[i]]->x_dir.push_back(dir[0]);
                nodes[descending_nodes[i]]->y_dir.push_back(dir[1]);
                nodes[descending_nodes[i]]->z_dir.push_back(dir[2]);
                
                        
                pos[0]+=dir[0]; //new position is reached by growing the axon in direction of the direcxtion vector... assuming all elements have a length of 1
                pos[1]+=dir[1];
                pos[2]+=dir[2];
                
                nodes[descending_nodes[i]]->x_pos.push_back(pos[0]);   
                nodes[descending_nodes[i]]->y_pos.push_back(pos[1]);
                nodes[descending_nodes[i]]->z_pos.push_back(pos[2]);
                }
                
                nodes[descending_nodes[i]]->last_dir.push_back(dir[0]);   //fill the last_dir of the node with the last growing direction (important for the branching )
                nodes[descending_nodes[i]]->last_dir.push_back(dir[1]);
                nodes[descending_nodes[i]]->last_dir.push_back(dir[2]);
            
            
            
            }
            
            descending_nodes=descending_nodes_new;
    }

    return 0;
}


int BinTree::Reset_3D(){    //Resets the 3D tree to the tree that was generated after Make_tree (all 3D growing info is reset... this could probably just be put in front of make3Dtree ... do that)
        
        In_cube_nodes.clear();      //This clears the vectors that are used later for the distribution of synapses. A new 3D structur invalidates the old vectors -> so we clear them here.
        In_cube_segment.clear();
        
        Synapse_x_pos.clear();  
        Synapse_y_pos.clear();
        Synapse_z_pos.clear();
        
        for (std::vector<TreeNode*>::iterator it = nodes.begin() ; it != nodes.end(); ++it)
        {
            (*it)->branching_angle[0]=-1000;    //resets the branching angles.
            (*it)->branching_angle[1]=-1000;
            (*it)->branch_turning_angle=-1000;
            (*it)->x_pos.clear();
            (*it)->y_pos.clear();
            (*it)->z_pos.clear();
            (*it)->last_dir.clear();
            (*it)->x_dir.clear();
            (*it)->y_dir.clear();
            (*it)->z_dir.clear();
            
        }
    
    return 0;
}


int BinTree::Spread_uptake(double cube_start[3], double*** cube, double resolution, int cube_size){    
    /* This function assumes that all single axon-segments have a length of 1 Mikrometer! If you change that prerequisite you also have to change this function (slightly)! */
    std::vector<int> contrib_nodes;
    std::vector<double> distances;
    const double axon_segment_length=1;
    unsigned int i,j;
    double side_length;
    double dist;
    int loc_code;   //location code that saves where the point is with respect to the cube. Uses a base 3 like system.
    Point_3 P_node;
    int seg_start_idx,node_idx;
    bool start_inside,end_inside;
    int in_cube_coord[3];    //the coordinates of the cubic cell that a point is in
    double x_wall_dist,y_wall_dist,z_wall_dist,x_dir_dist,y_dir_dist,z_dir_dist;
    double curr_x,curr_y,curr_z;
    side_length=cube_size*resolution; //lngth of one of the sides of the cube in mikrometers
    double cube_min[3]={cube_start[0],cube_start[1],cube_start[2]};    //the coordinate minima andmaxima of the cube
    double cube_max[3]={cube_start[0]+side_length,cube_start[1]+side_length,cube_start[2]+side_length};
    double min_dir_dist,length_remaining;
    int min_dir_dir;
    
    In_cube_nodes.clear();      //This clears the vectors that are used later for the distribution of synapses. This is done here to reset the segments before the vectors in case Spread uptake is called twice in a row.
    In_cube_segment.clear();
    length_in_cube=0;   //set the length in cube for this bintree to 0 for initialization. This variable should hold the sum of all axon_length in the cube, this will be required to figure out how many synapses are going to be attached to the axon later.
    
    
    //std::cout << cube_start[0] << " "<< cube_start[1] << " "<< cube_start[2] << '\n';
    
    
    Point_3 P[8]; /*points of the cube distributed so that x=0->P[0,1,2,3],x=1->P[4,5,6,7]; y=0->P[0,1,6,7], y=1->P[2,3,4,5] and z=0->P[0,2,4,6], z=1->[1,3,5,7] (here 0 means the cube_start of the axis and 1 is cube_start+side_length)*/
    P[0]=Point_3(cube_start[0],cube_start[1],cube_start[2]);  
    P[1]=Point_3(cube_start[0],cube_start[1],cube_start[2]+side_length);
    P[2]=Point_3(cube_start[0],cube_start[1]+side_length,cube_start[2]);      
    P[3]=Point_3(cube_start[0],cube_start[1]+side_length,cube_start[2]+side_length);  
    P[4]=Point_3(cube_start[0]+side_length,cube_start[1]+side_length,cube_start[2]);  
    P[5]=Point_3(cube_start[0]+side_length,cube_start[1]+side_length,cube_start[2]+side_length);  
    P[6]=Point_3(cube_start[0]+side_length,cube_start[1],cube_start[2]);  
    P[7]=Point_3(cube_start[0]+side_length,cube_start[1],cube_start[2]+side_length);  
    
    Segment_3 S[12]; /* line segments connecting the points ... this cube shit is really annoying ... there must be a better way but I guess I am too blind to see */
    S[0]=Segment_3(P[0],P[1]);
    S[1]=Segment_3(P[1],P[7]);
    S[2]=Segment_3(P[7],P[6]);
    S[3]=Segment_3(P[0],P[6]);
    S[4]=Segment_3(P[2],P[3]);
    S[5]=Segment_3(P[3],P[5]);
    S[6]=Segment_3(P[5],P[4]);
    S[7]=Segment_3(P[4],P[2]);
    S[8]=Segment_3(P[0],P[2]);
    S[9]=Segment_3(P[1],P[3]);
    S[10]=Segment_3(P[5],P[7]);
    S[11]=Segment_3(P[4],P[6]);

    Plane_3 F[6];   /* The faces of the cube*/
    F[0]=Plane_3(P[0],P[1],P[6]);   //Front
    F[1]=Plane_3(P[0],P[1],P[3]);   //left
    F[2]=Plane_3(P[2],P[3],P[4]);   //back
    F[3]=Plane_3(P[4],P[5],P[6]);   //right
    F[4]=Plane_3(P[1],P[3],P[5]);   //top
    F[5]=Plane_3(P[0],P[2],P[4]);   //bottom

   
    

    //-----------------------------------here we loop over the nodes and check if they parts of them could possibly be in the cube, if yes we put them in the vector active_nodes
    /* We only need to look at every 2nd node since the siblings are always consecutively numbered and start at the same point. -> Since they start at the same point 
     * we only have to calc the distance once. Then we check with both lengths if they can be inside.
     * siblings will reach the cube. */
    
   
    contrib_nodes.push_back(0);  //The parent node 0 is a special case since it doesn't have a sibling, but since it's just one node we add it to the contrib_nodes list without checking it's distance. 
    for (i = 2 ; i< nodes.size(); i+=2) //start with node 2 -> to avoid more if parts in the loop ... deal with parent node (id=0) as a special case before
    {
        Point_3 P_node(nodes[i]->x_pos[0],nodes[i]->y_pos[0],nodes[i]->z_pos[0]);
        
        //Here we check how far the starting point is away from the cube. This will require some nested logic. There might be a better more elegant way to do this ^^
        //First step checks if the point is left or right of the cube... or inside
        //This loc_code implementation is easier to read but might be a bit slower than a pure-> if/else mechanism since we also have to do the loc_code comparisons later 
        if (nodes[i]->x_pos[0]<cube_start[0]) loc_code=0;   //set first digit of loc_code -> left right center 0 1 2
        else if( nodes[i]->x_pos[0]>(cube_start[0]+side_length)) loc_code=1;
        else loc_code=2;
        
        if (nodes[i]->y_pos[0]<cube_start[1]) loc_code+=0;   //second digit of loc_code 0,3,6 -> front behind center
        else if( nodes[i]->y_pos[0]>(cube_start[1]+side_length)) loc_code+=3;
        else loc_code+=6;
        
        if (nodes[i]->z_pos[0]<cube_start[2]) loc_code+=0;   //third digit of loc_code 0,9,18 -> bottom top center
        else if( nodes[i]->z_pos[0]>(cube_start[2]+side_length)) loc_code+=9;
        else loc_code+=18;

        //----- the loc_code comparisons are messy but done like this so that the whole thing can be both somewhat efficient while also being somewhat readable ... probably achieving neither 
        //The switch statement should make it somewhat comprehensible
        dist=-1;
        switch(loc_code){
            //These cases all have point to point distances
            case 0:
                dist=squared_distance(P[0],P_node);
                break;
            
            case 1:
                dist=squared_distance(P[6],P_node);
                break;
            
            case 3:
                dist=squared_distance(P[2],P_node);
                break;
            
            case 4:
                dist=squared_distance(P[4],P_node);
                break;

            case 9:
                dist=squared_distance(P[1],P_node);
                break;        
        
            case 10:
                dist=squared_distance(P[7],P_node);
                break;
            
            case 12:
                dist=squared_distance(P[3],P_node);
                break;
            
            case 13:
                dist=squared_distance(P[5],P_node);
                break;
                
            //Now the cases for point to line distances come!
            //inside x-span(of the cube) but outside y+z span
            case 2:
                dist=squared_distance(S[3],P_node);
                break;
            
            case 5:
                dist=squared_distance(S[7],P_node);
                break;
            
            case 11:
                dist=squared_distance(S[1],P_node);
                break;
                
            case 14:
                dist=squared_distance(S[5],P_node);
                break;
            //inside y-span but outside x+z span 
            case 6:
                dist=squared_distance(S[8],P_node);
                break;
                
            case 7:
                dist=squared_distance(S[11],P_node);
                break;
                
            case 15:
                dist=squared_distance(S[9],P_node);
                break;
            
            case 16:
                dist=squared_distance(S[10],P_node);
                break;
            //inside z-span but outside x+y span
            case 18:
                dist=squared_distance(S[0],P_node);
                break;
            
            case 19:
                dist=squared_distance(S[2],P_node);
                break;
                
            case 21:
                dist=squared_distance(S[4],P_node);
                break;
                
            case 22:
                dist=squared_distance(S[6],P_node);
                break;
                
            //These cases now are point to plane cases
            //in front of x-y-plane
            case 8:
                dist=squared_distance(F[5],P_node);
                break;
            
            case 17:
                dist=squared_distance(F[4],P_node);
                break;
            //in front of x-z plane
            case 20:
                dist=squared_distance(F[0],P_node);
                break;
                
            case 23:
                dist=squared_distance(F[2],P_node);
                break;
            //in front of y-z plane
            case 24:
                dist=squared_distance(F[1],P_node);
                break;
                
            case 25:
                dist=squared_distance(F[3],P_node);
                break;
                
            //inside the cube:
            case 26:
                dist=0;
                break;
        
        }
        //the CGAL function gives th squared distance, so we take the sqrt here to find the actual distance.
        dist=sqrt(dist);
        
        /*after calculating the distance of the node starting point to the cube, we check if both sibling nodes could possibly reach the cube, 
          * if yes then they are added to the contributing nodes and will be taken into account for the DA-uptake distribution. */
        if ((nodes[i]->length)>dist) {contrib_nodes.push_back(i); distances.push_back(dist);  }       
        if ((nodes[i-1]->length)>dist) {contrib_nodes.push_back(i-1); distances.push_back(dist);  }
        
    }   //here the preselection is done -> the distribution onto the cube now has to be done for each segment of each node in the contrib_nodes vector. 
    
    //std::cout << "Before preselection we had: " << nodes.size() << " after we have: " << contrib_nodes.size() << " left \n"; 
    
    
    
    for (i = 0 ; i<contrib_nodes.size(); ++i){
        seg_start_idx=std::floor(distances[i]);  //segment start index -> segment from which we are starting to loop ... we can ignore all floor(distance) first segments since they can't reach the cube
        node_idx=contrib_nodes[i];  //idx of the contributing node
        
        /* Set up the following loop. Here we have to check if the first considered point is inside the cube or not*/
        end_inside=true;     
        if(nodes[node_idx]->x_pos[seg_start_idx]<cube_min[XVAR] || nodes[node_idx]->x_pos[seg_start_idx]>cube_max[XVAR]) end_inside=false;
        if(nodes[node_idx]->y_pos[seg_start_idx]<cube_min[YVAR] || nodes[node_idx]->y_pos[seg_start_idx]>cube_max[YVAR]) end_inside=false;
        if(nodes[node_idx]->z_pos[seg_start_idx]<cube_min[ZVAR] || nodes[node_idx]->z_pos[seg_start_idx]>cube_max[ZVAR]) end_inside=false;
        
        
        /* Here we loop over all the contributing nodes and all segments off them to calculate how much distance of each segment passes through each computing cell.*/
        for (j=seg_start_idx; j<nodes[node_idx]->x_pos.size()-1; ++j ){ //the last ends at the last point so we only have to loop to the second to last point 
            
            start_inside=end_inside;    //the start- point of the segment is inside if the end point oif the last segment was inside the ciube since the segments are connected
            
            /* This checks if the end point of the segment is inside the cube*/
            end_inside=true;
            if(nodes[node_idx]->x_pos[j+1]<cube_min[XVAR] || nodes[node_idx]->x_pos[j+1]>cube_max[XVAR]) end_inside=false;
            if(nodes[node_idx]->y_pos[j+1]<cube_min[YVAR] || nodes[node_idx]->y_pos[j+1]>cube_max[YVAR]) end_inside=false;
            if(nodes[node_idx]->z_pos[j+1]<cube_min[ZVAR] || nodes[node_idx]->z_pos[j+1]>cube_max[ZVAR]) end_inside=false;   
            
            
            
           
            if(start_inside) {
                length_remaining=axon_segment_length;
                In_cube_nodes.push_back(node_idx);  //push back the node that has segments inside
                In_cube_segment.push_back(j);       // push back which segments of the above pushed back node are inside the cube
                
                
                //x,y,z position of the start point of the segment we are working on. We copy this here because we will change this with time
                curr_x=nodes[node_idx]->x_pos[j];              
                curr_y=nodes[node_idx]->y_pos[j];
                curr_z=nodes[node_idx]->z_pos[j];

                //finds the cube coordinates of the start point of the segment
                in_cube_coord[XVAR]=std::floor((curr_x-cube_min[XVAR])/resolution);
                in_cube_coord[YVAR]=std::floor((curr_y-cube_min[YVAR])/resolution);
                in_cube_coord[ZVAR]=std::floor((curr_z-cube_min[ZVAR])/resolution);


                while(length_remaining>0){
                    //now we have to find the length in the segment
                    
                    //These dir_distances are the fraction of the direction vector that we have to traverse until we hit the respective wall, x_dir_dist=0.3 means that after 0.3 units of the direction vector we hit the x_wall
                    if (nodes[node_idx]->x_dir[j]!=0){ 
                        //calculates distance along the direction vector to the next wall in x_dir in units of length of the direction vector.
                        x_wall_dist=( (nodes[node_idx]->x_dir[j]>0) ? (cube_start[0]+(in_cube_coord[XVAR]+1)*resolution)-curr_x : (cube_start[0]+in_cube_coord[XVAR]*resolution)-curr_x );  //first find the relevant distances to the wall, positive distances are to the right (in x -dir) and negative to the left (also x-dir)... distances are calculated depending on the relevant component of the direction vector
                        x_dir_dist=x_wall_dist/nodes[node_idx]->x_dir[j];
                        if (x_dir_dist<0) x_dir_dist=0;
                    }
                    else x_dir_dist=DBL_MAX ;
                    
                    if (nodes[node_idx]->y_dir[j]!=0){
                        y_wall_dist=( (nodes[node_idx]->y_dir[j]>0) ? (cube_start[1]+(in_cube_coord[YVAR]+1)*resolution)-curr_y : (cube_start[1]+in_cube_coord[YVAR]*resolution)-curr_y );
                        y_dir_dist=y_wall_dist/nodes[node_idx]->y_dir[j];
                        if (y_dir_dist<0) y_dir_dist=0;
                    }
                    else y_dir_dist=DBL_MAX ;
                    
                    if (nodes[node_idx]->z_dir[j]!=0){
                        z_wall_dist=( (nodes[node_idx]->z_dir[j]>0) ? (cube_start[2]+(in_cube_coord[ZVAR]+1)*resolution)-curr_z : (cube_start[2]+in_cube_coord[ZVAR]*resolution)-curr_z );
                        z_dir_dist=z_wall_dist/nodes[node_idx]->z_dir[j];
                        if (z_dir_dist<0) z_dir_dist=0;
                    }
                    else z_dir_dist=DBL_MAX ;
                    
                    //finds distance along the dir_vector to the closest wall, and which wall is the closest. min_dir_dir holds information on which axis the closest wall is
                    if(x_dir_dist>y_dir_dist){  //is x or y the closest wall ?
                        min_dir_dist=y_dir_dist;
                        min_dir_dir=YVAR;
                        }
                    else{
                        min_dir_dist=x_dir_dist;
                        min_dir_dir=XVAR;
                        }
                    
                    if(min_dir_dist>z_dir_dist){    //check if it is z ... if not than just keep the old values
                        min_dir_dist=z_dir_dist;
                        min_dir_dir=ZVAR;
                        }
                    

                    
                    if(min_dir_dist>=length_remaining){
                        cube[in_cube_coord[XVAR]][in_cube_coord[YVAR]][in_cube_coord[ZVAR]]+=length_remaining;
                        length_in_cube+=length_remaining;
                        length_remaining=0;
                    }
                    else{
                        cube[in_cube_coord[XVAR]][in_cube_coord[YVAR]][in_cube_coord[ZVAR]]+=min_dir_dist;
                        length_in_cube+=min_dir_dist;
                        /*go along direction vector until wall is hit ... The code nows that in the next loop we are in a different cell since we updated the cell at the point
                         * where we figured out which wall is closest  */
                        curr_x+=min_dir_dist*(nodes[node_idx]->x_dir[j]);   
                        curr_y+=min_dir_dist*(nodes[node_idx]->y_dir[j]);
                        curr_z+=min_dir_dist*(nodes[node_idx]->z_dir[j]);
                        
                        switch(min_dir_dir){    //update the coordinates of the cell we are in because we hit the wall. This should also work in the case where we hit a corner since next iteration the distance to this coordinate will be 0 or close to 0)
                            case XVAR:
                                    if(nodes[node_idx]->x_dir[j]>0) in_cube_coord[XVAR]++;
                                    else in_cube_coord[XVAR]--;
                                break;
                            
                            case YVAR:
                                    if(nodes[node_idx]->y_dir[j]>0) in_cube_coord[YVAR]++;
                                    else in_cube_coord[YVAR]--;
                                break;
                                
                            case ZVAR:
                                    if(nodes[node_idx]->z_dir[j]>0) in_cube_coord[ZVAR]++;
                                    else in_cube_coord[ZVAR]--;
                                break;
                        }
                        length_remaining-=min_dir_dist;
                        if ((in_cube_coord[min_dir_dir]>cube_size-1) || (in_cube_coord[min_dir_dir]<0)) length_remaining=0; //if the coordinate of the next cell is outside the cube... terminate the loop-> only difference to the start+end part inside
                    }
                     
                    }
                    
                }   //end of the if-condition that applies when start and end of the segment are inside the large cube. 
                
                     
                
                
                //here we deal with only the end segment being inside 
                else if(!start_inside && end_inside) {
                /* This can be handled like the start inside but not end inside case by just flipping the calculation of the segment-> turn around dir vector and start at the end point*/
                    length_remaining=axon_segment_length;
                    In_cube_nodes.push_back(node_idx);  //push back the node that has segments inside
                    In_cube_segment.push_back(j);       // push back which segments of the above pushed back node are inside the cube-> we push back segments that only have an end inside too to not get in trouble at the boundaries later.
                    
                    
                    
                    //x,y,z position of the start point of the segment we are working on. We copy this here because we will change this with time
                    curr_x=nodes[node_idx]->x_pos[j+1];   //select the endpoint instead of the start point           
                    curr_y=nodes[node_idx]->y_pos[j+1];
                    curr_z=nodes[node_idx]->z_pos[j+1];

                    //finds the cube coordinates of the end point of the segment
                    in_cube_coord[XVAR]=std::floor((curr_x-cube_min[XVAR])/resolution);
                    in_cube_coord[YVAR]=std::floor((curr_y-cube_min[YVAR])/resolution);
                    in_cube_coord[ZVAR]=std::floor((curr_z-cube_min[ZVAR])/resolution);
                             
                    /*here we will know keep walking through the cube like in the case where both start and end point are inside, but we will stop this when we are leaving the cube 
                    * -> that means when one of the in_cube coord is larger than cube-size-1 or smaller than 0.
                    * */ 
                    
                    while(length_remaining>0){
                    //now we have to find the length in the segment
                    
                    //These dir_distances are the fraction of the direction vector that we have to traverse until we hit the respective wall, x_dir_dist=0.3 means that after 0.3 units of the direction vector we hit the x_wall
                    if (nodes[node_idx]->x_dir[j]!=0){ 
                        //calculates distance along the direction vector to the next wall in x_dir in units of length of the direction vector.
                        x_wall_dist=( ((-1.0*nodes[node_idx]->x_dir[j])>0) ? (cube_start[0]+(in_cube_coord[XVAR]+1)*resolution)-curr_x : (cube_start[0]+in_cube_coord[XVAR]*resolution)-curr_x );  //first find the relevant distances to the wall, positive distances are to the right (in x -dir) and negative to the left (also x-dir)... distances are calculated depending on the relevant component of the direction vector
                        x_dir_dist=x_wall_dist/(-1.0*nodes[node_idx]->x_dir[j]);
                        //the direction vector here is flipped since we go from the end to the start
                        if (x_dir_dist<0) x_dir_dist=0;
                    }
                    else x_dir_dist=DBL_MAX ;
                    
                    if (nodes[node_idx]->y_dir[j]!=0){
                        y_wall_dist=( ((-1.0*nodes[node_idx]->y_dir[j])>0) ? (cube_start[1]+(in_cube_coord[YVAR]+1)*resolution)-curr_y : (cube_start[1]+in_cube_coord[YVAR]*resolution)-curr_y );
                        y_dir_dist=y_wall_dist/(-1.0*nodes[node_idx]->y_dir[j]);
                        if (y_dir_dist<0) y_dir_dist=0;
                    }
                    else y_dir_dist=DBL_MAX ;
                    
                    if (nodes[node_idx]->z_dir[j]!=0){
                        z_wall_dist=( ((-1.0*nodes[node_idx]->z_dir[j])>0) ? (cube_start[2]+(in_cube_coord[ZVAR]+1)*resolution)-curr_z : (cube_start[2]+in_cube_coord[ZVAR]*resolution)-curr_z );
                        z_dir_dist=z_wall_dist/(-1.0*nodes[node_idx]->z_dir[j]);
                        if (z_dir_dist<0) z_dir_dist=0;
                    }
                    else z_dir_dist=DBL_MAX ;
                    
                    //finds distance along the dir_vector to the closest wall, and which wall is the closest. min_dir_dir holds information on which axis the closest wall is
                    if(x_dir_dist>y_dir_dist){  //is x or y the closest wall ?
                        min_dir_dist=y_dir_dist;
                        min_dir_dir=YVAR;
                        }
                    else{
                        min_dir_dist=x_dir_dist;
                        min_dir_dir=XVAR;
                        }
                    
                    if(min_dir_dist>z_dir_dist){    //check if it is z ... if not than just keep the old values
                        min_dir_dist=z_dir_dist;
                        min_dir_dir=ZVAR;
                        }
                    

                    
                    if(min_dir_dist>=length_remaining){
                        cube[in_cube_coord[XVAR]][in_cube_coord[YVAR]][in_cube_coord[ZVAR]]+=length_remaining;
                        length_in_cube+=length_remaining;
                        length_remaining=0;
                    }
                    else{
                        cube[in_cube_coord[XVAR]][in_cube_coord[YVAR]][in_cube_coord[ZVAR]]+=min_dir_dist;
                        length_in_cube+=min_dir_dist;
                        /*go along direction vector until wall is hit ... The code knows that in the next loop we are in a different cell since we updated the cell at the point
                         * where we figured out which wall is closest  */
                        curr_x+=min_dir_dist*((-1.0*nodes[node_idx]->x_dir[j]));   
                        curr_y+=min_dir_dist*((-1.0*nodes[node_idx]->y_dir[j]));
                        curr_z+=min_dir_dist*((-1.0*nodes[node_idx]->z_dir[j]));
                        
                        switch(min_dir_dir){    //update the coordinates of the cell we are in because we hit the wall. This should also work in the case where we hit a corner since next iteration the distance to this coordinate will be 0 or close to 0)
                            case XVAR:
                                    if((-1.0*nodes[node_idx]->x_dir[j])>0) in_cube_coord[XVAR]++;
                                    else in_cube_coord[XVAR]--;
                                break;
                            
                            case YVAR:
                                    if((-1.0*nodes[node_idx]->y_dir[j])>0) in_cube_coord[YVAR]++;
                                    else in_cube_coord[YVAR]--;
                                break;
                                
                            case ZVAR:
                                    if((-1.0*nodes[node_idx]->z_dir[j]>0)) in_cube_coord[ZVAR]++;
                                    else in_cube_coord[ZVAR]--;
                                break;
                        }
                        length_remaining-=min_dir_dist;
                        if ((in_cube_coord[min_dir_dir]>cube_size-1) || (in_cube_coord[min_dir_dir]<0)) length_remaining=0; //if the coordinate of the next cell is outside the cube... terminate the loop-> only difference to the start+end part inside
                    }
                     
                    }
                    
                
                }
                
                
                //When neither end nor start of a segment are inside a cube we can just ignore the case because no length will be added to the cube
                
                
            
            
            
            
            
            
            
        }
    }
    
    
    
    
    return 0;
    }


int BinTree::Attach_synapses(struct  Spatial_Tree_coeff * coeff,double cube_start[3], double*** cube, double resolution, int cube_size){
    double dist_synapses=(coeff->dist_synapses);
    
    
    int Nr_synapses;
    Nr_synapses_attached=int (length_in_cube/dist_synapses);
    Nr_synapses=Nr_synapses_attached;
    
    double size_exclusion=(coeff->size_exclusion);
    double side_length;
    
    side_length=cube_size*resolution; //lngth of one of the sides of the cube in mikrometers
    double cube_min[3]={cube_start[0],cube_start[1],cube_start[2]};    //the coordinate minima andmaxima of the cube
    double cube_max[3]={cube_start[0]+side_length,cube_start[1]+side_length,cube_start[2]+side_length};
    int in_cube_coord[3];    //the coordinates of the cubic cell that a point is in
    
    
    int i,idx,rnd_idx,seg_dir_idx; 
    double rnd,seg_dist;
    double syn_x,syn_y,syn_z;   //x,y,z coordinates of one synapdse 
    bool in_cube;
    int curr_node_idx,curr_segment_idx,next_node_idx,next_segment_idx;
    double curr_x_pos,curr_y_pos,curr_z_pos;
    double curr_x_dir,curr_y_dir,curr_z_dir;
    double x_dir_dist,y_dir_dist,z_dir_dist;
    double x_wall_dist,y_wall_dist,z_wall_dist;
    double min_dir_dist;
    int min_dir_dir;
    double length_remaining;
    int left_right;
    
    
    Synapse_x_pos.clear();  //clear the vectors, if the function is run twice it just gives new synapse positions then
    Synapse_y_pos.clear();
    Synapse_z_pos.clear();
    
    

    //Here we actually determine positions of synapses.
    for(i=0;i<Nr_synapses;++i){
        in_cube=false;
        while(!in_cube){
            idx=std::floor(drand48()*In_cube_nodes.size());  //randomly determines one of the segments on which the synapse should be 
            // For this random determination of the segment, the vector in_cube_nodes has multiple entries for each node, always corresponding to a segment on the node, so we only pull one random number, this will then be a synapse that is on 
            // one randomlz chosen segment of a randomly chosen node.
            
            rnd=drand48();
            //from the beginning of the segment there is a step of random length (between 0 and 1) taken in the direction of current segment
            syn_x=nodes[In_cube_nodes[idx]]->x_pos[In_cube_segment[idx]]+(nodes[In_cube_nodes[idx]]->x_dir[In_cube_segment[idx]]*rnd);
            syn_y=nodes[In_cube_nodes[idx]]->y_pos[In_cube_segment[idx]]+(nodes[In_cube_nodes[idx]]->y_dir[In_cube_segment[idx]]*rnd);
            syn_z=nodes[In_cube_nodes[idx]]->z_pos[In_cube_segment[idx]]+(nodes[In_cube_nodes[idx]]->z_dir[In_cube_segment[idx]]*rnd);
            
            //checks if synapse is inside the cube
            if(syn_x>cube_min[XVAR] && syn_x<cube_max[XVAR]){
                if(syn_y>cube_min[YVAR] && syn_y<cube_max[YVAR]){
                    if(syn_z>cube_min[ZVAR] && syn_z<cube_max[ZVAR]) in_cube=true;
                } 
            }
        
        }
        //std::cout << "x , y , z: " << syn_x << "  "<< syn_y << "  " << syn_z <<"\n"; 
        Synapse_x_pos.push_back(syn_x);
        Synapse_y_pos.push_back(syn_y);
        Synapse_z_pos.push_back(syn_z);
        
        
        
        
        //CAVEAT: It turns out that the effect of this is very likely to be extremely minor... but its a massive piece of logic... I guess sometimes we just waste our time
        //Now we have the x,y,z positions of the synapses now we remove a terminal exclusion zone from the axons around it The zone shoulkd be symetric and its length along the axon is decided by a parameter in coeff
        //This part here is similar to the part in the Axon Spread uptake function... but here we might be on more than one segment. so we have to adjust for this (usually we shouldn't skip more than on esegment but you never know)
        for(left_right=-1;left_right<=1;left_right+=2){ //goes to the left of the synapse first and then to the right
            
            length_remaining=size_exclusion; //This needs o be done twice ... first we go to the right... along direction second iteration we go left against the direction

                    
                    
            //x,y,z position of the start point of the segment we are working on. We copy this here because we will change this with time
            curr_x_pos=syn_x;             
            curr_y_pos=syn_y;
            curr_z_pos=syn_z;

            curr_x_dir=(double)left_right*nodes[In_cube_nodes[idx]]->x_dir[In_cube_segment[idx]];         //the direction we start with... opposite to the Spread uptake function this might actually change here.
            curr_y_dir=(double)left_right*nodes[In_cube_nodes[idx]]->y_dir[In_cube_segment[idx]];
            curr_z_dir=(double)left_right*nodes[In_cube_nodes[idx]]->z_dir[In_cube_segment[idx]];   //need to cast left_right to double for the flip of direction 

            //finds the cube coordinates of the start point of the segment
            in_cube_coord[XVAR]=std::floor((curr_x_pos-cube_min[XVAR])/resolution);
            in_cube_coord[YVAR]=std::floor((curr_y_pos-cube_min[YVAR])/resolution);
            in_cube_coord[ZVAR]=std::floor((curr_z_pos-cube_min[ZVAR])/resolution);

            curr_node_idx=In_cube_nodes[idx];
            curr_segment_idx=In_cube_segment[idx];

            while(length_remaining>0){  
                //now we have to find the length in the segment
                            
                //These dir_distances are the fraction of the direction vector that we have to traverse until we hit the respective wall, x_dir_dist=0.3 means that after 0.3 units of the direction vector we hit the x_wall
                if (curr_x_dir!=0){ 
                    //calculates distance along the direction vector to the next wall in x_dir in units of length of the direction vector.
                    x_wall_dist=( (curr_x_dir>0) ? (cube_start[0]+(in_cube_coord[XVAR]+1)*resolution)-curr_x_pos : (cube_start[0]+in_cube_coord[XVAR]*resolution)-curr_x_pos );  //first find the relevant distances to the wall, positive distances are to the right (in x -dir) and negative to the left (also x-dir)... distances are calculated depending on the relevant component of the direction vector
                    x_dir_dist=x_wall_dist/curr_x_dir;
                    if (x_dir_dist<0) x_dir_dist=0;
                }
                else x_dir_dist=DBL_MAX ;
                            
                if (curr_y_dir!=0){
                    y_wall_dist=( (curr_y_dir>0) ? (cube_start[1]+(in_cube_coord[YVAR]+1)*resolution)-curr_y_pos : (cube_start[1]+in_cube_coord[YVAR]*resolution)-curr_y_pos );
                    y_dir_dist=y_wall_dist/curr_y_dir;
                    if (y_dir_dist<0) y_dir_dist=0;
                }
                else y_dir_dist=DBL_MAX ;
                            
                if (curr_z_dir!=0){
                    z_wall_dist=( (curr_z_dir>0) ? (cube_start[2]+(in_cube_coord[ZVAR]+1)*resolution)-curr_z_pos : (cube_start[2]+in_cube_coord[ZVAR]*resolution)-curr_z_pos );
                    z_dir_dist=z_wall_dist/curr_z_dir;
                    if (z_dir_dist<0) z_dir_dist=0;
                }
                else z_dir_dist=DBL_MAX ;

                //finds distance along the dir_vector to the closest wall, and which wall is the closest. min_dir_dir holds information on which axis the closest wall is
                if(x_dir_dist>y_dir_dist){  //is x or y the closest wall ?
                    min_dir_dist=y_dir_dist;
                    min_dir_dir=YVAR;
                }
                else{
                    min_dir_dist=x_dir_dist;
                    min_dir_dir=XVAR;
                }
                            
                if(min_dir_dist>z_dir_dist){    //check if it is z ... if not than just keep the old values
                    min_dir_dist=z_dir_dist;
                    min_dir_dir=ZVAR;
                }
                


                /* Different to the spread uptake function we will here also calculate the distance along the segment to the end to the segment, in case we reach the end of the 
                 * segment before we reach a cell wall we have to update curr_pos and direction*/
                
                //Here we now have to make a case distinction for going left and right of the synapse (since we are either working up or down the tree)
                if(left_right==1){  //here we g right ... or down the tree
                
                    //First we have to check if the next segment is on the same node or not... In the first iteration when we sit in the middle of the segment this will alsways go to the else condition
                    if(curr_segment_idx+1>nodes[curr_node_idx]->x_pos.size()-1){    //check if we would be out of bounds when we just go one step further along the current segment, if not we cross a branch and need to choose a new node_idx
                        rnd_idx=(drand48()>0.5) ? 0 : 1;    //if there is a branching choose one random branch for the exclusion
                        if(nodes[curr_node_idx]->children[rnd_idx]!=-1){    //Check if the node where it should have actually has children... if not just set length_remaining to 0 and terminate

                            next_node_idx=nodes[curr_node_idx]->children[rnd_idx];
                            next_segment_idx=1; //we skip 0 because 0 has the same coordinates as curr_segment_idx+1 ... that means it has already been used and is the position we sit at
                            
                            curr_node_idx=next_node_idx;    //Since we move onto the next branch.
                            curr_x_dir=nodes[next_node_idx]->x_dir[0];      //In case of a branch the direction changes too
                            curr_y_dir=nodes[next_node_idx]->y_dir[0];
                            curr_z_dir=nodes[next_node_idx]->z_dir[0];
                            
                            curr_x_pos=nodes[next_node_idx]->x_pos[0];     
                            curr_y_pos=nodes[next_node_idx]->y_pos[0];
                            curr_z_pos=nodes[next_node_idx]->z_pos[0];
                            curr_segment_idx=0;
                            next_segment_idx=1;
                        
                        }
                        else{
                            length_remaining=0;
                        }
                        

                        
                    }
                    else{
                         next_segment_idx=curr_segment_idx++;   //after this if condition the next_segment and next_node_idx tell us where the next point is we have to go to
                         next_node_idx=curr_node_idx;
                    }

                }       //end of case distinction going left or right
                
                else if(left_right==-1){    //This is the case where we go left (up the tree)
                    //First we have to check if the next segment is on the same node or not
                    if(curr_segment_idx-1<0){    //check if we would be out of bounds when we just go one step further along the current segment, else we have to choose the last segment of the parent node
                            if(nodes[curr_node_idx]->parent>0){ 
                            
                                next_node_idx=nodes[curr_node_idx]->parent; //new node is the parent of the current branch
                                next_segment_idx=nodes[next_node_idx]->x_pos.size()-1; //last segment of the parent nodes branch
                                
                                curr_node_idx=next_node_idx;    //Since we move onto the next branch.
                                curr_x_dir=(double)left_right*nodes[next_node_idx]->x_dir[next_segment_idx-1];      //the direction changes too, we have to take
                                curr_y_dir=(double)left_right*nodes[next_node_idx]->y_dir[next_segment_idx-1];      //next_segment_idx-1 because the direction vector is shorter than the position vector
                                curr_z_dir=(double)left_right*nodes[next_node_idx]->z_dir[next_segment_idx-1];
                                
                                curr_x_pos=nodes[next_node_idx]->x_pos[next_segment_idx];     
                                curr_y_pos=nodes[next_node_idx]->y_pos[next_segment_idx];
                                curr_z_pos=nodes[next_node_idx]->z_pos[next_segment_idx];
                                curr_segment_idx=next_segment_idx;
                                next_segment_idx=next_segment_idx-1;
                            }
                            else{
                                length_remaining=0;
                                }
                        
                    }
                    else{
                         next_segment_idx=curr_segment_idx--;   //after this if condition the next_segment and next_node_idx tell us where the next point is we have to go to
                         next_node_idx=curr_node_idx;
                    }
                    
                    
                }     

                if(length_remaining>0){
                    
                //Here we have to calculate the distance to the end of the segment. 
                //First we choose the largest value of the x,y,z direction to get best numerical results
                if(fabs(curr_x_dir)>fabs(curr_y_dir)){
                    if (fabs(curr_x_dir)>fabs(curr_z_dir)) seg_dir_idx=0;
                    else seg_dir_idx=2;
                }
                else{
                    if(fabs(curr_y_dir)>fabs(curr_z_dir)) seg_dir_idx=1;
                    else seg_dir_idx=2;
                }
                
                switch(seg_dir_idx){
                    case XVAR:
                        seg_dist=fabs((nodes[next_node_idx]->x_pos[next_segment_idx]-curr_x_pos)/curr_x_dir);
                        break;
                    
                    case YVAR:
                        seg_dist=fabs((nodes[next_node_idx]->y_pos[next_segment_idx]-curr_y_pos)/curr_y_dir);
                        break;
                        
                    case ZVAR:
                        seg_dist=fabs((nodes[next_node_idx]->z_pos[next_segment_idx]-curr_z_pos)/curr_z_dir);
                        break;
                    } //Now we have a distance in units of the length of the direction vector (1) from the end of the segment... this can be used now to figure out if we hit a cell wall or a segment end first
                    //This might be a stupid way to do this... we could perhaps also check if the segment is just outside of the end of the cell coordinates... but idk. 
                
                
                            
                if (min_dir_dist<seg_dist){
                                
                    if(min_dir_dist>=length_remaining){
                        cube[in_cube_coord[XVAR]][in_cube_coord[YVAR]][in_cube_coord[ZVAR]]-=length_remaining;
                        length_remaining=0;
                    }
                    else{
                        cube[in_cube_coord[XVAR]][in_cube_coord[YVAR]][in_cube_coord[ZVAR]]-=min_dir_dist;
                        /*go along direction vector until wall is hit ... The code nows that in the next loop we are in a different cell since we updated the cell at the point
                        * where we figured out which wall is closest  */
                        curr_x_pos+=min_dir_dist*(curr_x_dir);   
                        curr_y_pos+=min_dir_dist*(curr_y_dir);
                        curr_z_pos+=min_dir_dist*(curr_z_dir);
                                    
                        switch(min_dir_dir){    //update the coordinates of the cell we are in because we hit the wall. This should also work in the case where we hit a corner since next iteration the distance to this coordinate will be 0 or close to 0)
                            case XVAR:
                                if(curr_x_dir>0) in_cube_coord[XVAR]++;
                                else in_cube_coord[XVAR]--;
                            break;
                                        
                            case YVAR:
                                if(curr_y_dir>0) in_cube_coord[YVAR]++;
                                else in_cube_coord[YVAR]--;
                            break;
                                                
                            case ZVAR:
                                if(curr_z_dir>0) in_cube_coord[ZVAR]++;
                                else in_cube_coord[ZVAR]--;
                            break;
                        }
                        length_remaining-=min_dir_dist;
                        if ((in_cube_coord[min_dir_dir]>cube_size-1) || (in_cube_coord[min_dir_dir]<0)) length_remaining=0; //if the coordinate of the next cell is outside the cube... terminate the loop-> only difference to the start+end part inside
                    }
                             
                }// end of case where min_dir_dist < seg_dist
                else{
                    if(seg_dist>=length_remaining){
                        cube[in_cube_coord[XVAR]][in_cube_coord[YVAR]][in_cube_coord[ZVAR]]-=length_remaining;
                        length_remaining=0;
                    } 
                    else{
                        cube[in_cube_coord[XVAR]][in_cube_coord[YVAR]][in_cube_coord[ZVAR]]-=seg_dist;
                        /*add the segment distance to the cube coord (technically remove)  */
                        curr_x_pos+=seg_dist*(curr_x_dir);   
                        curr_y_pos+=seg_dist*(curr_y_dir);
                        curr_z_pos+=seg_dist*(curr_z_dir);
                        
                        //here we have to make one more case distinction between left and right!
                        if(left_right==1) curr_segment_idx++; //we reach the next segment so the direction changes
                        else if (left_right==-1) curr_segment_idx--;
                        
                        
                        
                        
                        
                        curr_x_dir=nodes[curr_node_idx]->x_dir[curr_segment_idx]; 
                        curr_y_dir=nodes[curr_node_idx]->y_dir[curr_segment_idx];
                        curr_z_dir=nodes[curr_node_idx]->z_dir[curr_segment_idx];
                        
                        length_remaining-=seg_dist;
                        
                        }
                    
                    
                }
                
            }
            }
                
            
        }
    
    }
    
    return 0;
}
