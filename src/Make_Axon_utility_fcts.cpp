/*
 * 
 * 
 * This file Make_Axon_utility_fcts.cpp is the file containing the implementation of the Utility functions of the Make_Axons code.
 * These functions are also used by the BinTree class.  
 * These functions are part of the Make_axons utility of the DOPE-AMIN code suite. 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */


#include "Make_Axon_utility_fcts.h"
#include <math.h>
#include <stdlib.h>


double Box_Mueller_1_nr(){ /* returns a normally distributed number between 0 and 1 */
double a,temp1,temp2;

		temp1=drand48();
		temp2=drand48();	
		
		a= sqrt((-2)*log(temp1))*cos(2*M_PI*temp2);
		//b= sqrt((-2)*log(temp1))*sin(2*PI*temp2);
		return a;
}


std::vector<double> Rotate_vec(std::vector<double> vec,std::vector<double> axis, double angle){
    /* Rotates vec by an angle angle (in degrees) around the axis axis. vec and axis should be vectors with 3 elements ... if not sth. is wrong*/
    std::vector<double>  vec_rot;
    double Rotmat[3][3];
    double theta,length;
    unsigned int i;
    
    //Fills the rotation matrix with the correct values according to the rodrigues rotation formula
    theta=angle*(M_PI/180.0);
    Rotmat[0][0]=cos(theta)+axis[0]*axis[0]*(1.0-cos(theta));
    Rotmat[0][1]=axis[0]*axis[1]*(1.0-cos(theta))-axis[2]*sin(theta);
    Rotmat[0][2]=axis[1]*sin(theta)+axis[0]*axis[2]*(1.0-cos(theta));
    
    Rotmat[1][0]=axis[2]*sin(theta)+axis[0]*axis[1]*(1.0-cos(theta));
    Rotmat[1][1]=cos(theta)+axis[1]*axis[1]*(1.0-cos(theta));
    Rotmat[1][2]=-axis[0]*sin(theta)+axis[1]*axis[2]*(1.0-cos(theta));
        
    Rotmat[2][0]=-axis[1]*sin(theta)+axis[0]*axis[2]*(1.0-cos(theta));
    Rotmat[2][1]=axis[0]*sin(theta)+axis[1]*axis[2]*(1.0-cos(theta));
    Rotmat[2][2]=cos(theta)+axis[2]*axis[2]*(1.0-cos(theta));
    
    //matrix multiplikation
    vec_rot.push_back(Rotmat[0][0]*vec[0]+Rotmat[0][1]*vec[1]+Rotmat[0][2]*vec[2]);
    vec_rot.push_back(Rotmat[1][0]*vec[0]+Rotmat[1][1]*vec[1]+Rotmat[1][2]*vec[2]);
    vec_rot.push_back(Rotmat[2][0]*vec[0]+Rotmat[2][1]*vec[1]+Rotmat[2][2]*vec[2]);
    
    for(i=0; i<vec_rot.size(); i++) {
        if (fabs(vec_rot[i])<1.0e-15) vec_rot[i]=0;   //take care of some numerical artifacts that arise from sin(Pi)!=0 in the math library 
    }
    
    length=sqrt(vec_rot[0]*vec_rot[0]+vec_rot[1]*vec_rot[1]+vec_rot[2]*vec_rot[2]);   //length of vector
    vec_rot[0]/=length;  //normalize vector to prohibit runaway numerical errors
    vec_rot[1]/=length;
    vec_rot[2]/=length;
    
    //we normalize the vector to keep numerical errors under control
    return vec_rot;
}

std::vector<double> Find_perpendicular(std::vector<double> vec){
    /* Calculates a unit vector perpendicular to the unit vector vec and returns it*/
    std::vector<double>  vec_perp;
    if (fabs(vec[0])<1e-15 &&  fabs(vec[1])<1e-15){
        //if the vectors first 2 coponents are 0 the vector lies on the z axis so we return a vector on the y-axis
        vec_perp.push_back(0.0);
        vec_perp.push_back(1.0);
        vec_perp.push_back(0.0);
        
        }
    else{
        vec_perp.push_back(vec[1]/sqrt(vec[1]*vec[1]+vec[0]*vec[0]));   //vector is normalized here
        vec_perp.push_back(-vec[0]/sqrt(vec[1]*vec[1]+vec[0]*vec[0]));
        vec_perp.push_back(0.0);
        

        
        
        }
    
    return vec_perp;
}
    
std::vector<double> shear_out(std::vector<double> vec,double angle){
    /* sheares a vector out of the direction in moves in*/
    std::vector<double>  vec_sheared,vec_perp;
    vec_perp=Find_perpendicular(vec);
    vec_sheared=Rotate_vec(vec,vec_perp,angle);
    
    //for(i=0; i<vec_perp.size(); i++) {
        //std::cout << vec_perp[i] << '\n'; 
    //}
    
    
    return vec_sheared;
}
