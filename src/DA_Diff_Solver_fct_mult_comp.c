/*
 * 
 * 
 * This file DA_Diff_Solver_fct_mult_comp.c is the file containing all functions 
 * that are used by the multi component solver of the diffusion code DOPE-AMINE only.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */

#include "DA_Diff_macros_structs_shared.h"
#include "DA_Diff_macros_structs_mult_comp.h"
#include "DA_Diff_LAPACK.h"
#include "DA_Diff_Solver_fct_mult_comp.h"
#include <math.h>
#include <stdlib.h>





int Create_RHS_w_Fct(int N_comp,int Nx_calc,int Ny_calc,int Nz_calc,double **** RHS_1, double *** RHS_iter,double **** Conc_now,double **** fct_grid,double **** Jacobi,double dt){
    //This function also depends on the Reaction function, if the reaction function is time dependent then there needs to be a partial time derivative term here... I skipped that ... but be aware
    // this also has the Jacobi Matrix in it... so that also depends on the function in the reaction term
    int i,j,k,l,n;
    int ix,iy,iz;
    int n_offset;
    double JW_1,JW_2;
    double d_12,d_23,dt_05;
    
    d_12=1.0/12.0;
    d_23=2.0/3.0;
    dt_05=0.5*dt;
    
    
    for(n=0;n<N_comp;n++){  //this is the part of the RHS_iter where there is no matrix multiplication required for the RHS 
        for(i=0;i<Nx_calc;i++){
            for(j=0;j<Ny_calc;j++){
                for(k=0;k<Nz_calc;k++){
                    ix=i+1;
                    iy=j+1;
                    iz=k+1;
    
                    RHS_iter[i][j][k*N_comp+n]=RHS_1[n][i][j][k]+dt_05*(fct_grid[n][ix][iy][iz]+d_12*(fct_grid[n][ix][iy][iz-1]-2.0*fct_grid[n][ix][iy][iz]+fct_grid[n][ix][iy][iz+1]));
                
                    JW_1=0;   //Here we calculate the matrix vector products because we have J*w_ijk where w_ijk here is in Conc because it is not stared
                    JW_2=0;
                    n_offset=n*N_comp;
                    for(l=0;l<N_comp;l++){  //This handles the matrix multiplication terms ... so to say the JW term
                        
                        
                        JW_1-=(Jacobi[ix][iy][iz-1][n_offset+l]+Jacobi[ix][iy][iz+1][n_offset+l])*Conc_now[l][ix][iy][iz];    //dz2 applied to J
                        JW_1-=(Conc_now[l][ix][iy][iz-1]+Conc_now[l][ix][iy][iz+1])*Jacobi[ix][iy][iz][n_offset+l];   //dz2 applied to w
                          


                        JW_2-=Jacobi[ix][iy][iz][n_offset+l]*Conc_now[l][ix][iy][iz]; //this is the J*W term
                        
                        
                        
                        
                        
                        
                        }
                         RHS_iter[i][j][k*N_comp+n]+=dt_05*(JW_1*d_12+JW_2*d_23);
                
                
                }
            }
        }
    }
    
return 0;
}


int Calc_fct_grid(int N_comp, int Nx, int Ny, int Nz,double **** Conc_now, double **** fct_grid,struct Fct_param * F_param, double **** V_spatial){
    //This function has to be changed for each system we look at ... 
    int i,j,k;
    
    double K_m = (F_param->K_m);
    double k_unspecific= (F_param->k_unspecific);
    double k_on_D1= (F_param->k_on_D1);
    double k_on_D2= (F_param->k_on_D2);
    double k_off_D1= (F_param->k_off_D1);
    double k_off_D2= (F_param->k_off_D2);
    double C_D1_max= (F_param->C_D1_max);
    double C_D2_max= (F_param->C_D2_max);
    
    //In the conc array component 1 and 2 are D1 and D2 receptors... always the bound state
    
            
            //This is automatically periodic if Conc is periodic since it calculates it value depoending on only the value in the conc cell
            for(i=0;i<Nx;i++){
                for(j=0;j<Ny;j++){
                    for(k=0;k<Nz;k++){
                        
                        //We use V_spatial since the MM uptake is not spatially homogenous 
                        //We also added a value for unspecific uptake (this is spatially homogenous here but can be made spatially varying later)
                        //0th component-> changes in C_DA
                        fct_grid[0][i][j][k]=(V_spatial[0][i][j][k]*Conc_now[0][i][j][k])/(K_m+Conc_now[0][i][j][k]);   //MM uptake
                        fct_grid[0][i][j][k]+=k_unspecific*Conc_now[0][i][j][k];         //linear unspecific uptake
                        fct_grid[0][i][j][k]-=(k_on_D1*Conc_now[0][i][j][k]*(C_D1_max-Conc_now[1][i][j][k])+k_on_D2*Conc_now[0][i][j][k]*(C_D2_max-Conc_now[2][i][j][k])); //binding off da onto receptors
                        fct_grid[0][i][j][k]+=k_off_D1*Conc_now[1][i][j][k]+k_off_D2*Conc_now[2][i][j][k]; //Dopamine coming off the receptor        
                        
                        //1th component -> changes in C_D1_bound
                        fct_grid[1][i][j][k]=k_on_D1*Conc_now[0][i][j][k]*(C_D1_max-Conc_now[1][i][j][k])-k_off_D1*Conc_now[1][i][j][k];
                        
                        //2th component -> changes in C_D2_bound
                        fct_grid[2][i][j][k]=k_on_D2*Conc_now[0][i][j][k]*(C_D2_max-Conc_now[2][i][j][k])-k_off_D2*Conc_now[2][i][j][k];
              
              
                    }
                }
            }
            
            return 0;
    



}

int Block_vec_vec_product(double *U,double *vty,double *result,int m,int n){
    /*This function writes the result off the block_vector*vector product into the array result
     * This assumes U is a block vector with size m*n where vty is a vector with length m the result will be a vector of length n  */
    int i,k;
    
    
    for(i=0;i<n;i++) {
        result[i]=0;
        for(k=0;k<m;k++){
            result[i]+=U[k*n+i]*vty[k];
        }
    }
    
    
    return 0;
    
    
}



int Advance_t(int N_comp,int Nx_calc,int Ny_calc,int Nz_calc,double ****RHS_1,double **** RHS_reorg, double*** RHS_iter, struct D_system * Dsys,double **** fct_grid,double ****Conc_now,double ****Conc_next,double ****Jacobi, double * Diff_Mat_iter,double * U,double dt,struct Fct_param * F_param,double ****V_spatial){
    int i,j,k,n,l;
    int info,ldb,ldab,NRHS;
    char TRANS;
    double a,b;
    int iterations;
    
    double** r = (Dsys->r);
    double**** Diff_Mat = (Dsys->Diff_Mat);
    int*** IPIV = (Dsys->IPIV);
    int* IPIV_iter = (Dsys->IPIV_iter);
    double** x_periodic_sol=(Dsys->x_periodic_sol);  //has the solution of the part that doesn't change in it (x(2) for periodic boundaries) x_coor
    double** y_periodic_sol=(Dsys->y_periodic_sol);
    
    double* Component_max=(Dsys->Component_max);
    int Nr_diagonals,calc_size;
    int temp_lim;   //limit for for loops if the limit is the result of a calculation
    int temp_offset; //temporary offset for vector positions
    int Max_iterations=(Dsys->Max_iterations);
    double *temp_matrix;
    double *vty,*IVTZ,*IVTZ_temp,*vec_to_add;    //vty holds the vty vector for the SWM formula, IVTZ holds the N_compxN_comp matrix of the SWM formula vec_to_add will be a temporary container for holding the final result to be subtracted from x in the SWm formula
    int *IPIV_short;
    
    
    
    vty = (double *) malloc(N_comp*sizeof(double));
    IVTZ = (double *) malloc(N_comp*N_comp*sizeof(double));
    IVTZ_temp = (double *) malloc(N_comp*N_comp*sizeof(double));
    vec_to_add=(double *) malloc(N_comp*Nz_calc*sizeof(double));
    IPIV_short=(int *) malloc(N_comp*sizeof(int));
    temp_matrix = (double *) malloc(((3*((2*N_comp)-1)+1)*Nz_calc*N_comp)*sizeof(double));
    
        //printf("Timestep starts here! \n");
        //printf("Before first step, RHS[0][52][51][51]: %.5g \n",RHS_1[0][52][51][51]);
            
        //Reorganize the data in the RHS array so that the last dimension has the arrays of x ... so that it can be passed to the LAPACK routine  
        for(n=0;n<N_comp;n++){
            for(i=0;i<Nx_calc;i++){
                for(j=0;j<Ny_calc;j++){
                    for(k=0;k<Nz_calc;k++){  
                        RHS_reorg[n][j][k][i]=RHS_1[n][i][j][k];
                        
                    }
                }
            }
        }

        
        //Use dgttrs to solve equation 38 afterwards RHS_reorg will have the solution to 38 which is w**
        ldb=Nx_calc-1;  //this is the size of the reduced system now-needs to be solved first
        NRHS=1;
        TRANS='N';
        for(n=0;n<N_comp;n++){
              b=(1.0/(12.0))-((r[n][XVAR])/(2.0));  //off diagonal element of diff_mat
              a=1.0-(1.0/(6.0))+(r[n][XVAR]);                //a is main diagonal element of DIff_mat
              
              for(j=0;j<Ny_calc;j++){
                  for(k=0;k<Nz_calc;k++){
                      //this step should solve for x(1)
                      
                      dgttrs_( &TRANS, &ldb, &NRHS, Diff_Mat[n][XVAR][DIAG_L] , Diff_Mat[n][XVAR][DIAG], Diff_Mat[n][XVAR][DIAG_U], Diff_Mat[n][XVAR][DIAG_U2], IPIV[n][XVAR], RHS_reorg[n][j][k], &ldb, &info);
                      //Here I hope with ldb set to NX_reduced... that the wraparound cells are untouched during the process. -> If the solution looks funky the problem could be here
                      
                      //calculate value of wraparound cell and save it in RHS_reorg
                      RHS_reorg[n][j][k][Nx_calc-1]= (RHS_reorg[n][j][k][Nx_calc-1]-b*(RHS_reorg[n][j][k][0]+RHS_reorg[n][j][k][Nx_calc-2]))/(a+b*(x_periodic_sol[n][0]+x_periodic_sol[n][Nx_calc-2]));   //since our off diagonal elements are the same we can combine them for calculating X N+1
                      
                      //Correct solution of the reduced system to have the full solution in RHS_reorg
                      for(i=0;i<Nx_calc-1;i++){
                            RHS_reorg[n][j][k][i]+=x_periodic_sol[n][i]*RHS_reorg[n][j][k][Nx_calc-1];
                         }

                    }
                  
                  }
              
              
              
        }
    
    
    // !!!!!!!!!!!!!!!Solve second step !!!!!!!!!!!!!!
    //Reorganize so that RHS is again in oder x y z (there seem to be problems with reordering on the fly... so we try this now)
        for(n=0;n<N_comp;n++){
            for(i=0;i<Nx_calc;i++){
                for(j=0;j<Ny_calc;j++){
                    for(k=0;k<Nz_calc;k++){  
                        RHS_1[n][i][j][k]=RHS_reorg[n][j][k][i]; //reorganize to starting position

                    }
                }
            }
        }
            


        

            
        //Reorganize so that RHS is in oder x z y (there seem to be problems with reordering on the fly... so we try this now)
        for(n=0;n<N_comp;n++){
            
            for(i=0;i<Nx_calc;i++){
                for(j=0;j<Ny_calc;j++){
                    for(k=0;k<Nz_calc;k++){  
         
                        RHS_reorg[n][i][k][j]=RHS_1[n][i][j][k]; //reorganize to starting position
                    }
                }
            }
        }
        
    
    
    //use dgttrs again o solve eq. 39 to get w*
        ldb=Ny_calc-1;
        NRHS=1;
        TRANS='N';
        for(n=0;n<N_comp;n++){
              b=(1.0/(12.0))-((r[n][YVAR])/(2.0));  //off diagonal element of diff_mat
              a=1.0-(1.0/(6.0))+(r[n][YVAR]);                //a is main diagonal element of DIff_mat
            
              for(i=0;i<Nx_calc;i++){
                  for(k=0;k<Nz_calc;k++){
                      //this step should solve for x(1)
                      dgttrs_( &TRANS, &ldb, &NRHS, Diff_Mat[n][YVAR][DIAG_L] , Diff_Mat[n][YVAR][DIAG], Diff_Mat[n][YVAR][DIAG_U], Diff_Mat[n][YVAR][DIAG_U2], IPIV[n][YVAR], RHS_reorg[n][i][k], &ldb, &info);
                      //Here I hope with ldb set to NY_reduced... that the wraparound cells are untouched during the process. -> If the solution looks funky the problem could be here

                     //calculate value of wraparound cell and save it in RHS_reorg
                      RHS_reorg[n][i][k][Ny_calc-1]= (RHS_reorg[n][i][k][Ny_calc-1]-b*(RHS_reorg[n][i][k][0]+RHS_reorg[n][i][k][Ny_calc-2]))/(a+b*(y_periodic_sol[n][0]+y_periodic_sol[n][Nx_calc-2]));   //since our off diagonal elements are the same we can combine them for calculating X N+1
                      
                      //Correct solution of the reduced system to have the full solution in RHS_reorg
                      for(j=0;j<Ny_calc-1;j++){
                            RHS_reorg[n][i][k][j]+=y_periodic_sol[n][j]*RHS_reorg[n][i][k][Ny_calc-1];
                        } 

                    }
                  
                  }
              
              
              
        }
    
    // !!!!!!!!!!!!!!!Solve third step !!!!!!!!!!!!!!
        //Reorganize so that array is again xyz since reordering on the fly seems to lead to trouble (fix later for performance)
        for(n=0;n<N_comp;n++){
            for(i=0;i<Nx_calc;i++){
                for(j=0;j<Ny_calc;j++){
                    for(k=0;k<Nz_calc;k++){  

                        RHS_1[n][i][j][k]=RHS_reorg[n][i][k][j];

                    }
                }
            }
        }

        
        
        
        //solve last step here like eq.40 
        //RHS_1[n][i][j][k] has w* in it
        
        
        calc_size=N_comp*Nz_calc; //Dimension of the matrix we are solving
        Nr_diagonals=2*N_comp-1;
        ldab=3*(Nr_diagonals)+1;
        NRHS=1;
        ldb=N_comp*Nz_calc;
        TRANS='N';
        
        
        for(iterations=0;iterations<Max_iterations;iterations++){
        
            if(iterations!=0) Calc_fct_grid(N_comp,Nx_calc+2,Ny_calc+2,Nz_calc+2,Conc_now,fct_grid,F_param,V_spatial);  //the fct_grid should be advanced with each iteration!
        
            Fill_Jacobi(N_comp,Nx_calc+2,Ny_calc+2,Nz_calc+2,Conc_now,Jacobi,F_param,V_spatial);   //Fill the Jacobi Matrix for the whole domain.
            Create_RHS_w_Fct(N_comp,Nx_calc,Ny_calc,Nz_calc,RHS_1,RHS_iter,Conc_now,fct_grid,Jacobi,dt); //calculates right hand sides with the function parts like in equation 40. First oiteration can use fct_grid.
            //The RHS to be used is in RHS_iter, since we have to keep RHS_1=W* for later iterations
        
        
        // Here we build the diagonal matrix for each n,i,j coordinates and solve them on the fly
        //    for(n=0;n<N_comp;n++){ This loop was a legacy loop for the 1 component method, in the new code version we can't solve the components independent of each other because they are linked through the jacobi matrix
        //    we will have to fill the whole set here, the multiple component solution will have to have it's own code... perhaps branch it ? In a code set... suite wise we should choose different functions on compile time to use the appropriate advance t function
                  for(i=0;i<Nx_calc;i++){
                      for(j=0;j<Ny_calc;j++){
                          Fill_Diff_Mat_iter(i,j,Nz_calc,Diff_Mat_iter,Conc_now,Jacobi,r,dt,N_comp);    //Writes matrix T into Diff_mat_iter array   
                          Fill_block_vector_U(i,j,Nz_calc,U,Conc_now,Jacobi,r,dt,N_comp);                 //Writes the block vector U into the array U
                          
                          /*since we have to use the solved matrix for calculating the z-periodic aolution and the non modified solution  we use dgbtrf and solve for the vectors after
                           * this allows us to reuse the LU decomposition off the modified matrix T (See SWM formula)  to solve for Z and y in SWM formula*/
                          Transpose_matrix(Diff_Mat_iter,temp_matrix,calc_size,ldab);   //we need to transpose the matrix since we filled the DIFFMATiter array in row major instead of column major order (and LAPACK is compiled in forttran so it needs column major)
                          dgbtrf_(&calc_size,&calc_size,&Nr_diagonals,&Nr_diagonals,Diff_Mat_iter,&ldab,IPIV_iter,&info); 
                          
                          //here we use dgbtrs to solve for Z and y in the SWM formula
                          //first we solve for Z with NRHS=3
                          NRHS=3;
                          dgbtrs_(&TRANS,&calc_size,&Nr_diagonals,&Nr_diagonals,&NRHS,Diff_Mat_iter,&ldab,IPIV_iter,U,&ldb,&info);
                          
                          
                          //Then we solve for y, that is with one RHS
                          NRHS=1;
                          dgbtrs_(&TRANS,&calc_size,&Nr_diagonals,&Nr_diagonals,&NRHS,Diff_Mat_iter,&ldab,IPIV_iter,RHS_iter[i][j],&ldb,&info);
                          

                          
                          /* **********  Using SWM formula to create Periodic boundaries                     ******************************* */
                          /*Now we have to combine Z and y according to the SWM formula to the new RHS_iter 
                           *before we do this, we have y from the SWM formula in RHS_iter and Z from SWM in te Array U */
                          
                          //Fill vty vector with V^T y of the SWM formula , this means V^Ty=y1+yn where the vector y is inRHS_iter
                          temp_offset=N_comp*(Nz_calc-1);
                          for(k=0;k<N_comp;k++) vty[k]=RHS_iter[i][j][k]+RHS_iter[i][j][temp_offset+k];
                          
                          //Fill matrix I+V^TZ which is Z1+ZN+I   ... filled in column major order for passing to LAPACK
                          temp_offset=ldb-N_comp;
                          for(k=0;k<N_comp;k++){
                              for(l=0;l<N_comp;l++){
                                  //ldb is the length of one column in the block vector U ... we just reuse this here!
                                  IVTZ[k+l*N_comp]=U[k+l*ldb]+U[k+temp_offset+l*ldb]; //this is calculating Z1+ZN ... the block vector Z is in the array U in this case
                                  
                                  
                               }
                              IVTZ[k+N_comp*k]+=1;  //This adds I to Z1+ZN
                           }
                          
                          //solve the ((I+V^TZ)^-1)*V^Ty part. CAUTION wwe use NRHS without setting it to 1 again... if you change the code and change NRHS you have to put it to 1 before this call
                          dgesv_(&N_comp,&NRHS,IVTZ,&N_comp,IPIV_short,vty,&N_comp,&info);
                          
                          //Now we add Z*( ((I+V^TZ)^-1)*V^Ty) to y where the ((I+V^TZ)^-1)*V^Ty part is already calculated and currently in vty... so now we have to do a matrix vector product and add the whole thing to y
                          //here we do the matrix_vector product to calculate Z*vty, we use our own function Block_vec_vec_product here because we know how the product looks
                          Block_vec_vec_product(U,vty,vec_to_add,N_comp,N_comp*Nz_calc);
                         
                          
                          
                          //Now we finally subtract the vector that is the result of the SWM formula from the vector y to get the final solution of our periodic boundary problem
                          temp_lim=N_comp*Nz_calc;
                          for(k=0;k<temp_lim;k++){
                            //This is eq. 3 of the SWM in the Solving_periodic systems paper basically the combination of y and z  
                            RHS_iter[i][j][k]=RHS_iter[i][j][k]-vec_to_add[k];                             
                          }
                          
                          

                          
                          
                          
                          for(n=0;n<N_comp;n++){ 
                            for(k=0;k<Nz_calc;k++) Conc_next[n][i+1][j+1][k+1]=RHS_iter[i][j][n+k*N_comp];    //Write results in Conc_next (basically result of this iteration)
                          }
                          
                        }
                      
                      }
                      
                     
                      
                      
                      
                    for(n=0;n<N_comp;n++){  
                    //THE WRAPAROUND PARTS OF CONC HAVE TO BE FILLED TOO, that happens here!
                    //For the multiple component version we had to replace RHS_reorg with RHS_iter... which has different dimensions ... might be a bug position in case the indices have been messed up
                        //x direction
                        for(j=0;j<Ny_calc;j++){
                            for(k=0;k<Nz_calc;k++){
                                Conc_next[n][0][j+1][k+1]=RHS_iter[Nx_calc-1][j][n+k*N_comp]; //Double periodic filling of the conc array 
                                Conc_next[n][Nx_calc+1][j+1][k+1]=RHS_iter[0][j][n+k*N_comp];
                                
                                
                            }
                        }
                    
                        //y direction
                        for(i=0;i<Nx_calc;i++){
                            for(k=0;k<Nz_calc;k++){
                                Conc_next[n][i+1][0][k+1]=RHS_iter[i][Ny_calc-1][n+k*N_comp]; //Double periodic filling of the conc array 
                                Conc_next[n][i+1][Ny_calc+1][k+1]=RHS_iter[i][0][n+k*N_comp];
                                
                                
                            }
                        }
                        //z direction
                          for(i=0;i<Nx_calc;i++){
                            for(j=0;j<Ny_calc;j++){
                                Conc_next[n][i+1][j+1][0]=RHS_iter[i][j][n+(Nz_calc-1)*N_comp]; //Double periodic filling of the conc array 
                                Conc_next[n][i+1][j+1][Nz_calc+1]=RHS_iter[i][j][n+0*N_comp];
                                
                                
                            }
                        }
                        
                          //Here we fill the edges of the cube with the face cells from the diagonally opposite side
                          //along x -direction ... these are 4 edges running along the x-axis but without the corners
                          for(i=0;i<Nx_calc;i++){
                            Conc_next[n][i+1][0][0]=RHS_iter[i][Ny_calc-1][n+(Nz_calc-1)*N_comp];
                            Conc_next[n][i+1][Ny_calc+1][0]=RHS_iter[i][0][n+(Nz_calc-1)*N_comp];
                            Conc_next[n][i+1][0][Nz_calc+1]=RHS_iter[i][Ny_calc-1][n+0*N_comp];
                            Conc_next[n][i+1][Ny_calc+1][Nz_calc+1]=RHS_iter[i][0][n+0*N_comp];
                            
                            
                            }
                            
                          //along y -direction ... these are 4 edges running along the y-axis but without the corners
                          for(i=0;i<Ny_calc;i++){
                            Conc_next[n][0][i+1][0]=RHS_iter[Nx_calc-1][i][n+(Nz_calc-1)*N_comp];
                            Conc_next[n][Nx_calc+1][i+1][0]=RHS_iter[0][i][n+(Nz_calc-1)*N_comp];
                            Conc_next[n][0][i+1][Nz_calc+1]=RHS_iter[Nx_calc-1][i][n+0*N_comp];
                            Conc_next[n][Nx_calc+1][i+1][Nz_calc+1]=RHS_iter[0][i][n+0*N_comp];
                            
                            
                            }
                        
                          //along z -direction ... these are 4 edges running along the x-axis but without the corners
                          for(i=0;i<Nz_calc;i++){
                            Conc_next[n][0][0][i+1]=RHS_iter[Nx_calc-1][Ny_calc-1][n+i*N_comp];
                            Conc_next[n][Nx_calc+1][0][i+1]=RHS_iter[0][Ny_calc-1][n+i*N_comp];
                            Conc_next[n][0][Ny_calc+1][i+1]=RHS_iter[Nx_calc-1][0][n+i*N_comp];
                            Conc_next[n][Nx_calc+1][Ny_calc+1][i+1]=RHS_iter[0][0][n+i*N_comp];
                            
                            
                            }
                            
                         // Now only the 8 corner cells have to be periodized 
                         Conc_next[n][0][0][0]=RHS_iter[Nx_calc-1][Ny_calc-1][n+(Nz_calc-1)*N_comp];
                         
                         Conc_next[n][Nx_calc+1][0][0]=RHS_iter[0][Ny_calc-1][n+(Nz_calc-1)*N_comp];   
                         Conc_next[n][0][Ny_calc+1][0]=RHS_iter[Nx_calc-1][0][n+(Nz_calc-1)*N_comp];
                         Conc_next[n][0][0][Nz_calc+1]=RHS_iter[Nx_calc-1][Ny_calc-1][n+0*N_comp];  
                    
                         Conc_next[n][0][Ny_calc+1][Nz_calc+1]=RHS_iter[Nx_calc-1][0][n+0*N_comp];   
                         Conc_next[n][Nx_calc+1][0][Nz_calc+1]=RHS_iter[0][Ny_calc-1][n+0*N_comp];
                         Conc_next[n][Nx_calc+1][Ny_calc+1][0]=RHS_iter[0][0][n+(Nz_calc-1)*N_comp];
                         
                         Conc_next[n][Nx_calc+1][Ny_calc+1][Nz_calc+1]=RHS_iter[0][0][n+0*N_comp];
                    
                      
                  
                  
                    }
                  
                  //Write result of iteration in current Concentration matrix (Conc_now) after checking f iteration can be abborted prematurely (the checking part is not yet implemented!) Also should check if any off the values is unrealistic (C_DA <0 or C_D1 > C_D1_max, C_D2> C_D2_max)
                  for(n=0;n<N_comp;n++){
                    for(i=0;i<Nx_calc+2;i++){
                        for(j=0;j<Ny_calc+2;j++){                  
                            for(k=0;k<Nz_calc+2;k++){
                                //Here we check if any off the values is larger than the maximum allowed (for receptors... or smaller than 0 (no negative concentrations))
                                if (Conc_next[n][i][j][k]>Component_max[n]) Conc_now[n][i][j][k]=Component_max[n];
                                else {
                                    if(Conc_next[n][i][j][k]<0) Conc_now[n][i][j][k]=0;
                                    else Conc_now[n][i][j][k]=Conc_next[n][i][j][k];
                                }
                                
                                }
                            }
                        }
                    }
                    
                }
              
        free(vty); 
        free(IVTZ);
        free(IVTZ_temp);
        free(IPIV_short);
        free(vec_to_add);
        free(temp_matrix);
   
		return 0;     
}


int Fill_Diff_Mat_iter(int i,int j,int N,double * Diff_Mat_iter,double **** Conc_now,double**** Jacobi,double **r,double dt, int Nr_comp){
    /*Here we fill the Diff_Mat_iter array with the column major (because using lapack) ordered version of our matrix in the lapack band storage scheme.  
     *The filled DIFF_Mat_iter array will have the matrix T for periodic boundary conditions (See Sherman Morrison Woodsbury  formula. Paper: "solving periodic
     * block tridiagonal systems using SMW formula 1989". Basically we have to modify the original matrix for solving the periodic case.) 
     * */
    
    int k,ix,iy,l,m;
    double dt_1_3,dt_24;
    int ab_offset,current_upper_offset,current_lower_offset,main_offset,end_offset;
    int Nr_diagonals;
    int J_dist;
    int size_DMI;
    
    size_DMI=(3*((2*Nr_comp)-1)+1)*N*Nr_comp;
    for(l=0;l<size_DMI;l++){
        Diff_Mat_iter[l]=0;
        }
    
    
    ix=i+1; //offset because of periodical solutions
    iy=j+1;
    
    dt_1_3=(1.0/3.0)*dt;             //these are calculated once and then reused
    dt_24=(1.0/24.0)*dt;
    
    J_dist=Nr_comp+1;           //distance of 2 components on the same diagonal in the jacobi matrix
    
    
    Nr_diagonals=2*Nr_comp-1; //Number of upper and lower diagonals (for our matrix this is the same always)
    ab_offset=Nr_diagonals*(Nr_comp*N);  //rows 1 to KL are empty, that is needed for the solver... so all entries are offset by this. (2*Nr_comp-1) is KL and NR_comp*N is the length of a row


    
    
    main_offset=2*ab_offset; //the main diagonal is 2*KL away from the beginning off the array (first KL rows are empty and then the KU rows come)
    for(l=1;l<N+1;l++){   //Fill the main diagonal; Since the jacobi array and things have a size that is larger than the domain (for periodic boundaries) we start counting from 1 to N+1, so that we don't have to add +1 to each l value... but only if we actually shift
        for(k=0;k<Nr_comp;k++){
            Diff_Mat_iter[main_offset+Nr_comp*(l-1)+k]=1.0-(1.0/6.0-r[k][ZVAR])-dt_1_3*Jacobi[ix][iy][l][k*(J_dist)]-dt_24*(Jacobi[ix][iy][l+1][k*(J_dist)]+Jacobi[ix][iy][l-1][k*(J_dist)]); //last term is diagonal elements of jacobi shifted to z+1 and z-1 positions; k*Nr_comp+k selects the 0,4,and 8th element off the jacobi... respectively coding for J11,J21 and J31 (elements on the diagonal)
        
            }
        }
    
    /* This part fills the off diagonals that have elements from blocks that are on the block main diagonal */
    for(m=1;m<Nr_comp;m++){    //First fills the upper off diagonals that have at least one element off the block on the main diagonal
        current_upper_offset=main_offset-m*(Nr_comp*N)+m;               //offset for the upper diagonal, the +m is for the shift off the upper diagonals according to the LAPACK saving strategy
        current_lower_offset=main_offset+m*(Nr_comp*N);
        
        for(k=0;k<Nr_comp-m;k++){   //these are components on this off-diagonal that are in the "block" on the main diagonal
            for(l=1;l<N+1;l++){   //put the larger loop farther down in the nested loops to minimize overhead... that is probably very premature optimization... but doesn't really complicate things
                Diff_Mat_iter[current_upper_offset+Nr_comp*(l-1)+k]=-dt_1_3*(Jacobi[ix][iy][l][m+(J_dist)*k])-dt_24*(Jacobi[ix][iy][l+1][m+J_dist*k]+Jacobi[ix][iy][l-1][m+J_dist*k]);
                Diff_Mat_iter[current_lower_offset+Nr_comp*(l-1)+k]=-dt_1_3*(Jacobi[ix][iy][l][(m*Nr_comp)+(J_dist)*k])-dt_24*(Jacobi[ix][iy][l+1][(m*Nr_comp)+J_dist*k]+Jacobi[ix][iy][l-1][(m*Nr_comp)+J_dist*k]);
                
                //I hope to write a piece of documentation explaining how the jacobi indices follow from the diagonal (m) and position in diagonal (k) index ... if this does not exist yet... sry :/
                
            }
        }
        
    
        for(k=Nr_comp-1;k>Nr_comp-m-1;k--){ //These are components on this off diagonal that are in an off-diagonal block 
            for(l=1;l<N;l++){ //here it is 1 shorter (instead to N+1 it only goes to N) since the off diagonal blocks are one block shorter   
                //put the larger loop farther down in the nested loops to minimize overhead... that is probably very premature optimization... but doesn't really complicate things
                
                    Diff_Mat_iter[current_upper_offset+Nr_comp*(l-1)+k]=-dt_24*(Jacobi[ix][iy][l][(k*J_dist)-(Nr_comp-m)]);      //these are parts on the off diagonal in blocks that are not in the block-diagonal
                    Diff_Mat_iter[current_lower_offset+Nr_comp*(l-1)+k]=-dt_24*(Jacobi[ix][iy][l+1][(k*J_dist)-(Nr_comp*(Nr_comp-m))]);                 //here we use l+1 because the values are already from the next "block-row" meaning the next position for J
            }
            
            
        }
        
            
    }
        
    /* This part fills the off diagonals that only have elements from blocks that are on the block off diagonal */
    //We start by filling the main diagonal off the off-diagonal blocks (these have extra components )
    current_upper_offset=main_offset-Nr_comp*(Nr_comp*N)+(Nr_comp);
    current_lower_offset=main_offset+Nr_comp*(Nr_comp*N);
    for(k=0;k<Nr_comp;k++){
        for(l=1;l<N;l++){   //again this loop deals with off diagonal blocks... so it has to be shorter
            Diff_Mat_iter[current_upper_offset+Nr_comp*(l-1)+k]=(1.0/12.0-0.5*r[k][ZVAR])-dt_24*(Jacobi[ix][iy][l][k*J_dist]);
            Diff_Mat_iter[current_lower_offset+Nr_comp*(l-1)+k]=(1.0/12.0-0.5*r[k][ZVAR])-dt_24*(Jacobi[ix][iy][l+1][k*J_dist]);
            }
        }
      
      
      
      
      
      for(m=1;m<Nr_comp;m++){   /*These are the off diagonals that don't have elements on the Main block diagonal... there are Nr_comp off those ... 
      technically this should be Nr_comp to 2*Nr_comp but it's easier for the logic to count from 0 to this... we change the current offset to reflect this though */
          current_upper_offset=main_offset-m*(Nr_comp*N)-Nr_comp*(Nr_comp*N)+(Nr_comp)+m; //Here we add Nr_comp*(Nr_comp*N) which skips the first Nr_comp upper off diagonals  
          current_lower_offset=main_offset+m*(Nr_comp*N)+Nr_comp*(Nr_comp*N); 
           
           for(k=0;k<Nr_comp-m;k++){
               for(l=1;l<N;l++){
                   Diff_Mat_iter[current_upper_offset+Nr_comp*(l-1)+k]=-dt_24*(Jacobi[ix][iy][l][(k*J_dist)+m]);
                   Diff_Mat_iter[current_lower_offset+Nr_comp*(l-1)+k]=-dt_24*(Jacobi[ix][iy][l+1][(k*J_dist)+(m*Nr_comp)]);
                }
            }
          
          
        }
        
        
       /*Now the matrix is filled fully, but we have to subtract the Blocks A1, and CN (see paper) from the matrix. This is done here.
        * This step is only important for periodic boundaries. If you wan't to do non periodic boundaries you don't need to do this step!*/ 
        
        
        /*Subtract from the main diagonal off the first and last main diagonal blocks */
        end_offset=main_offset+Nr_comp*(N-1);   //calculates the offset to the main diagonal off the last block in the matrix P
        for(k=0;k<Nr_comp;k++){
        /* Subtract the diagonal */
            Diff_Mat_iter[main_offset+k]-=(1.0/12.0-0.5*r[k][ZVAR])-dt_24*(Jacobi[ix][iy][1][(k*J_dist)]);               //subtract the elemnts off the main diagonal in A1 from the 1st block on the block main diagonal (according to SWM)
            Diff_Mat_iter[end_offset+k]-=(1.0/12.0-0.5*r[k][ZVAR])-dt_24*(Jacobi[ix][iy][N+1][(k*J_dist)]);
            /*The jacobi matrices are for the coordinate off the block main diagonal. */
            
        }
        
        /*  subtract from the off diagonals off the 1st and last main diagonal blocks*/
        for(m=1;m<Nr_comp;m++){    //First fills the upper off diagonals that have at least one element off the block on the main diagonal
            current_upper_offset=main_offset-m*(Nr_comp*N)+m;               //offset for the upper diagonal
            current_lower_offset=main_offset+m*(Nr_comp*N);
            
            
            for(k=0;k<Nr_comp-m;k++){   //these are components on this off-diagonal that are in the "block" on the main diagonal
                    /* This if for l=0 meaning the first block on the block main diagonal*/
                    Diff_Mat_iter[current_upper_offset+k]+=dt_24*Jacobi[ix][iy][1][m+(J_dist)*k];  //it's += because we subtract -dt24() from the value and -- is +
                    Diff_Mat_iter[current_lower_offset+k]+=dt_24*Jacobi[ix][iy][1][(m*Nr_comp)+(J_dist)*k];
                    
                    /* This if for l=N+1 meaning the last block on the block main diagonal*/
                    Diff_Mat_iter[current_upper_offset+(N-1)*Nr_comp+k]+=dt_24*Jacobi[ix][iy][N+1][m+(J_dist)*k];
                    Diff_Mat_iter[current_lower_offset+(N-1)*Nr_comp+k]+=dt_24*Jacobi[ix][iy][N+1][(m*Nr_comp)+(J_dist)*k];
                    //I hope to write a piece of documentation explaining how the jacobi indices follow from the diagonal (m) and position in diagonal (k) index ... if this does not exist yet... sry :/
                    

                
            }
        
        
        

    
    
        }
return 0;
}





int Fill_Jacobi(int N_comp,int Nx,int Ny,int Nz,double **** Conc_now,double **** Jacobi,struct Fct_param * F_param,double ****V_spatial){
    //This function also depends on the function that is supposed to describe the reaction 
    int i,j,k;
    
    double K_m = (F_param->K_m);
    double k_unspecific= (F_param->k_unspecific);
    double k_on_D1= (F_param->k_on_D1);
    double k_on_D2= (F_param->k_on_D2);
    double k_off_D1= (F_param->k_off_D1);
    double k_off_D2= (F_param->k_off_D2);
    double C_D1_max= (F_param->C_D1_max);
    double C_D2_max= (F_param->C_D2_max);
    double a;
        
        
        
        //Spatially varying MM uptake in V_spatial
        //With linear unspecific Uptake, that is spatially homogenous k_unspecific
        for(i=0;i<Nx;i++){
            for(j=0;j<Ny;j++){
                for(k=0;k<Nz;k++){
                //First row of Jacobi matrix
                //J11
                a=V_spatial[0][i][j][k]*K_m;
                Jacobi[i][j][k][0]=a/((K_m+Conc_now[0][i][j][k])*(K_m+Conc_now[0][i][j][k]));   //derivative of MM term
                Jacobi[i][j][k][0]+=k_unspecific;                                               //derivative of unspecific uptake term k*C
                Jacobi[i][j][k][0]-=k_on_D1*(C_D1_max-Conc_now[1][i][j][k]); 
                Jacobi[i][j][k][0]-=k_on_D2*(C_D2_max-Conc_now[2][i][j][k]);
                //J12
                 Jacobi[i][j][k][1]=k_on_D1*Conc_now[0][i][j][k]+k_off_D1;
                //J13
                Jacobi[i][j][k][2]=k_on_D2*Conc_now[0][i][j][k]+k_off_D2;
                
                //Second row of Jacobi
                //J21
                Jacobi[i][j][k][3]=k_on_D1*(C_D1_max-Conc_now[1][i][j][k]);
                //J22
                Jacobi[i][j][k][4]=-k_on_D1*Conc_now[0][i][j][k]-k_off_D1;
                //J23
                Jacobi[i][j][k][5]=0;
                
                //Third row of Jacobi
                //J31
                Jacobi[i][j][k][6]=k_on_D2*(C_D2_max-Conc_now[2][i][j][k]);
                //J32
                Jacobi[i][j][k][7]=0;
                //J33
                Jacobi[i][j][k][8]=-k_on_D2*Conc_now[0][i][j][k]-k_off_D2;
                
                }
            }
        }
    
    
return 0; 
}





int Fill_block_vector_U(int i,int j,int N,double * U,double **** Conc_now,double**** Jacobi,double **r,double dt, int Nr_comp){
/* Here the block vector U from the SWM formula is filled
 * That means we will have block A1 in the first block off this vector and Block Cn in the last block ... the rest will be 0*/
int k,m;
int J_dist,ix,iy;
int length_column;
int size_U;
double dt_24;

length_column=N*Nr_comp; //length of a column of U, since U is a vector of size mn*m
size_U=length_column*Nr_comp;
ix=i+1;
iy=j+1;
dt_24=dt*(1.0/24.0);


J_dist=Nr_comp+1;
for (k=0;k<size_U;k++) U[k]=0;  //zero the vector

        for(k=0;k<Nr_comp;k++){
        /* Main diagonals of block A1 and CN in SWM formula */
            U[k+length_column*k]+=(1.0/12.0-0.5*r[k][ZVAR])-dt_24*(Jacobi[ix][iy][1][(k*J_dist)]);               
            U[k+length_column*k+length_column-Nr_comp]+=(1.0/12.0-0.5*r[k][ZVAR])-dt_24*(Jacobi[ix][iy][N+1][(k*J_dist)]);
            
        }

        for(m=1;m<Nr_comp;m++){    //Now we fill the off diagonals
            for(k=0;k<Nr_comp-m;k++){
                /* Upper diagonals in the blocks*/
                U[k+length_column*(k+m)]=-dt_24*Jacobi[ix][iy][1][m+(J_dist)*k];                        //Block A1 
                U[k+length_column*(k+m)+length_column-Nr_comp]=-dt_24*Jacobi[ix][iy][N+1][m+(J_dist)*k];            //Block CN
           
                /* Lower diagonals in the blocks*/
                U[k+length_column*k+m]=-dt_24*Jacobi[ix][iy][1][(m*Nr_comp)+(J_dist)*k];               //Block A1
                U[k+length_column*k+length_column-Nr_comp+m]=-dt_24*Jacobi[ix][iy][N+1][(m*Nr_comp)+(J_dist)*k];   //Block CN
           
            }
        }
return 0;
}


int Transpose_matrix(double * matrix,double* matrix_temp,int linelength,int ldab){
    //Transposes the Fortran array by taking the A[i,j] element and putting it in the A[j,i] element;
    int i,j;
    
    

      
    for(i=0;i<linelength*ldab;i++) matrix_temp[i]=matrix[i];  //copy the input matrix to matrix_in ... could probably use memcpy but whatever
    
    /* Now we transpose since the array is currently organized [[linelength],[linelength],[linelength],[linelength]]
     * but should be [[ldab][ldab][ldab][ldab]]*/
     
     for(i=0;i<linelength;i++){
         for(j=0;j<ldab;j++){
             matrix[i*ldab+j]=matrix_temp[j*linelength+i];
             
             
             }
         
         
         }
    
return 0;    
}
