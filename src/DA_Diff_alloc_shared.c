/*
 * 
 * 
 * This file DA_Diff_alloc_shared.c contains all the memory allocation functions used by  DOPE-AMINE
 * that are shared for the one component and multiple component solver.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */

#include "DA_Diff_macros_structs_shared.h"
#include "DA_Diff_alloc_shared.h"
#include <stdlib.h>


double**** allocate_Conc(int N_comp, int Nx, int Ny, int Nz)
{   
    int n,i,j;
    double **** Conc;
    
    Conc = (double****) calloc(N_comp, sizeof(double ***));
    for(n=0;n<N_comp;n++){
        Conc[n]=(double***) calloc(Nx, sizeof(double **));
        for(i=0;i<Nx;i++){
            Conc[n][i]=(double**) calloc(Ny, sizeof(double *));
            for(j=0;j<Ny;j++){
                Conc[n][i][j]=(double*) calloc(Nz, sizeof(double));
                
                }
            }
        }
    
    return Conc;
}
    

int deallocate_Conc(double**** Conc,int N_comp,int Nx,int Ny,int Nz)
{
    int n,i,j;
    for(n=0;n<N_comp;n++){
        for(i=0;i<Nx;i++){
            for(j=0;j<Ny;j++){
                free(Conc[n][i][j]);
                }
            free(Conc[n][i]);
            }
            free(Conc[n]);
        }
    free(Conc);
    return 0;
}

  
double** allocate_D(int N_comp)
{
    int n;
    double **D;
    
    D= (double**) calloc(N_comp, sizeof(double *));
    for(n=0;n<N_comp;n++){
        D[n]=(double*) calloc(3, sizeof(double));
        }
    
    return D;
}


int deallocate_D(double** D,int N_comp)
{
    int n;
    for(n=0;n<N_comp;n++){
        free(D[n]);
        }
    free(D);
    
    return 0;
}
    
    
    
    
    
    
    
double**** allocate_Diff_Mat(int N_comp, int Nx, int Ny, int Nz)
{   
    int n;
    double **** Diff_Mat;
    
    Diff_Mat = (double****) calloc(N_comp, sizeof(double ***));
    for(n=0;n<N_comp;n++){
        Diff_Mat[n]=(double***) calloc(3, sizeof(double **));
        Diff_Mat[n][XVAR]=(double**) calloc(4, sizeof(double *)); //the necesarry 4 parts of the DIAG matrix for the LAPACK call
        Diff_Mat[n][YVAR]=(double**) calloc(4, sizeof(double *));
        Diff_Mat[n][ZVAR]=(double**) calloc(4, sizeof(double *));
        
        Diff_Mat[n][XVAR][DIAG]=(double*) calloc(Nx, sizeof(double));
        Diff_Mat[n][XVAR][DIAG_L]=(double*) calloc(Nx-1, sizeof(double));
        Diff_Mat[n][XVAR][DIAG_U]=(double*) calloc(Nx-1, sizeof(double));
        Diff_Mat[n][XVAR][DIAG_U2]=(double*) calloc(Nx-2, sizeof(double));
        
        Diff_Mat[n][YVAR][DIAG]=(double*) calloc(Ny, sizeof(double));
        Diff_Mat[n][YVAR][DIAG_L]=(double*) calloc(Ny-1, sizeof(double));
        Diff_Mat[n][YVAR][DIAG_U]=(double*) calloc(Ny-1, sizeof(double));
        Diff_Mat[n][YVAR][DIAG_U2]=(double*) calloc(Ny-2, sizeof(double));
        
        Diff_Mat[n][ZVAR][DIAG]=(double*) calloc(Nz, sizeof(double));
        Diff_Mat[n][ZVAR][DIAG_L]=(double*) calloc(Nz-1, sizeof(double));
        Diff_Mat[n][ZVAR][DIAG_U]=(double*) calloc(Nz-1, sizeof(double));
        Diff_Mat[n][ZVAR][DIAG_U2]=(double*) calloc(Nz-2, sizeof(double));
        
        }
    return Diff_Mat;
}
    
int deallocate_Diff_Mat(double **** Diff_Mat,int N_comp,int Nx,int Ny,int Nz)
{
    int i,n;
    
    for(n=0;n<N_comp;n++){
         for(i=0;i<4;i++) free(Diff_Mat[n][XVAR][i]);
         for(i=0;i<4;i++) free(Diff_Mat[n][YVAR][i]);
         for(i=0;i<4;i++) free(Diff_Mat[n][ZVAR][i]);
         free(Diff_Mat[n][XVAR]);
         free(Diff_Mat[n][YVAR]);
         free(Diff_Mat[n][ZVAR]);
         free(Diff_Mat[n]);
        }
    free(Diff_Mat);
    return 0;
}
    
    
int*** allocate_IPIV(int N_comp,int Nx,int Ny,int Nz)
{
    int n;
    int ***IPIV;
    
    IPIV= (int***) calloc(N_comp, sizeof(int **));
    for(n=0;n<N_comp;n++){
        IPIV[n]=(int**) calloc(3, sizeof(int*));
        IPIV[n][XVAR]=(int*) calloc(Nx, sizeof(int)); //the necesarry 4 parts of the DIAG matrix for the LAPACK call
        IPIV[n][YVAR]=(int*) calloc(Ny, sizeof(int));
        IPIV[n][ZVAR]=(int*) calloc(Nz, sizeof(int));
        
        
        }
    
    return IPIV;
}

    
int deallocate_IPIV(int *** IPIV,int N_comp,int Nx,int Ny,int Nz)
{
    int n;
    
    for(n=0;n<N_comp;n++){
        
        free(IPIV[n][XVAR]);
        free(IPIV[n][YVAR]);
        free(IPIV[n][ZVAR]);
        free(IPIV[n]);
        }
    free(IPIV);
    return 0;
}
    
    
double**** allocate_Jacobi(int N_comp,int Nx, int Ny, int Nz)
{   
    int i,j,k;
    double **** Jacobi;
    
    Jacobi = (double****) calloc(Nx, sizeof(double ***));
    for(i=0;i<Nx;i++){
        Jacobi[i]=(double***) calloc(Ny, sizeof(double **));
        for(j=0;j<Ny;j++){
            Jacobi[i][j]=(double**) calloc(Nz, sizeof(double *));
            for(k=0;k<Ny;k++){
                Jacobi[i][j][k]=(double*) calloc(N_comp*N_comp, sizeof(double));
                
                }
            }
        }
        
    return Jacobi;
}
    

int deallocate_Jacobi(double**** Jacobi,int N_comp,int Nx,int Ny,int Nz)
{
    int i,j,k;
    for(i=0;i<Nx;i++){
        for(j=0;j<Ny;j++){
            for(k=0;k<Nz;k++){
                free(Jacobi[i][j][k]);
                }
            free(Jacobi[i][j]);
            }
            free(Jacobi[i]);
        }
    free(Jacobi);
    return 0;
}
    


double*** allocate_RHS_iter(int Nx,int Ny,int Nz)
{
    int i,j;
    double*** RHS_1;
    

        RHS_1=(double***) calloc(Nx, sizeof(double **));
        for(i=0;i<Nx;i++){
            RHS_1[i]=(double**) calloc(Ny, sizeof(double *));
            for(j=0;j<Ny;j++){
                RHS_1[i][j]=(double*) calloc(Nz, sizeof(double));
                
                }
            }
        
     return RHS_1;
}



int deallocate_RHS_iter(double*** RHS_1,int Nx,int Ny,int Nz)
{
    int i,j;
        for(i=0;i<Nx;i++){
            for(j=0;j<Ny;j++){
                free(RHS_1[i][j]);
                }
            free(RHS_1[i]);
            }
            free(RHS_1);
        

    return 0;
}
