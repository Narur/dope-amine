/*
 * 
 * 
 * This file DA_Diff_Solver_fct_1_comp.c is the file containing all functions 
 * that are used by the 1 component solver of the diffusion code DOPE-AMINE only.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */

#include "DA_Diff_macros_structs_shared.h"
#include "DA_Diff_macros_structs_1_comp.h"
#include "DA_Diff_LAPACK.h"
#include "DA_Diff_Solver_fct_1_comp.h"
#include <math.h>

int Create_RHS_w_Fct(int N_comp,int Nx_calc,int Ny_calc,int Nz_calc,double **** RHS_1, double **** RHS_reorg,double **** Conc_now,double **** fct_grid,double **** Jacobi,double dt){
    //This function also depends on the Reaction function, if the reaction function is time dependent then there needs to be a partial time derivative term here... I skipped that ... but be aware
    // this also has the Jacobi Matrix in it... so that also depends on the function in the reaction term
    int i,j,k,n;
    int ix,iy,iz;
    double JW;
    
        for(n=0;n<N_comp;n++){
        
        for(i=0;i<Nx_calc;i++){
            for(j=0;j<Ny_calc;j++){
                for(k=0;k<Nz_calc;k++){
                    ix=i+1;
                    iy=j+1;
                    iz=k+1;
                    
                    
                    RHS_reorg[n][i][j][k]=RHS_1[n][i][j][k]+dt*0.5*(fct_grid[n][ix][iy][iz]-(Jacobi[ix][iy][iz][0]*Conc_now[0][ix][iy][iz]));    //the term without dz2
                    // JW with product rule:
                    JW=((Jacobi[ix][iy][iz-1][0]-2.0*Jacobi[ix][iy][iz][0]+Jacobi[ix][iy][iz+1][0])*Conc_now[0][ix][iy][iz])+Jacobi[ix][iy][iz][0]*(Conc_now[0][ix][iy][iz-1]-2.0*Conc_now[0][ix][iy][iz]+Conc_now[0][ix][iy][iz+1]);                                                                                                  //decides how dz^2 J*w is evaluated, this one is product rule  
                    RHS_reorg[n][i][j][k]+=dt*0.5*(1.0/12.0)*(fct_grid[n][ix][iy][iz-1]-2.0*fct_grid[n][ix][iy][iz]+fct_grid[n][ix][iy][iz+1]-JW);                                                                                                                                         
                    
                    
                    
                    }
                }
            }
        }
return 0;
}






int Calc_fct_grid(int N_comp, int Nx, int Ny, int Nz,double **** Conc_now, double **** fct_grid,struct Fct_param * F_param, double **** V_spatial){
    //This function has to be changed for each system we look at ... 
    int i,j,k;
    
    double K_m = (F_param->K_m);
    double k_unspecific = (F_param->k_unspecific);

    
    
            
            //This is automatically periodic if Conc is periodic since it calculates it value depoending on only the value in the conc cell
            for(i=0;i<Nx;i++){
                for(j=0;j<Ny;j++){
                    for(k=0;k<Nz;k++){
                        //We use V_spatial since the MM uptake is not spatially homogenous 
                        //We also added a value for unspecific uptake (this is spatially homogenous here but can be made spatially varying later)
                        fct_grid[0][i][j][k]=(V_spatial[0][i][j][k]*Conc_now[0][i][j][k])/(K_m+Conc_now[0][i][j][k]);   //MM uptake
                        fct_grid[0][i][j][k]+=k_unspecific*Conc_now[0][i][j][k];         //linear unspecific uptake
              
                    }
                }
            }
            
            return 0;
    



}


int Advance_t(int N_comp,int Nx_calc,int Ny_calc,int Nz_calc,double ****RHS_1,double **** RHS_reorg, struct D_system * Dsys,double **** fct_grid,double ****Conc_now,double ****Conc_next,double ****Jacobi, double ** Diff_Mat_iter,double dt,struct Fct_param * F_param,double ****V_spatial){
    int i,j,k,n,l;
    int info,ldb,NRHS;
    char TRANS;
    double a,b;
    int iterations;
    
    double** r = (Dsys->r);
    double**** Diff_Mat = (Dsys->Diff_Mat);
    int*** IPIV = (Dsys->IPIV);
    double** x_periodic_sol=(Dsys->x_periodic_sol);  //has the solution of the part that doesn't change in it (x(2) for periodic boundaries) x_coor
    double** y_periodic_sol=(Dsys->y_periodic_sol);
    double** z_periodic_sol=(Dsys->z_periodic_sol);
    double Component_max=(Dsys->Component_max);

    int Max_iterations=(Dsys->Max_iterations);
    
    
        //printf("Timestep starts here! \n");
        //printf("Before first step, RHS[0][52][51][51]: %.5g \n",RHS_1[0][52][51][51]);
            
        //Reorganize the data in the RHS array so that the last dimension has the arrays of x ... so that it can be passed to the LAPACK routine  
        for(n=0;n<N_comp;n++){
            for(i=0;i<Nx_calc;i++){
                for(j=0;j<Ny_calc;j++){
                    for(k=0;k<Nz_calc;k++){  
                        RHS_reorg[n][j][k][i]=RHS_1[n][i][j][k];
                        
                    }
                }
            }
        }

        
        //Use dgttrs to solve equation 38 afterwards RHS_reorg will have the solution to 38 which is w**
        ldb=Nx_calc-1;  //this is the size of the reduced system now-needs to be solved first
        NRHS=1;
        TRANS='N';
        for(n=0;n<N_comp;n++){
              b=(1.0/(12.0))-((r[n][XVAR])/(2.0));  //off diagonal element of diff_mat
              a=1.0-(1.0/(6.0))+(r[n][XVAR]);                //a is main diagonal element of DIff_mat
              
              for(j=0;j<Ny_calc;j++){
                  for(k=0;k<Nz_calc;k++){
                      //this step should solve for x(1)
                      
                      dgttrs_( &TRANS, &ldb, &NRHS, Diff_Mat[n][XVAR][DIAG_L] , Diff_Mat[n][XVAR][DIAG], Diff_Mat[n][XVAR][DIAG_U], Diff_Mat[n][XVAR][DIAG_U2], IPIV[n][XVAR], RHS_reorg[n][j][k], &ldb, &info);
                      //Here I hope with ldb set to NX_reduced... that the wraparound cells are untouched during the process. -> If the solution looks funky the problem could be here
                      
                      //calculate value of wraparound cell and save it in RHS_reorg
                      RHS_reorg[n][j][k][Nx_calc-1]= (RHS_reorg[n][j][k][Nx_calc-1]-b*(RHS_reorg[n][j][k][0]+RHS_reorg[n][j][k][Nx_calc-2]))/(a+b*(x_periodic_sol[n][0]+x_periodic_sol[n][Nx_calc-2]));   //since our off diagonal elements are the same we can combine them for calculating X N+1
                      
                      //Correct solution of the reduced system to have the full solution in RHS_reorg
                      for(i=0;i<Nx_calc-1;i++){
                            RHS_reorg[n][j][k][i]+=x_periodic_sol[n][i]*RHS_reorg[n][j][k][Nx_calc-1];
                         }

                    }
                  
                  }
              
              
              
        }
    
    
    // !!!!!!!!!!!!!!!Solve second step !!!!!!!!!!!!!!
    //Reorganize so that RHS is again in oder x y z (there seem to be problems with reordering on the fly... so we try this now)
        for(n=0;n<N_comp;n++){
            for(i=0;i<Nx_calc;i++){
                for(j=0;j<Ny_calc;j++){
                    for(k=0;k<Nz_calc;k++){  
                        RHS_1[n][i][j][k]=RHS_reorg[n][j][k][i]; //reorganize to starting position

                    }
                }
            }
        }
            


        

            
        //Reorganize so that RHS is in oder x z y (there seem to be problems with reordering on the fly... so we try this now)
        for(n=0;n<N_comp;n++){
            
            for(i=0;i<Nx_calc;i++){
                for(j=0;j<Ny_calc;j++){
                    for(k=0;k<Nz_calc;k++){  
         
                        RHS_reorg[n][i][k][j]=RHS_1[n][i][j][k]; //reorganize to starting position
                    }
                }
            }
        }
        
    
    
    //use dgttrs again o solve eq. 39 to get w*
        ldb=Ny_calc-1;
        NRHS=1;
        TRANS='N';
        for(n=0;n<N_comp;n++){
              b=(1.0/(12.0))-((r[n][YVAR])/(2.0));  //off diagonal element of diff_mat
              a=1.0-(1.0/(6.0))+(r[n][YVAR]);                //a is main diagonal element of DIff_mat
            
              for(i=0;i<Nx_calc;i++){
                  for(k=0;k<Nz_calc;k++){
                      //this step should solve for x(1)
                      dgttrs_( &TRANS, &ldb, &NRHS, Diff_Mat[n][YVAR][DIAG_L] , Diff_Mat[n][YVAR][DIAG], Diff_Mat[n][YVAR][DIAG_U], Diff_Mat[n][YVAR][DIAG_U2], IPIV[n][YVAR], RHS_reorg[n][i][k], &ldb, &info);
                      //Here I hope with ldb set to NY_reduced... that the wraparound cells are untouched during the process. -> If the solution looks funky the problem could be here

                     //calculate value of wraparound cell and save it in RHS_reorg
                      RHS_reorg[n][i][k][Ny_calc-1]= (RHS_reorg[n][i][k][Ny_calc-1]-b*(RHS_reorg[n][i][k][0]+RHS_reorg[n][i][k][Ny_calc-2]))/(a+b*(y_periodic_sol[n][0]+y_periodic_sol[n][Nx_calc-2]));   //since our off diagonal elements are the same we can combine them for calculating X N+1
                      
                      //Correct solution of the reduced system to have the full solution in RHS_reorg
                      for(j=0;j<Ny_calc-1;j++){
                            RHS_reorg[n][i][k][j]+=y_periodic_sol[n][j]*RHS_reorg[n][i][k][Ny_calc-1];
                        } 

                    }
                  
                  }
              
              
              
        }
    
    // !!!!!!!!!!!!!!!Solve third step !!!!!!!!!!!!!!
        //Reorganize so that array is again xyz since reordering on the fly seems to lead to trouble (fix later for performance)
        for(n=0;n<N_comp;n++){
            for(i=0;i<Nx_calc;i++){
                for(j=0;j<Ny_calc;j++){
                    for(k=0;k<Nz_calc;k++){  

                        RHS_1[n][i][j][k]=RHS_reorg[n][i][k][j];

                    }
                }
            }
        }

        
        
        
        //solve last step here like eq.40 
        //RHS_1[n][i][j][k] has w* in it
        
        ldb=Nz_calc-1;
        NRHS=1;
        TRANS='N';
        
        
        for(iterations=0;iterations<Max_iterations;iterations++){
        
            if(iterations!=0) Calc_fct_grid(N_comp,Nx_calc+2,Ny_calc+2,Nz_calc+2,Conc_now,fct_grid,F_param,V_spatial);  //the fct_grid should be advanced with each iteration!
        
            Fill_Jacobi(N_comp,Nx_calc+2,Ny_calc+2,Nz_calc+2,Conc_now,Jacobi,F_param,V_spatial);   //Fill the Jacobi Matrix for the whole domain.
            Create_RHS_w_Fct(N_comp,Nx_calc,Ny_calc,Nz_calc,RHS_1,RHS_reorg,Conc_now,fct_grid,Jacobi,dt); //calculates right hand sides with the function parts like in equation 40. First oiteration can use fct_grid.
            //The RHS to be used is in RHS_reorg, since we have to keep RHS_1=W* for later iterations
        
        
        // Here we build the diagonal matrix for each n,i,j coordinates and solve them on the fly
            for(n=0;n<N_comp;n++){
                  for(i=0;i<Nx_calc;i++){
                      for(j=0;j<Ny_calc;j++){
                          Fill_Diff_Mat_iter(i,j,n,Nz_calc-1,Diff_Mat_iter,Conc_now,Jacobi,r,dt);
                          //since we have to use the solved matrix for calculating the z-periodic aolution and the RHS we use dgttrf and solve the vectors after 
                          dgttrf_(&ldb,Diff_Mat_iter[DIAG_L],Diff_Mat_iter[DIAG],Diff_Mat_iter[DIAG_U],Diff_Mat_iter[DIAG_U2],IPIV[n][ZVAR],&info); 
                          
                          dgttrs_( &TRANS, &ldb, &NRHS, Diff_Mat_iter[DIAG_L] , Diff_Mat_iter[DIAG], Diff_Mat_iter[DIAG_U], Diff_Mat_iter[DIAG_U2], IPIV[n][ZVAR], RHS_reorg[n][i][j], &ldb, &info);
                          
                          //we also have to recalculate the value of z_periodic solution in each step .... do this here
                            //the z periodic vector has to be filled bevor it is solved. (according to the equation for x(2) in the solution algorithm)
                            a=1.0-(1.0/(6.0))+(r[n][ZVAR]);  //standard value without functional dependence
                            b=(1.0/(12.0))-((r[n][ZVAR])/(2.0));
                              
                            for (l=0;l<Nz_calc-1;l++) z_periodic_sol[n][l]=0;  // reinitialize z_per_sol for each coordinate+iteration
                            z_periodic_sol[n][0]=-(b-(dt/24.0)*(Jacobi[i+1][j+1][1][n]));
                            z_periodic_sol[n][Nz_calc-2]=-(b-(dt/24.0)*(Jacobi[i+1][j+1][Nz_calc-1][n]));
                           
                            //solve the filled z-periodic solution
                            dgttrs_( &TRANS, &ldb, &NRHS, Diff_Mat_iter[DIAG_L] , Diff_Mat_iter[DIAG], Diff_Mat_iter[DIAG_U], Diff_Mat_iter[DIAG_U2], IPIV[n][ZVAR], z_periodic_sol[n], &ldb, &info);
                           
                          
                          
                          
                          
                          //calculate value of wraparound cell and save it in RHS_reorg
                            //for this calculation we need the main diagonal and off diagonal elements off the diffusion matrix, here modified by the uptake term -> put this in here stolen from the Diff-Mat Iter part
                            a-=dt*0.5*Jacobi[i+1][j+1][Nz_calc][n];
                            a-=(dt/24.0)*((Jacobi[i+1][j+1][Nz_calc-1][n]-2.0*Jacobi[i+1][j+1][Nz_calc][n]+Jacobi[i+1][j+1][Nz_calc+1][0])-2.0*Jacobi[i+1][j+1][Nz_calc][n]); //uses that jacobi wraps around too so that Jacobi[Nz_calc]==Jacobi[1] 
                            
                            //I think b_N and c_N are the same since they are left and right og a_N and only depend on the jacobi matrix at N  calculating x_N (see webpage with Thomas-algorithm ... there x_N+1 is equivalent to our x_N)
                            b-=(dt/24.0)*(Jacobi[i+1][j+1][Nz_calc][n]);
                            
                            
                            
                            RHS_reorg[n][i][j][Nz_calc-1]= (RHS_reorg[n][i][j][Nz_calc-1]-b*(RHS_reorg[n][i][j][0]+RHS_reorg[n][i][j][Nz_calc-2]))/(a+b*(z_periodic_sol[n][0]+z_periodic_sol[n][Nz_calc-2]));   //since our off diagonal elements are the same we can combine them for calculating X N+1
                      
                          
                                                
                            for(k=0;k<Nz_calc-1;k++){
                                RHS_reorg[n][i][j][k]+=z_periodic_sol[n][k]*RHS_reorg[n][i][j][Nz_calc-1];
                            } 
                          
                          
                          
                          
                          for(k=0;k<Nz_calc;k++) Conc_next[n][i+1][j+1][k+1]=RHS_reorg[n][i][j][k];    //Write results in Conc_next (basically result of this iteration)
                          
                          
                        }
                      
                      }
                      
                      
                    //THE WRAPAROUND PARTS OF CONC HAVE TO BE FILLED TOO, that happens here!
                        //x direction
                        for(j=0;j<Ny_calc;j++){
                            for(k=0;k<Nz_calc;k++){
                                Conc_next[n][0][j+1][k+1]=RHS_reorg[n][Nx_calc-1][j][k]; //Double periodic filling of the conc array 
                                Conc_next[n][Nx_calc+1][j+1][k+1]=RHS_reorg[n][0][j][k];
                                
                                
                            }
                        }
                    
                        //y direction
                        for(i=0;i<Nx_calc;i++){
                            for(k=0;k<Nz_calc;k++){
                                Conc_next[n][i+1][0][k+1]=RHS_reorg[n][i][Ny_calc-1][k]; //Double periodic filling of the conc array 
                                Conc_next[n][i+1][Ny_calc+1][k+1]=RHS_reorg[n][i][0][k];
                                
                                
                            }
                        }
                        //z direction
                          for(i=0;i<Nx_calc;i++){
                            for(j=0;j<Ny_calc;j++){
                                Conc_next[n][i+1][j+1][0]=RHS_reorg[n][i][j][Nz_calc-1]; //Double periodic filling of the conc array 
                                Conc_next[n][i+1][j+1][Nz_calc+1]=RHS_reorg[n][i][j][0];
                                
                                
                            }
                        }
                        
                          //Here we fill the edges of the cube with the face cells from the diagonally opposite side
                          //along x -direction ... these are 4 edges running along the x-axis but without the corners
                          for(i=0;i<Nx_calc;i++){
                            Conc_next[n][i+1][0][0]=RHS_reorg[n][i][Ny_calc-1][Nz_calc-1];
                            Conc_next[n][i+1][Ny_calc+1][0]=RHS_reorg[n][i][0][Nz_calc-1];
                            Conc_next[n][i+1][0][Nz_calc+1]=RHS_reorg[n][i][Ny_calc-1][0];
                            Conc_next[n][i+1][Ny_calc+1][Nz_calc+1]=RHS_reorg[n][i][0][0];
                            
                            
                            }
                            
                          //along y -direction ... these are 4 edges running along the y-axis but without the corners
                          for(i=0;i<Ny_calc;i++){
                            Conc_next[n][0][i+1][0]=RHS_reorg[n][Nx_calc-1][i][Nz_calc-1];
                            Conc_next[n][Nx_calc+1][i+1][0]=RHS_reorg[n][0][i][Nz_calc-1];
                            Conc_next[n][0][i+1][Nz_calc+1]=RHS_reorg[n][Nx_calc-1][i][0];
                            Conc_next[n][Nx_calc+1][i+1][Nz_calc+1]=RHS_reorg[n][0][i][0];
                            
                            
                            }
                        
                          //along z -direction ... these are 4 edges running along the x-axis but without the corners
                          for(i=0;i<Nz_calc;i++){
                            Conc_next[n][0][0][i+1]=RHS_reorg[n][Nx_calc-1][Ny_calc-1][i];
                            Conc_next[n][Nx_calc+1][0][i+1]=RHS_reorg[n][0][Ny_calc-1][i];
                            Conc_next[n][0][Ny_calc+1][i+1]=RHS_reorg[n][Nx_calc-1][0][i];
                            Conc_next[n][Nx_calc+1][Ny_calc+1][i+1]=RHS_reorg[n][0][0][i];
                            
                            
                            }
                            
                         // Now only the 8 corner cells have to be periodized 
                         Conc_next[n][0][0][0]=RHS_reorg[n][Nx_calc-1][Ny_calc-1][Nz_calc-1];
                         
                         Conc_next[n][Nx_calc+1][0][0]=RHS_reorg[n][0][Ny_calc-1][Nz_calc-1];   
                         Conc_next[n][0][Ny_calc+1][0]=RHS_reorg[n][Nx_calc-1][0][Nz_calc-1];
                         Conc_next[n][0][0][Nz_calc+1]=RHS_reorg[n][Nx_calc-1][Ny_calc-1][0];  
                    
                         Conc_next[n][0][Ny_calc+1][Nz_calc+1]=RHS_reorg[n][Nx_calc-1][0][0];   
                         Conc_next[n][Nx_calc+1][0][Nz_calc+1]=RHS_reorg[n][0][Ny_calc-1][0];
                         Conc_next[n][Nx_calc+1][Ny_calc+1][0]=RHS_reorg[n][0][0][Nz_calc-1];
                         
                         Conc_next[n][Nx_calc+1][Ny_calc+1][Nz_calc+1]=RHS_reorg[n][0][0][0];
                    
                      
                  
                  
                  }
              
                  //Write result of iteration in current Concentration matrix (Conc_now) after checking f iteration can be abborted prematurely (the checking part is not yet implemented!)
                  for(n=0;n<N_comp;n++){
                    for(i=0;i<Nx_calc+2;i++){
                        for(j=0;j<Ny_calc+2;j++){                  
                            for(k=0;k<Nz_calc+2;k++){
                                
                                if(Conc_next[n][i][j][k]>Component_max) Conc_now[n][i][j][k]=Component_max;
                                else{
                                    if(Conc_next[n][i][j][k]<0) Conc_now[n][i][j][k]=0;
                                    else Conc_now[n][i][j][k]=Conc_next[n][i][j][k];
                                    }
                                }
                            }
                        }
                    }
        
              
        
        }
    
return 0;    
}

int Copy_DA_to_RK_scratch(double ****A, double ****B,int time_id, int Nx, int Ny, int Nz, int offset) {
    //Copies the DA concentration to the RK matrix that will use it later. time_id is either 0,1,2 for t=n,t=n+0.5 ot t=n+1 respectively. Offset is used in case Conc array is with periodic boundaries
        int i,j,k;

        for(i=0;i<Nx;i++){
            for(j=0;j<Ny;j++){
                for(k=0;k<Nz;k++){
                    B[time_id][i][j][k]=A[0][i+offset][j+offset][k+offset];    //always copy the DA concentration (A[0])
         
         
                }
            }
        }



return 0;
}

int Do_RK4_for_recep_activation(double ****DA_timepoints,double ****Conc_now,double dt,int N_comp_out, int Nx, int Ny, int Nz,struct Fct_param* F_param){
    double k1,k2,k3,k4;
    int no,n,i,j,k,io,jo,ko;
    
    const double k_on[2]={ F_param->k_on_D1, F_param->k_on_D2 };
    const double k_off[2]={ F_param->k_off_D1, F_param->k_off_D2};
    const double C_DR_max[2]= { F_param->C_D1_max, F_param->C_D2_max};
    const double a=1.0/6.0;
    
    // This whole thing would probably benefit from being vectorized... like a lot of the stencil calculations in this code... will this ever happen?... probably not
    for(n=1;n<N_comp_out;n++){
        no=n-1; //for the k_on
        for(i=0;i<Nx;i++){
            io=i+1;
            for(j=0;j<Ny;j++){
                jo=j+1;
                for(k=0;k<Nz;k++){
                    ko=k+1;
                    //This is just a 1 to 1 copy of the RK4 method as seen on e.g. Wikipedia or anywhere else
                    k1=dt*(k_on[no]*DA_timepoints[0][i][j][k]*(C_DR_max[no]-Conc_now[n][io][jo][ko])-k_off[no]*Conc_now[n][io][jo][ko]);
                    k2=dt*(k_on[no]*DA_timepoints[1][i][j][k]*(C_DR_max[no]-(Conc_now[n][io][jo][ko]+k1*0.5))-k_off[no]*(Conc_now[n][io][jo][ko]+k1*0.5));
                    k3=dt*(k_on[no]*DA_timepoints[1][i][j][k]*(C_DR_max[no]-(Conc_now[n][io][jo][ko]+k2*0.5))-k_off[no]*(Conc_now[n][io][jo][ko]+k2*0.5));
                    k4=dt*(k_on[no]*DA_timepoints[2][i][j][k]*(C_DR_max[no]-(Conc_now[n][io][jo][ko]+k3))-k_off[no]*(Conc_now[n][io][jo][ko]+k3));
                    
                    Conc_now[n][io][jo][ko]+=a*(k1+2*k2+2*k3+k4);
                    //if ((i==2) && (j==2) && (k==2)) printf("%d : %.6f %.6f %.6f %.6f %.6f \n",no,k_on[no],k1,DA_timepoints[0][i][j][k],DA_timepoints[1][i][j][k],DA_timepoints[2][i][j][k]);
                    
                    
                }
            }
        }
    }
    
    
    
    
return 0;    
}



int Fill_Diff_Mat_iter(int i,int j,int n,int N,double ** Diff_Mat_iter,double **** Conc_now,double**** Jacobi,double **r,double dt){
    double a,b;
    int k,ix,iy,iz;
    
    
    ix=i+1;
    iy=j+1;
    a=1.0-(1.0/(6.0))+(r[n][ZVAR]);  //standard value without functional dependence
    b=(1.0/(12.0))-((r[n][ZVAR])/(2.0));
    
    //Fill the Matrix with its tridiagonal entries!
    //First fill Main Diagonal
    for(k=0;k<N;k++){
        //i,j,k are for the inside part without the borders , so for taking the correct values in the jacobi+conc array we have to add the +1 again     
        iz=k+1;
        Diff_Mat_iter[DIAG][k]=a-dt*0.5*Jacobi[ix][iy][iz][0];    //this is a with the normal jacobi term I*J*w
        Diff_Mat_iter[DIAG][k]-=(dt/24.0)*((Jacobi[ix][iy][iz-1][0]-2.0*Jacobi[ix][iy][iz][0]+Jacobi[ix][iy][iz+1][0])-2.0*Jacobi[ix][iy][iz][0]);   //Here we add the wijk terms from the dz^2*J*w term we also evaluate this with the product rule
        }
    
    for(k=0;k<N-1;k++){ //Fill Side Diagonals    
        iz=k+1;
        Diff_Mat_iter[DIAG_L][k]=b-(dt/24.0)*(Jacobi[ix][iy][iz][0]);   //The order of Lower and upper might be wrong   ... this might generally be wrong... I don't think this should be symetric like this
        Diff_Mat_iter[DIAG_U][k]=b-(dt/24.0)*(Jacobi[ix][iy][iz][0]);
    }
    
    
    
return 0;    
}





int Fill_Jacobi(int N_comp,int Nx,int Ny,int Nz,double **** Conc_now,double **** Jacobi,struct Fct_param * F_param,double ****V_spatial){
    //This function also depends on the function that is supposed to describe the reaction 
    int i,j,k;
    
    double K_m = (F_param->K_m);
    double k_unspecific= (F_param->k_unspecific);
    double a;
        
        
        
        //Spatially varying MM uptake in V_spatial
        //With linear unspecific Uptake, that is spatially homogenous k_unspecific
        for(i=0;i<Nx;i++){
            for(j=0;j<Ny;j++){
                for(k=0;k<Nz;k++){
                a=V_spatial[0][i][j][k]*K_m;
                Jacobi[i][j][k][0]=a/((K_m+Conc_now[0][i][j][k])*(K_m+Conc_now[0][i][j][k]));   //derivative of MM term
                Jacobi[i][j][k][0]+=k_unspecific;                                               //derivative of unspecific uptake term k*C
                
                
                }
            }
        }
    
    
return 0;    
}

































































































































































































