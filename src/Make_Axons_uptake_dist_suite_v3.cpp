/*
 * 
 * 
Make_Axons_uptake_dist_suite_v3.cpp is axon-generator for diffusion code DOPE-AMINE.
It generates synthethic axons and calculates the length in voxel for a given simulation domain. 
It will also distribute synapses along these axons. 
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */






#include "TreeNode.h"
#include "BinTree.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
#include <vector>
#include <list>
#include <time.h>
#include <stdlib.h>
#include <algorithm>
#include <math.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/point_generators_3.h>
#include <CGAL/algorithm.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/convex_hull_3.h>
#include <CGAL/centroid.h>
#include <CGAL/Cartesian_d.h>
#include <CGAL/MP_Float.h>
#include <CGAL/Approximate_min_ellipsoid_d.h>
#include <CGAL/Approximate_min_ellipsoid_d_traits_3.h>
#include <CGAL/squared_distance_3.h>
#include <CGAL/Segment_3.h>
#include <CGAL/Plane_3.h>
#include <hdf5.h>
//#include <hdf5/serial/hdf5.h>

//./Make_axon_dist.exe 70.0 30.0 17.0 20.0 0.1 17000 100 1.0 0.01 0.25> Synapse_pos.dat //Typical call




using namespace std;











  


        






    
    
    
   

int main(int argc, const char* argv[])
{

long seed;
unsigned int i,j,k,nr_iterations;
int target_nr_branches;
struct  Spatial_Tree_coeff coeff; // Struct that saves parameters for making the 3D_tree
std::vector<Point_3> all_points,outer_points;
 const double eps = 0.001; // approximation ratio is (1+eps)


Polyhedron_3 poly;  //CGAL polyhedron
double cube_start[3],resolution;   //beginning point on lower left corner of the DA_diff calculation cube
double ***cube;     //Will hold the whole cube
unsigned int cube_size,iterations;
int file_nr;
double *Write_buffer;
double p_branch,p_elongate;


//HDF5-stuff
hid_t           file, dataset,dcpl;    /* Handles */
hid_t           dataspace,datatype;
herr_t          status;
char filename[200],DATASETNAME[200],fn_syn_pos[200],tree_3d_pos_switch[200]; //Char arrays in a c++ code... didn't think you would ever see that did you?!
char tree_node_topo_switch[200],prov_seed[200];




//set the parameters for the 3D Tree generator
coeff.branching_angle_mean=atof(argv[1]);  //mean of the branching angle on branch bifurcation
coeff.branching_angle_std=atof(argv[2]);   // sigma of branching angle on branch bifurcation
coeff.kink_angle_mean=atof(argv[3]);   // This if rom the "How straight do axons grow paper, but the range there is 0... 110 so perhaps check that againb and don't allow negative values"
coeff.kink_angle_std=atof(argv[4]);
coeff.L_angle_mean=180.0-(coeff.branching_angle_mean/2.0); //Makes the branching symetric, independent of the branching angle mean
coeff.L_angle_std=atof(argv[5]);    //Width of distribution of the left angle on the branch, higher values here mean more asymetric branches
coeff.p_change_dir=atof(argv[6]);


coeff.dist_synapses=atof(argv[7]);    //average distance of synapses on the axon in mikrometers. Usually chosen to be 7.6 mikrometers according to the 2006 Arbuthnott and Wickens paper?

target_nr_branches=atoi(argv[8]);
cube_size=atoi(argv[9]);    //size of the cube in cells along one axis
resolution=atof(argv[10]);   // resolution of the cube, meaning size of one cell in mikrometers
coeff.Nr_axons=atoi(argv[11]);   // Which nr of axons is generated

coeff.size_exclusion=atof(argv[12]);    //length of the synapse DAT exclusion zone along the axon (on both sites so 0.25-> 0.5 full length)
p_branch=atof(argv[13]);        //branching (Def: 0.0192) and elongation (def: 0.9677) probability... why are they not in the coeff struct... IDK  
p_elongate=atof(argv[14]); 


memset(fn_syn_pos,0,sizeof(fn_syn_pos));    //argument 15 is the filename used for the synapse_pos file
strcpy(fn_syn_pos, argv[15]);
memset(tree_3d_pos_switch,0,sizeof(tree_3d_pos_switch));    //argument 16 is the switch that controls if the positions of all segments are written out
strcpy(tree_3d_pos_switch, argv[16]);

memset(tree_node_topo_switch,0,sizeof(tree_node_topo_switch));    //argument 17 is the switch for tree node topo
strcpy(tree_node_topo_switch, argv[17]);

memset(prov_seed,0,sizeof(prov_seed));    //argument 17 is the switch for tree node topo
strcpy(prov_seed, argv[18]);




nr_iterations=coeff.Nr_axons;

file_nr=0;

//Open files and such:
ofstream f_synapses;
f_synapses.open(fn_syn_pos);    //file to which the synapse position is being written

//sets and prints the seed for reproduceability
if (strcmp(prov_seed, "none")==0) seed=time(0);   //if no seed is provided the seed is used from the time
else seed=atoi(prov_seed);

f_synapses << "Seed used:" << seed << '\n';
srand48(seed);



// Allocate cube    (In this version the cube has the length in cube for one axon)
cube = new double**[cube_size];
for (i = 0; i < cube_size; ++i) {
    cube[i] = new double*[cube_size];

    for (j = 0; j < cube_size; ++j)
      cube[i][j] = new double[cube_size];
}
//Allocate Write_buffer_for Cube HDF5_file
Write_buffer=new double[cube_size*cube_size*cube_size];

// Here we set HDF5 parameters!

hsize_t dims[1] = {cube_size*cube_size*cube_size};
hsize_t chunk[1] = {cube_size*cube_size*cube_size};
sprintf(DATASETNAME, "Length_in_cell");


f_synapses << "dist_synapses Nr_axons: \n";
f_synapses << coeff.dist_synapses << " " << coeff.Nr_axons << "\n";



BinTree Tree(p_branch,p_elongate,target_nr_branches);
Tree.Instrument();

double sum_of_lengths_in_cube;
sum_of_lengths_in_cube=0;

for(iterations=0;iterations<nr_iterations; iterations++){
		Tree.Remake(p_branch,p_elongate,target_nr_branches);
        while(Tree.nr_branch_points<(0.9*target_nr_branches))   //wanna have a tree with at least 0.9*target_nr_branches branch_points
            {
            Tree.Remake(p_branch,p_elongate,target_nr_branches);
            Tree.Instrument();
        
            }


        all_points.clear();
        Tree.Make_3D_tree(&coeff);


        //Instead of Printing the tree we are going to write the coordinates of all points in a points 3 object given by CGAL 

            for (std::vector<TreeNode*>::iterator it = Tree.nodes.begin() ; it != Tree.nodes.end(); ++it)
                {
                    for(i=0;i<(*it)->x_pos.size();i++){
                    
                    all_points.push_back(Point_3((*it)->x_pos[i],(*it)->y_pos[i],(*it)->z_pos[i]));
                    }
                }

            

        //std::cout << "In the begining there are " << all_points.size() << " points" << std::endl;  
        // compute convex hull of non-collinear points
        CGAL::convex_hull_3(all_points.begin(), all_points.end(), poly);




          //// compute approximation:
          Traits traits;
          AME ame(eps, poly.points_begin(), poly.points_end(), traits);

            // output center coordinates:
          /*std::cout << "Cartesian center coordinates: ";
          for (AME::Center_coordinate_iterator c_it = ame.center_cartesian_begin(); c_it != ame.center_cartesian_end(); ++c_it) std::cout << *c_it << ' ';
          std::cout << ".\n";*/
          
          // set the lower left corner of the cube onto the position of the center of the ellipse
          i=0;
          for (AME::Center_coordinate_iterator c_it = ame.center_cartesian_begin(); c_it != ame.center_cartesian_end(); ++c_it){
               
               cube_start[i]=*c_it-0.5*cube_size*resolution;
               i++;
           }
        
        //Zero the array holding the length in cube, this is so that each Axon file only has the length in cube for one axon, the diffusion code will then sum up the length in cube for each Axon
        for(i=0;i<cube_size;i++){
            for(j=0;j<cube_size;j++){
                for(k=0;k<cube_size;k++){  
                    cube[i][j][k]=0;                          
                        
                }
            }
        }
        
        
        
        Tree.Spread_uptake(cube_start,cube,resolution,cube_size); //Writes the length in cube values, later used for the uptake/cell into &cube which is a 3D array that has a cell for each value.   
           
        //This is a stupid hack that keeps the attach synapses loop from going into an endless loop ... however if there are less than 10 nodes inside the cube then maybe this tree shouldn't be used anyway  
        if(Tree.In_cube_nodes.size()>10) { 
            Tree.Attach_synapses(&coeff,cube_start,cube,resolution,cube_size);
          
        sum_of_lengths_in_cube+=Tree.length_in_cube;
        f_synapses << "Itertion: "<< iterations << " length_in_cube: "<< Tree.length_in_cube << " nr_synapses_on_tree: "<< Tree.Nr_synapses_attached << "\n";
        for(i=0; i<Tree.Synapse_x_pos.size();++i) f_synapses << iterations << "  "<< i << "    "<< Tree.Synapse_x_pos[i]-cube_start[XVAR] << "  "<< Tree.Synapse_y_pos[i]-cube_start[YVAR] << "  " << Tree.Synapse_z_pos[i]-cube_start[ZVAR] <<"\n"; 
        
        //The tree position data is written out before we reset the Tree
        if (strcmp(tree_3d_pos_switch, "yes")==0){ //writes the Tree 3D pos out so that the full tree can be reconstructed
            memset(filename,0,sizeof(filename));
            sprintf(filename, "Tree_nr_%.5d.dat", file_nr);
            
            
            Tree.print_3D_pos_py_readable(filename);
        }
        
        //The node topology data is written out before we reset the Tree
        if (strcmp(tree_node_topo_switch, "yes")==0){ //writes the Tree 3D pos out so that the full tree can be reconstructed
            
            Tree.Instrument();  //calculates levels and strahl levels for the full tree
            
            memset(filename,0,sizeof(filename));
            sprintf(filename, "Node_topo_nr_%.5d.dat", file_nr);
            
            
            Tree.print_nodes(filename);
        }
        
        
        
        Tree.Reset_3D();
          
    
        if ( true ){
            //Organize Data in Write buffer for writing it out
            for(i=0;i<cube_size;i++){
                for(j=0;j<cube_size;j++){
                    for(k=0;k<cube_size;k++){  
                        Write_buffer[cube_size*cube_size*i+cube_size*j+k]=cube[i][j][k];                          
                        
                    }
                }
            }
            
           
            memset(filename,0,sizeof(filename));
            sprintf(filename, "Axon_l_in_c_nr_%.5d.h5", file_nr);
            
            
            file = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    
            datatype  = H5Tcopy(H5T_NATIVE_DOUBLE);
            dataspace = H5Screate_simple (1, dims, NULL);
    
            dcpl = H5Pcreate(H5P_DATASET_CREATE);
            status = H5Pset_shuffle (dcpl);
            status = H5Pset_deflate (dcpl, 9);
            status = H5Pset_chunk (dcpl, 1, chunk);
    

            dataset = H5Dcreate (file, DATASETNAME, H5T_NATIVE_DOUBLE, dataspace, H5P_DEFAULT, dcpl,H5P_DEFAULT);
    
    
            status = H5Dwrite (dataset, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT,Write_buffer);
                
            status = H5Tclose(datatype);
            status = H5Pclose(dcpl);
            status = H5Dclose(dataset);
            status = H5Sclose(dataspace);
            status = H5Fclose(file);
            if (status<0) printf("There was a problem closing %s \n",filename);
            //printf("file_nr: %d  Time: %.10f   burst_length %.10f \n",file_nr,t,burst_length);
            
            
            
            
            file_nr++;
        }
        

        
        
    }
    else iterations--;
    
    
    
    
    
}
f_synapses << "The length in cube of all axons is: " << sum_of_lengths_in_cube << "\n";
    
//close the file with the synapse positions
f_synapses.close();

// De-Allocate cube
for (i = 0; i < cube_size; ++i) {
    for (j = 0; j < cube_size; ++j)
      delete [] cube[i][j];

    delete [] cube[i];
  }
delete [] cube;

delete Write_buffer;

}


    



