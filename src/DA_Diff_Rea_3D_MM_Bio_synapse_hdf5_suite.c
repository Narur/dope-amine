/*
 * 
 * 
This file DA_Diff_Rea_3D_MM_Bio_synapse_hdf5_suite.c is the main solver for the diffusion code DOPE-AMINE
* This version of the solver solves the full system as a reaction diffusion system taking into account receptor binding.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */






#include "DA_Diff_macros_structs_shared.h"
#include "DA_Diff_macros_structs_mult_comp.h"
#include "DA_Diff_LAPACK.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <float.h>
#include <hdf5.h>
#include "DA_Diff_IO_utility.h"
#include "DA_Diff_Solver_fct_mult_comp.h"
#include "DA_Diff_Solver_fct_shared.h"
#include "DA_Diff_alloc_shared.h"

const double Avogadro=6.022e23;


int main(int argc, const char* argv[]){
int n,i,j,k,info,l;
int Nx,Ny,Nz,N_comp,Nx_calc,Ny_calc,Nz_calc;
double ****Conc_now, ****Conc_next,****Conc_now_half,****fct_grid_now,****V_spatial, ****Conc_running_mean;  // Will be 4Dimensional arrays that hold the concentrations of the reaction components. Axis are: number of components and x,y,z (2 arrays for n and n+1) 
double phys_domain_size,t;
double **D,**r,**r_half; // 2 dimensional arrays storing the Diffusion constant+modified diffusion parameters for all components and all 3 spatial dimensions
double hx,hy,hz,dt,Tmax;
double **** Diff_Mat,**** Diff_Mat_half; //Holds all the matrices used to solve the Diffusion part of the Problem
int ***IPIV,*** IPIV_half,*IPIV_iter;
double **** RHS_1,****RHS_reorg;
double *** RHS_iter;
double ****Jacobi;
double *Write_buffer;
double temp;
double *ax,*ay,*az,*ayz,*axy,*axz,*axyz;
double *ax_half,*ay_half,*az_half,*ayz_half,*axy_half,*axz_half,*axyz_half;
double *fx,*fy,*fz,*fyz,*fxy,*fxz,*fxyz;
double *fx_half,*fy_half,*fz_half,*fyz_half,*fxy_half,*fxz_half,*fxyz_half;
double * Diff_Mat_iter,*U;
double V_max,K_m,alpha,*D_DA,D_DA_scalar,Quant,k_unspecific;
int ldb,NRHS,write_out_n,mean_write_out_n;

int it_count,file_nr,mean_file_nr,chk_file_nr;
int Max_iterations,Nr_axons;     //Nr_axons is the number of axons in the spiketrain and Synapse pos file, all will be read, but not all will be used!
int ***Axons_synapses; //Holds position of each neuron vor each axon
double *Next_axon_fire;   //Holds the time till the specified axon fires again 
int x_syn,y_syn,z_syn;
double output_t_wait;
double output_dt,output_dt_mean;
int N_spiketrains, Max_nr_spikes,temp_neuron_number,old_neuron_number,count;               //variables for reading and using SNC_spiketrains for the spiking of the axons
int * which_spike,*which_axons;
double temp_spike_time;
double ** spiketrains;
int Nx_reduced,Ny_reduced,Nz_reduced;
int Nx_conc,Ny_conc,Nz_conc;
double **x_periodic_sol, **x_periodic_sol_half,**y_periodic_sol, **y_periodic_sol_half,**z_periodic_sol, **z_periodic_sol_half;
int *nr_synapses;
char Trans;
double rnd_nr,Rel_prob,Curr_Rel_prob;
double syn_x_pos,syn_y_pos,syn_z_pos;
int axon_compare,synapse_compare;
double *Read_buffer, *Accumulate_buffer;
int Nr_active_axons,it_dump;            //Nr_active_axons is the amount of actually used axons
double *length_in_cube;                 //holds length in cube for each axon that is being read
double V_max_correction_in_cell,V_max_corrected;
double Quant_std,This_Quant;
double ISI_to_last_spike;
double k_on_D2,k_on_D1,k_off_D1,k_off_D2,C_D1_max,C_D2_max;
double *init_vals;  //initialisation values for all components
double *Component_max; //The maximum value of each component... if not set it will be set to the maximum possible value for doubles
double sum_length_in_cube;
int sum_nr_synapses;
long seed;
int restart_flag;
double t_start;
double output_dt_chk,T_chunk;
int write_out_chk_n;

hid_t           dcpl;    /* Handles */
hid_t           file_id,dataset_id;
hid_t           dataspace;
hid_t           attr;
herr_t          status;

char filename[200],spike_list_fn[200],synapse_pos_fn[200],Axons_used_fn[200],Axons_used_diff_code_fn[200];
char V_max_flat_switch[200],Spiking_switch[200],str_dump[200],init_val_fn[200], prov_seed[200], fn_xdmf[200];
char res_path[200], subfolder[200];
char temp_datasetname[200];
char ** DATASETNAMES;
FILE* f_spike_list;
FILE* f_synapse_pos, *f_which_axons;





restart_flag=0;
Max_iterations=3;

strcpy(subfolder, "./Resources/"); //The subfolder for all the resource files


/* *****************************************************Allocation of memory and setting  parameters! ************/

/* These parameters control size of simulation cube*/
N_comp=atoi(argv[1]);   // Number of components of the reaction diffusion system
Nx = atoi(argv[2]);     // size of domain in cells
phys_domain_size=atof(argv[3]); //The physical Domain extent in micrometers

/* Control for timestepping of the solver and timestepping for file output*/
dt=atof(argv[4]);   //full timestep for the solver in seconds
Tmax=atof(argv[5]);   //Maximum simulation time
T_chunk=atof(argv[6]);  //length of simulation chunk before code exits... if -1 code will just run from 0.0 to T_max
output_dt=atof(argv[7]);    //timestep for file_output
output_t_wait=atof(argv[8]); //Time in s for whoich no output is generated (usefull if we don't want to write out the "swing in" every run, when 0 everything is written out)
output_dt_mean=atof(argv[9]);
output_dt_chk=atof(argv[10]); //Timestep for checkpoint write out should be long, from these points simulations can be restarted

/* Filenames have to be provided here*/ 
strcpy(spike_list_fn,argv[11]);     //spike_list which axon spikes when
strcpy(synapse_pos_fn,argv[12]);    //where are the synapses on an axon ?
strcpy(Axons_used_fn, argv[13]);       //The file that has a list of all axons that are being used

// Add the subfolder to all fns
memset(res_path,0,sizeof(res_path));
strcpy(res_path, subfolder);
strcat(res_path, Axons_used_fn);
memset(Axons_used_fn,0,sizeof(Axons_used_fn));
strcpy(Axons_used_fn, res_path);  //copy the finished string into the filename used

memset(res_path,0,sizeof(res_path));
strcpy(res_path, subfolder);
strcat(res_path, synapse_pos_fn);
memset(synapse_pos_fn,0,sizeof(synapse_pos_fn));
strcpy(synapse_pos_fn, res_path);  //copy the finished string into the filename used

memset(res_path,0,sizeof(res_path));
strcpy(res_path, subfolder);
strcat(res_path, spike_list_fn);
memset(spike_list_fn,0,sizeof(spike_list_fn));
strcpy(spike_list_fn, res_path);  //copy the finished string into the filename used



/* Reaction diffusion parameters are passed here*/
alpha=atof(argv[14]);   //default 0.2
D_DA_scalar=atof(argv[15]);   //default 320.0 ,modified diffusion coefficient in micrometers^2/s is equivalent to D=7.6e-6 cm^2/s with tortuosity of 1.54 
k_on_D1=atof(argv[16]);  //in 1/micro-mole*s
k_on_D2=atof(argv[17]);  //in 1/micro-mole*s
k_off_D1=atof(argv[18]);
k_off_D2=atof(argv[19]);
C_D1_max=atof(argv[20]); //in micro-mole  
C_D2_max=atof(argv[21]); //in micro mole




Quant=atof(argv[22]);
Quant_std=atof(argv[23]);    //std is 0.1 of the value

Rel_prob=atof(argv[24]);
V_max=atof(argv[25]);   //Should be negative, already scaled by alpha
K_m=atof(argv[26]);
k_unspecific=atof(argv[27]);    //shoule be negative

V_max_correction_in_cell=atof(argv[28]);    //The average length of axon per cell, to be used to set so that V_max/cell is roughly the correct value ..! -> this should actually be the average length in cell for all realizations (so some realizations will have different V_max... basically that calculates V_max per micrometer in cell for the average length in cell so that V_max is correct in the end)
V_max_corrected=V_max/V_max_correction_in_cell;
strcpy(V_max_flat_switch,argv[29]);    //Holds the switch if V_max is flat, read from the appropriate Axon_files, or is just read from one file 
strcpy(Spiking_switch,argv[30]); //Holds the switch if spiking is none, PPF or PPD


memset(prov_seed,0,sizeof(prov_seed));   
strcpy(prov_seed, argv[31]);


memset(init_val_fn,0,sizeof(init_val_fn));  //set init_val_fn to empty ... this will be used to see if the initialisation should be read from a file 
init_vals=(double*) calloc(N_comp, sizeof(double)); 
if ( sscanf( argv[32], "%lf", &init_vals[0] ) == 1 ){   //check if the first entered initialisation value is a float... if yes we assume a flat initialization to this value, if no the parameter should be the filename from which to read the initialisation values for the domain
    for(i=1;i<N_comp;i++){  //If float has been entered as first value read the rest of the floats
        init_vals[i]=atof(argv[32+i]);
    }
}

else if (strcmp(argv[32], "restart")==0){
        //restart from the checkpoint file given 
        restart_flag=1;
        memset(init_val_fn,0,sizeof(init_val_fn));
        strcpy(init_val_fn, argv[33]);
}


else{
    
    //make filename with subfolder
    memset(init_val_fn,0,sizeof(init_val_fn));
    strcpy(init_val_fn, argv[32]); //if the first init_val is not a float/double it should be a filename and then this filename will be used to read the init_vals from 
    memset(res_path,0,sizeof(res_path));
    strcpy(res_path, subfolder);
    strcat(res_path, init_val_fn);
    memset(init_val_fn,0,sizeof(init_val_fn));
    strcpy(init_val_fn, res_path);  //copy the finished string into the filename used
    
}


//initialize RNG
//sets and prints the seed for reproduceability
if (strcmp(prov_seed, "none")==0) seed=time(0);   //if no seed is provided the seed is used from the time
else seed=atoi(prov_seed);
srand48(seed);






write_out_n=(int) (output_dt/dt);
mean_write_out_n=(int) (output_dt_mean/dt);
write_out_chk_n=(int) (output_dt_chk/dt);
printf("Parameters used are: N_comp=%d, NX= %d, domain_size=%.2f \n", N_comp, Nx, phys_domain_size);
printf("Timestepping: dt= %.6f, Tmax=%.6f, T_chunk=%.6f, output_dt=%.6f, output_dt_mean=%.6f,output_dt_chk=%.6f ,output_t_wait=%.6f \n  ",dt, Tmax, T_chunk, output_dt, output_dt_mean, output_dt_chk, output_t_wait);
printf("Files: Spike_list=%s, synapse_pos=%s, Axons_used=%s \n  ",spike_list_fn, synapse_pos_fn, Axons_used_fn);
printf("Reaction-diffusion parameters: alpha=%.6f, D_DA_scalar=%.6f, k_on_D1=%.6f, k_on_D2=%.6f, k_off_D1=%.6f, k_off_D2=%.6f, C_D1_max=%.6f, C_D2_max=%.6f \n",alpha, D_DA_scalar, k_on_D1, k_on_D2, k_off_D1, k_off_D2, C_D1_max, C_D2_max);
printf("Spiking parameters: Quant=%.6f, Quant_Std=%.6f, Rel_prob=%.6f \n", Quant, Quant_std, Rel_prob);
printf("Uptake Parameters: V_max=%.6f, K_m=%.6f, k_unspecific=%.6f, V_max_correction_in_cell=%.6f, V_max_corrected=%.6f \n", V_max, K_m, k_unspecific, V_max_correction_in_cell, V_max_corrected);
printf("Switches: Uptake_switch=%s, Spiking_switch=%s, prov_seed=%s \n", V_max_flat_switch, Spiking_switch, prov_seed);
if ( sscanf( argv[32], "%lf", &init_vals[0] ) == 1 ) printf("Init: Init_val_DA=%.6f, Init_val_D1=%.6f, Init_val_D2=%.6f \n",init_vals[0], init_vals[1], init_vals[2]);
else printf("Init: init_val_fn=%s \n", init_val_fn);

//set Diffusion coefficients to 0 except for the Dopamine part
D_DA=(double*) calloc(N_comp, sizeof(double));  
D_DA[0]=D_DA_scalar;    
for(i=1;i<N_comp;i++) D_DA[i]=0;

Component_max=(double*) calloc(N_comp, sizeof(double));  
Component_max[0]=DBL_MAX;        //max DA... no max    
Component_max[1]=C_D1_max;      //max C_D1 ... maximum is number of receptors 
Component_max[2]=C_D2_max;      //max C_D2     



Ny=Nz=Nx;               //for now the domain has the same extent in all 3 spatial directions 
hx=phys_domain_size/Nx; //hx, hy,hz are the stepsizes for the spatial step
hy=hz=hx;

//NX calc for matrices etc are smaller because of the boundary conditions!
Nx_calc=Nx; //For periodic boundaries want to calculate full size
Ny_calc=Ny;
Nz_calc=Nz;

Nx_reduced=Nx_calc-1; //the reduced version is for the smaller system that needs to be solved in the periodic case
Ny_reduced=Ny_calc-1; 
Nz_reduced=Nz_calc-1;

Nx_conc=Nx+2;   // with wraparound cells
Ny_conc=Ny+2;
Nz_conc=Nz+2;



//First we read the number of and which Axons are active Axons from a file:
f_which_axons=fopen(Axons_used_fn, "r");
fscanf(f_which_axons,"%d \n",&Nr_active_axons); //after this Nr_active_axons is the number of axons actually used

which_axons=(int*) calloc(Nr_active_axons,sizeof(int));    //This array holds the axon numbers of the axons that are being used
for (i=0;i<Nr_active_axons;i++) fscanf(f_which_axons,"%d",&which_axons[i]); //which_axons is now an array with all the axon_ids that are being used in the simulation
fclose(f_which_axons);

printf("Nr of active Axons: %d, write out every: %d steps \n",Nr_active_axons,write_out_n);
fflush(stdout);








//Here we calculate axon and synapse number while opening the file that has the synapse locations in it
f_synapse_pos=fopen(synapse_pos_fn, "r");
fscanf(f_synapse_pos, "%*[^\n]\n");   //skips the seed line in the f_synapse file
fscanf(f_synapse_pos, "%*[^\n]\n");   //skips a description
double dummy_temp;
fscanf(f_synapse_pos,"%lf %d",&dummy_temp, &Nr_axons);


//Allocate memory for the Axon and synapse parts
nr_synapses=(int *) calloc(Nr_axons,sizeof(int));   //callof with Nr_axons,the number of axons in the file, so that the whole file is being read
length_in_cube=(double *) calloc(Nr_axons,sizeof(double));   //callof with Nr_axons,the number of axons in the file, so that the whole file is being read

//allocates memory for synapse position and also reads the synapse position (in the next loop)
Axons_synapses=(int***) calloc(3,sizeof(int**));    //the 3 spatial coordinates
for (i=0;i<3;i++){
    Axons_synapses[i]=(int**) calloc(Nr_axons,sizeof(int*));
}
    
    
for (j=0;j<Nr_axons;j++){
    fscanf(f_synapse_pos,"%s %d %s %lf %s %d \n",str_dump,&it_dump,str_dump,&length_in_cube[j],str_dump,&nr_synapses[j]);
    //printf("It: %d length %g	synapses %d \n",it_dump,length_in_cube,nr_synapses[j]);
    Axons_synapses[XVAR][j]=(int*) calloc(nr_synapses[j],sizeof(int));  //coordinates are int because they are coordinates in the calculation cube 
    Axons_synapses[YVAR][j]=(int*) calloc(nr_synapses[j],sizeof(int));
    Axons_synapses[ZVAR][j]=(int*) calloc(nr_synapses[j],sizeof(int));
		
        for(k=0;k<nr_synapses[j];k++){
            fscanf(f_synapse_pos,"%d %d %lf %lf %lf",&synapse_compare,&axon_compare,&syn_x_pos,&syn_y_pos,&syn_z_pos);
				
            Axons_synapses[XVAR][j][k]= floor(syn_x_pos/hx)<Nx_reduced ? floor(syn_x_pos/hx) : Nx_reduced;    //drops the synapse positions in the synapse pos file to coordinates in the cube 
            Axons_synapses[YVAR][j][k]= floor(syn_y_pos/hy)<Ny_reduced ? floor(syn_y_pos/hy) : Ny_reduced;
            Axons_synapses[ZVAR][j][k]= floor(syn_z_pos/hz)<Nz_reduced ? floor(syn_z_pos/hz) : Nz_reduced;
				
            //printf("%d %d %g %g %g \n",synapse_compare,axon_compare,syn_x_pos,syn_y_pos,syn_z_pos);
		
		
        }
    }

//sum length and nr_synapses for each ACTIVE axon
sum_nr_synapses=0;
sum_length_in_cube=0;

for (j=0;j<Nr_active_axons;j++){
    sum_nr_synapses+=nr_synapses[which_axons[j]];
    sum_length_in_cube+=length_in_cube[which_axons[j]];
    
}        
    
    

    
printf("Total length of axons in cube: %g	Total number of attached synapses: %d on %d Axons \n", sum_length_in_cube,sum_nr_synapses,Nr_active_axons);
fclose(f_synapse_pos);


//Read spiketimes data from  the Spiketimes file (... why is this code such a mess )
f_spike_list=fopen(spike_list_fn, "r");
fscanf(f_spike_list, "%*[^\n]\n");   //This block skips all the comments and info lines in the Spiketrain file
fscanf(f_spike_list, "%*[^\n]\n"); 
fscanf(f_spike_list, "%*[^\n]\n");  
fscanf(f_spike_list, "%*[^\n]\n");  
fscanf(f_spike_list, "%*[^\n]\n");  
fscanf(f_spike_list, "%*[^\n]\n");   
fscanf(f_spike_list, "%*[^\n]\n");   
fscanf(f_spike_list, "%*[^\n]\n");   
fscanf(f_spike_list, "%*[^\n]\n");   

fscanf(f_spike_list,"%d %d",&N_spiketrains,&Max_nr_spikes); 
if (N_spiketrains!=Nr_axons){   //If the Nr_spiketrains is not Nr_axons there might be unforeseen problems so we will write some kind of abort message and exit here!
    printf("The Nr_axons in the synape position file is not the same as the Number of spiketrains in the Spiketrain file, this will lead to unforeseen consequences... exiting! (That should not be if you used the Setup_script... if not look into the documentation for the synapse pos and spiketrain files... which hopefully exists...) \n");
    exit(8);
    }


spiketrains=(double**) calloc(N_spiketrains, sizeof(double*));     //mallocs the that holds the spiketrains we read from the data file, with order spiketrains[which spiketrain][ spiketimes] 

if (Max_nr_spikes>0){
    for(i=0;i<N_spiketrains;i++){
        spiketrains[i]=(double*) calloc(Max_nr_spikes,sizeof(double));
        spiketrains[i][0]=1e80; //in case the spiketrain has no spikess it will not fire because the first spiketime is very far in the future... that needs to be done to keep random firing from happening if a spiketrain is empty
        }
}
else{   //If there are no spikes, calloc a size 1 array for each spiketrain, so that we can initialize the first value in the spiketrain to a high number  
    printf("It seems like the spiketrains you provided have no spikes... thats not a problem... just think I let you know in case that wasn't your plan! \n");
    for(i=0;i<N_spiketrains;i++){
        spiketrains[i]=(double*) calloc(1,sizeof(double));
        spiketrains[i][0]=1e80;
    }
}



which_spike=(int*) calloc(N_spiketrains, sizeof(int)); //for the injection... remembers which spike for which axon should be next
old_neuron_number=-1;
count=0;
while (fscanf(f_spike_list, "   %d  %lf   ", &temp_neuron_number,&temp_spike_time) != EOF) {
    //fscanf(f_spike_list,"%d    %lf", );
    if(temp_neuron_number!=old_neuron_number) count=0;
    spiketrains[temp_neuron_number][count]=temp_spike_time/1000.0;  //turn spiketimes in ms to spiketimes in s
    //printf("%d  %d  %d\n", temp_neuron_number,old_neuron_number,count);
    old_neuron_number=temp_neuron_number;
    count++;
}    
    
    
fclose(f_spike_list);

//Here we initialize the next axon fire times for the active axons (the axons that actually fire)
Next_axon_fire=(double*) calloc(Nr_active_axons,sizeof(double));
for(i=0;i<Nr_active_axons;i++){
    temp_neuron_number=which_axons[i];
    Next_axon_fire[i]=spiketrains[temp_neuron_number][0];    //Reads axon firing times from the saved firing times... only using the active axons IDs  
}





// Here we set HDF5 parameters!
hsize_t dims[1] = {Nx*Ny*Nz};
DATASETNAMES=(char**) calloc(N_comp, sizeof(char*));     //Malloc the DATAsetnames char arrays for each component that needs to be written in the HDF5 file
for(i=0;i<N_comp;i++){
    DATASETNAMES[i]=(char*) calloc(200,sizeof(char));
    }

sprintf(DATASETNAMES[0], "DA");                 //Write the DATAsetnames for the hdf5 file, probably that part should be a part of the ini file that sets up a simulation.. or not for now its just DA!
if (N_comp>1) sprintf(DATASETNAMES[1], "D1");
if (N_comp>2) sprintf(DATASETNAMES[2], "D2");


D=allocate_D(N_comp);
r=allocate_D(N_comp);
Jacobi=allocate_Jacobi(N_comp,Nx_conc,Ny_conc,Nz_conc);
r_half=allocate_D(N_comp);
Conc_now=allocate_Conc(N_comp,Nx_conc,Ny_conc,Nz_conc);
fct_grid_now=allocate_Conc(N_comp,Nx_conc,Ny_conc,Nz_conc);
V_spatial=allocate_Conc(N_comp,Nx_conc,Ny_conc,Nz_conc);    //Spatially inhomogenous Uptake constant for MM uptake
Conc_now_half=allocate_Conc(N_comp,Nx_conc,Ny_conc,Nz_conc);
Conc_running_mean=allocate_Conc(N_comp,Nx_conc,Ny_conc,Nz_conc); //For storing the running average that will be occasionally be written out
Conc_next=allocate_Conc(N_comp,Nx_conc,Ny_conc,Nz_conc);
Diff_Mat=allocate_Diff_Mat(N_comp,Nx_reduced,Ny_reduced,Nz_reduced);    //Diff Matrices can be smaller since the full system is solved later
Diff_Mat_half=allocate_Diff_Mat(N_comp,Nx_reduced,Ny_reduced,Nz_reduced);

IPIV=allocate_IPIV(N_comp,Nx_calc,Ny_calc,Nz_calc); //Array with pivoting elements coming out of the Lapack routine dgttrf
IPIV_half=allocate_IPIV(N_comp,Nx_calc,Ny_calc,Nz_calc);
IPIV_iter=(int*) calloc(N_comp*Nz_calc,sizeof(int));

RHS_1=allocate_Conc(N_comp,Nz_calc,Nx_calc,Ny_calc);
RHS_reorg=allocate_Conc(N_comp,Nz_calc,Nx_calc,Ny_calc);  //reorganized RHS array so that it can be passed to the Lapack routines
RHS_iter=allocate_RHS_iter(Nx_calc,Ny_calc,Nz_calc*N_comp);





     
Write_buffer=(double*) calloc(Nx*Ny*Nz,sizeof(double));
Read_buffer=(double*) calloc(Nx*Ny*Nz,sizeof(double));


Diff_Mat_iter=(double*) calloc(((3*((2*N_comp)-1)+1)*Nz*N_comp), sizeof(double));     //Diff Mat iter is now a 1D array that uses the Band storage system off LAPACK size is 2*KL+Ku+1, since our matrix is square we have KL==KU so it's 3*KL in the allocation
U=(double*) calloc((N_comp*N_comp*Nz), sizeof(double));                               //The array that will hold the block vector U for the Sherman Morrison woodbury formula


//allocate the (x,y,z)_periodic solution vectors . 
    x_periodic_sol=(double**) calloc(N_comp,sizeof(double*));
    y_periodic_sol=(double**) calloc(N_comp,sizeof(double*));
    z_periodic_sol=(double**) calloc(N_comp,sizeof(double*));
    for (i=0;i<N_comp;i++){
        x_periodic_sol[i]=(double*) calloc(Nx_reduced,sizeof(double));
        y_periodic_sol[i]=(double*) calloc(Ny_reduced,sizeof(double));
        z_periodic_sol[i]=(double*) calloc(Nz_reduced,sizeof(double));
        } 
        
    x_periodic_sol_half=(double**) calloc(N_comp,sizeof(double*));
    y_periodic_sol_half=(double**) calloc(N_comp,sizeof(double*));
    z_periodic_sol_half=(double**) calloc(N_comp,sizeof(double*));
    for (i=0;i<N_comp;i++){
        x_periodic_sol_half[i]=(double*) calloc(Nx_reduced,sizeof(double));
        y_periodic_sol_half[i]=(double*) calloc(Ny_reduced,sizeof(double));
        z_periodic_sol_half[i]=(double*) calloc(Nz_reduced,sizeof(double));

        } 


    

// !!! all these allocations should be prettified when the code is done ... but they haven't been LIES ALL LIES!
ax=(double*) calloc(N_comp,sizeof(double)); //Meh this allocation can probably be done better too! do this when accelerating the code ... HAHAHAHA
ay=(double*) calloc(N_comp,sizeof(double));
az=(double*) calloc(N_comp,sizeof(double));
ayz=(double*) calloc(N_comp,sizeof(double));
axy=(double*) calloc(N_comp,sizeof(double));
axz=(double*) calloc(N_comp,sizeof(double));
axyz=(double*) calloc(N_comp,sizeof(double));

ax_half=(double*) calloc(N_comp,sizeof(double));    //Meh this allocation can probably be done better too! do this when accelerating the code
ay_half=(double*) calloc(N_comp,sizeof(double));
az_half=(double*) calloc(N_comp,sizeof(double));
ayz_half=(double*) calloc(N_comp,sizeof(double));
axy_half=(double*) calloc(N_comp,sizeof(double));
axz_half=(double*) calloc(N_comp,sizeof(double));
axyz_half=(double*) calloc(N_comp,sizeof(double));

fx=(double*) calloc(N_comp,sizeof(double)); //Meh this allocation can probably be done better too! do this when accelerating the code
fy=(double*) calloc(N_comp,sizeof(double));
fz=(double*) calloc(N_comp,sizeof(double));
fyz=(double*) calloc(N_comp,sizeof(double));
fxy=(double*) calloc(N_comp,sizeof(double));
fxz=(double*) calloc(N_comp,sizeof(double));
fxyz=(double*) calloc(N_comp,sizeof(double));

fx_half=(double*) calloc(N_comp,sizeof(double)); //Meh this allocation can probably be done better too! do this when accelerating the code
fy_half=(double*) calloc(N_comp,sizeof(double));
fz_half=(double*) calloc(N_comp,sizeof(double));
fyz_half=(double*) calloc(N_comp,sizeof(double));
fxy_half=(double*) calloc(N_comp,sizeof(double));
fxz_half=(double*) calloc(N_comp,sizeof(double));
fxyz_half=(double*) calloc(N_comp,sizeof(double));

/* *****************************************************Allocation of memory and setting  parameters! DONE ************/



/* ********************************************************* SETUP THE SOLVER SYSTEM *************************************************** */ 
for(n=0;n<N_comp;n++){  //set Diffusion coefficient for all components and all directions
    D[n][XVAR]=D_DA[n]; 
    D[n][YVAR]=D_DA[n];
    D[n][ZVAR]=D_DA[n];
    r[n][XVAR]=(D[n][XVAR]*dt)/(hx*hx);
    r[n][YVAR]=(D[n][YVAR]*dt)/(hy*hy);
    r[n][ZVAR]=(D[n][ZVAR]*dt)/(hz*hz);
    
    r_half[n][XVAR]=(D[n][XVAR]*dt*0.5)/(hx*hx);    //r_half is for half the timestep, this will be used for the Richardson extrapolation later (see eq.41)
    r_half[n][YVAR]=(D[n][YVAR]*dt*0.5)/(hy*hy);
    r_half[n][ZVAR]=(D[n][ZVAR]*dt*0.5)/(hz*hz);
    

    }

//calculate the diffusion prefactors for the stencils, this is done for delta_t and 0.5 delta_t (half) so that we can create a system with halp the timestep too for the Richardson extrapolation
for(n=0;n<N_comp;n++){
        az[n]=(1.0/12.0+(r[n][ZVAR]/2.0));
        ay[n]=(1.0/12.0+(r[n][YVAR]/2.0));
        ax[n]=(1.0/12.0+(r[n][XVAR]/2.0));
        ayz[n]=((r[n][YVAR]*r[n][ZVAR])/4.0+r[n][YVAR]/24.0+r[n][ZVAR]/24.0+1.0/144.0);
        axy[n]=((r[n][XVAR]*r[n][YVAR])/4.0+r[n][XVAR]/24.0+r[n][YVAR]/24.0+1.0/144.0);
        axz[n]=((r[n][XVAR]*r[n][ZVAR])/4.0+r[n][XVAR]/24.0+r[n][ZVAR]/24.0+1.0/144.0);
        axyz[n]=(((r[n][XVAR]*r[n][YVAR]*r[n][ZVAR])/8.0)+((r[n][XVAR]*r[n][YVAR])/48.0)+((r[n][XVAR]*r[n][ZVAR])/48.0)+((r[n][YVAR]*r[n][ZVAR])/48.0)+(r[n][YVAR]/288.0)+(r[n][XVAR]/288.0)+(r[n][ZVAR]/288.0)+1.0/1728.0);
    
        fz[n]=(1.0/12.0);
        fy[n]=(1.0/12.0+(r[n][YVAR]/2.0));
        fx[n]=(1.0/12.0+(r[n][XVAR]/2.0));
        fyz[n]=((r[n][YVAR]/24.0+1.0/144.0));
        fxy[n]=((r[n][XVAR]*r[n][YVAR])/4.0+r[n][XVAR]/24.0+r[n][YVAR]/24.0+1.0/144.0);
        fxz[n]=((r[n][XVAR]/24.0+1.0/144.0));
        fxyz[n]=(((r[n][XVAR]*r[n][YVAR])/48.0)+(r[n][YVAR]/288.0)+(r[n][XVAR]/288.0)+1.0/1728.0);
    
    
    
        //printf("a factors: %.8f   %.8f   %.8f   %.8f   %.8f %.8f %.8f \n",az[n],ay[n],ax[n],ayz[n],axy[n],axz[n],axyz[n]);
    }

struct Diff_coeff Dcoeff_full = { ax,ay,az,axy,ayz,axz,axyz}; 
struct Diff_coeff f_coeff_full = { fx,fy,fz,fxy,fyz,fxz,fxyz};   //Calculate the coefficients for the function d's in equation 38

//here r_half is used for filling the coefficients
for(n=0;n<N_comp;n++){
        az_half[n]=(1.0/12.0+(r_half[n][ZVAR]/2.0));
        ay_half[n]=(1.0/12.0+(r_half[n][YVAR]/2.0));
        ax_half[n]=(1.0/12.0+(r_half[n][XVAR]/2.0));
        ayz_half[n]=((r_half[n][YVAR]*r_half[n][ZVAR])/4.0+r_half[n][YVAR]/24.0+r_half[n][ZVAR]/24.0+1.0/144.0);
        axy_half[n]=((r_half[n][XVAR]*r_half[n][YVAR])/4.0+r_half[n][XVAR]/24.0+r_half[n][YVAR]/24.0+1.0/144.0);
        axz_half[n]=((r_half[n][XVAR]*r_half[n][ZVAR])/4.0+r_half[n][XVAR]/24.0+r_half[n][ZVAR]/24.0+1.0/144.0);
        axyz_half[n]=(((r_half[n][XVAR]*r_half[n][YVAR]*r_half[n][ZVAR])/8.0)+((r_half[n][XVAR]*r_half[n][YVAR])/48.0)+((r_half[n][XVAR]*r_half[n][ZVAR])/48.0)+((r_half[n][YVAR]*r_half[n][ZVAR])/48.0)+(r_half[n][YVAR]/288.0)+(r_half[n][XVAR]/288.0)+(r_half[n][ZVAR]/288.0)+1.0/1728.0);
    
        fz_half[n]=(1.0/12.0);
        fy_half[n]=(1.0/12.0+(r_half[n][YVAR]/2.0));
        fx_half[n]=(1.0/12.0+(r_half[n][XVAR]/2.0));
        fyz_half[n]=((r_half[n][YVAR]/24.0+1.0/144.0));
        fxy_half[n]=((r_half[n][XVAR]*r_half[n][YVAR])/4.0+r_half[n][XVAR]/24.0+r_half[n][YVAR]/24.0+1.0/144.0);
        fxz_half[n]=((r_half[n][XVAR]/24.0+1.0/144.0));
        fxyz_half[n]=(((r_half[n][XVAR]*r_half[n][YVAR])/48.0)+(r_half[n][YVAR]/288.0)+(r_half[n][XVAR]/288.0)+1.0/1728.0);
    
    
    
        //printf("a factors: %.8f   %.8f   %.8f   %.8f   %.8f %.8f %.8f \n",az[n],ay[n],ax[n],ayz[n],axy[n],axz[n],axyz[n]);
    }

struct Diff_coeff Dcoeff_half = { ax_half,ay_half,az_half,axy_half,ayz_half,axz_half,axyz_half};  
struct Diff_coeff f_coeff_half = { fx_half,fy_half,fz_half,fxy_half,fyz_half,fxz_half,fxyz_half}; 




// Here the Diffusion matrices for the algorithm are filled (again full timestep and half timestep!)
for(n=0;n<N_comp;n++){
    Fill_Diff_Mat(Diff_Mat[n][XVAR],Nx_reduced,r[n][XVAR],hx);  //Only need reduced system to be solved for the periodic case... don't explicitly need full Diff_mat
    Fill_Diff_Mat(Diff_Mat[n][YVAR],Ny_reduced,r[n][YVAR],hy);
    Fill_Diff_Mat(Diff_Mat[n][ZVAR],Nz_reduced,r[n][ZVAR],hz);
    
    Fill_Diff_Mat(Diff_Mat_half[n][XVAR],Nx_reduced,r_half[n][XVAR],hx);
    Fill_Diff_Mat(Diff_Mat_half[n][YVAR],Ny_reduced,r_half[n][YVAR],hy);
    Fill_Diff_Mat(Diff_Mat_half[n][ZVAR],Nz_reduced,r_half[n][ZVAR],hz);
    
    }


//Here the matrices that don't change are solved by the use of the LAPACK routine dgttrf ... this is done after the second filling since we have to keep these!

for(n=0;n<N_comp;n++){
dgttrf_(&Nx_reduced,Diff_Mat[n][XVAR][DIAG_L],Diff_Mat[n][XVAR][DIAG],Diff_Mat[n][XVAR][DIAG_U],Diff_Mat[n][XVAR][DIAG_U2],IPIV[n][XVAR],&info);  //Here the Matrix solution is also just done for the reduced system
dgttrf_(&Ny_reduced,Diff_Mat[n][YVAR][DIAG_L],Diff_Mat[n][YVAR][DIAG],Diff_Mat[n][YVAR][DIAG_U],Diff_Mat[n][YVAR][DIAG_U2],IPIV[n][YVAR],&info);  
dgttrf_(&Nz_reduced,Diff_Mat[n][ZVAR][DIAG_L],Diff_Mat[n][ZVAR][DIAG],Diff_Mat[n][ZVAR][DIAG_U],Diff_Mat[n][ZVAR][DIAG_U2],IPIV[n][ZVAR],&info);  

dgttrf_(&Nx_reduced,Diff_Mat_half[n][XVAR][DIAG_L],Diff_Mat_half[n][XVAR][DIAG],Diff_Mat_half[n][XVAR][DIAG_U],Diff_Mat_half[n][XVAR][DIAG_U2],IPIV_half[n][XVAR],&info);  
dgttrf_(&Ny_reduced,Diff_Mat_half[n][YVAR][DIAG_L],Diff_Mat_half[n][YVAR][DIAG],Diff_Mat_half[n][YVAR][DIAG_U],Diff_Mat_half[n][YVAR][DIAG_U2],IPIV_half[n][YVAR],&info);  
dgttrf_(&Nz_reduced,Diff_Mat_half[n][ZVAR][DIAG_L],Diff_Mat_half[n][ZVAR][DIAG],Diff_Mat_half[n][ZVAR][DIAG_U],Diff_Mat_half[n][ZVAR][DIAG_U2],IPIV_half[n][ZVAR],&info);  
}


  


//This thing should also be done in a function... to make the code prettier
//Solve the periodic solution vectors with the correct matrix ... so that the solution to the partial problem A*x2=[-b1...cn], x2 can be passed to advance t (only depends on the matrix so that has to be solved only once)
for (i=0;i<N_comp;i++){ //when the periodic solution vector is filled we put the same value in first and last element since offdiagonal elements are the same in our case ... since Diffusion doesn't care if the movement is forward or backward in x direction
    x_periodic_sol[i][0]=-((1.0/(12.0))-((r[i][XVAR])/(2.0)));
    x_periodic_sol[i][Nx_reduced-1]=-((1.0/(12.0))-((r[i][XVAR])/(2.0)));
    x_periodic_sol_half[i][0]=-((1.0/(12.0))-((r_half[i][XVAR])/(2.0)));
    x_periodic_sol_half[i][Nx_reduced-1]=-((1.0/(12.0))-((r_half[i][XVAR])/(2.0)));
    
    y_periodic_sol[i][0]=-((1.0/(12.0))-((r[i][YVAR])/(2.0)));
    y_periodic_sol[i][Ny_reduced-1]=-((1.0/(12.0))-((r[i][YVAR])/(2.0)));
    y_periodic_sol_half[i][0]=-((1.0/(12.0))-((r_half[i][YVAR])/(2.0)));
    y_periodic_sol_half[i][Ny_reduced-1]=-((1.0/(12.0))-((r_half[i][YVAR])/(2.0)));
    
    z_periodic_sol[i][0]=-((1.0/(12.0))-((r[i][ZVAR])/(2.0)));
    z_periodic_sol[i][Nz_reduced-1]=-((1.0/(12.0))-((r[i][ZVAR])/(2.0)));
    z_periodic_sol_half[i][0]=-((1.0/(12.0))-((r_half[i][ZVAR])/(2.0)));
    z_periodic_sol_half[i][Nz_reduced-1]=-((1.0/(12.0))-((r_half[i][ZVAR])/(2.0)));
        
    }
    
//This solves the problem *x2=[-b1...cn] 
Trans='N';
ldb=Nx_reduced; //this assumes cubic domain, if this is not the case the symetry here has to be broken
NRHS=1;
 for (i=0;i<N_comp;i++){
    
    dgttrs_(&Trans,&Nx_reduced,&NRHS,Diff_Mat[i][XVAR][DIAG_L],Diff_Mat[i][XVAR][DIAG],Diff_Mat[i][XVAR][DIAG_U],Diff_Mat[i][XVAR][DIAG_U2],IPIV[i][XVAR],x_periodic_sol[i],&ldb,&info);
    dgttrs_(&Trans,&Nx_reduced,&NRHS,Diff_Mat_half[i][XVAR][DIAG_L],Diff_Mat_half[i][XVAR][DIAG],Diff_Mat_half[i][XVAR][DIAG_U],Diff_Mat_half[i][XVAR][DIAG_U2],IPIV_half[i][XVAR],x_periodic_sol_half[i],&ldb,&info);
    
    dgttrs_(&Trans,&Ny_reduced,&NRHS,Diff_Mat[i][YVAR][DIAG_L],Diff_Mat[i][YVAR][DIAG],Diff_Mat[i][YVAR][DIAG_U],Diff_Mat[i][YVAR][DIAG_U2],IPIV[i][YVAR],y_periodic_sol[i],&ldb,&info);
    dgttrs_(&Trans,&Ny_reduced,&NRHS,Diff_Mat_half[i][YVAR][DIAG_L],Diff_Mat_half[i][YVAR][DIAG],Diff_Mat_half[i][YVAR][DIAG_U],Diff_Mat_half[i][YVAR][DIAG_U2],IPIV_half[i][YVAR],y_periodic_sol_half[i],&ldb,&info);
    
    dgttrs_(&Trans,&Nz_reduced,&NRHS,Diff_Mat[i][ZVAR][DIAG_L],Diff_Mat[i][ZVAR][DIAG],Diff_Mat[i][ZVAR][DIAG_U],Diff_Mat[i][ZVAR][DIAG_U2],IPIV[i][ZVAR],z_periodic_sol[i],&ldb,&info);
    dgttrs_(&Trans,&Nz_reduced,&NRHS,Diff_Mat_half[i][ZVAR][DIAG_L],Diff_Mat_half[i][ZVAR][DIAG],Diff_Mat_half[i][ZVAR][DIAG_U],Diff_Mat_half[i][ZVAR][DIAG_U2],IPIV_half[i][ZVAR],z_periodic_sol_half[i],&ldb,&info);
    
    
    
}

struct D_system Dsys_full={r,Diff_Mat,IPIV,Max_iterations,x_periodic_sol,y_periodic_sol,z_periodic_sol,IPIV_iter,Component_max};
struct D_system Dsys_half={r_half,Diff_Mat_half,IPIV_half,Max_iterations,x_periodic_sol_half,y_periodic_sol_half,z_periodic_sol_half,IPIV_iter,Component_max};

/* ***************************************************** End of the setup of the Solver system ****************************************** */



/* ***************************************************** Initial values and uptake values being set/read here ****************************************** */
//set inital values
if(init_val_fn[0]==0){  //if there is no filename specified for initialising the domain the domain will be initialized with the flat values provided  
    //... initialises the whole domain to a value ,if 0 then this can also be skipped since the arrays are calloced, watch out the domain is wraparound, meaning cells 0 and N-1 are wraparound cells
    printf("OK... reading init from values not file! \n");
    
    for(n=0;n<N_comp;n++){ //N_comp;n++){    
        //In this test case we only set the DA concentration to a non-zero value
        for(i=0;i<Nx_conc;i++){
            for(j=0;j<Ny_conc;j++){
                for(k=0;k<Nz_conc;k++){  
                    Conc_now_half[n][i][j][k]=init_vals[n]; //50 nmol dopamine for this test case
                    Conc_now[n][i][j][k]=init_vals[n];              //0.0;             
                            }
                        }
                    }
                }
}
else{  //if init_val_fn is a filename, this part will read this file and write all the datasets from the file into the initial conditions
       // It should be noted that if the simulation is restarted it will read the initial values from the provided checkpoint file, however that doesnt require another elseif fork, because the filename is already the checkpoint fn in init_val_fn
       // We read the restart values here because they will be periodizised like normal initial conditions
            hsize_t check_dims[1], max_dims[1];
            int rank;
            file_id = H5Fopen(init_val_fn,  H5F_ACC_RDONLY, H5P_DEFAULT); //opens the hdf 5 file with init values in it 
            for(n=0;n<N_comp;n++){  //now we read the hdf5 file and check if its the correct size, before writing it into the init_vals
                dataset_id = H5Dopen2(file_id, DATASETNAMES[n], H5P_DEFAULT);
                dcpl = H5Dget_create_plist (dataset_id);
                dataspace = H5Dget_space (dataset_id);
                rank      = H5Sget_simple_extent_ndims(dataspace);
                
                if (rank!=1){
                    printf("The initialization file has not the correct rank! \n");
                    exit(6);
                }
                else{
                    status  = H5Sget_simple_extent_dims(dataspace, check_dims, max_dims);
                    if(check_dims[0]!=dims[0]) {
                        printf("The data in the initialisation file does not have the right size! \n");
                        exit(6);
                    }
                    else{
                        status = H5Dread(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT, Read_buffer);
                        for(i=1;i<Nx+1;i++){
                            for(j=1;j<Ny+1;j++){
                                for(k=1;k<Nz+1;k++){  
                                    Conc_now_half[n][i][j][k]=Read_buffer[Nx*Ny*(i-1)+Nx*(j-1)+(k-1)]; 
                                    Conc_now[n][i][j][k]=Read_buffer[Nx*Ny*(i-1)+Nx*(j-1)+(k-1)];                           
                                }
                            }
                        }
                        
                        status = H5Pclose(dcpl);
                        status = H5Sclose(dataspace);
                        status =H5Dclose(dataset_id);
                        }
                    }
                }
            
            status = H5Fclose(file_id);
            if (status<0) printf("There was a problem closing %s \n",init_val_fn);
            
}   //end of reading the initial values





/* --------------- Here we check if we want uptake to be flat with the given V_max, or spatially unhomogenous with the sum of all length in cells of the active axons, or if the uptake is inhomogenous specified in one file */
/* If the switch is "flat" it does flat, if the switch is "one_file" it will read from one file, otherwise it will sum up the length in cells of the active axons specified in the axons_used file*/
/* If restart is specified, it will read the V_spatial from the checkpoint file, we read the restart V_spatial values here because then they will be periodizised like normal initial conditions*/
//This logic is organically grown... and not very nice and tidy 
if (restart_flag==1){
    //in case of restarted simulation just copy V_spatial from the checkpoint file
    file_id = H5Fopen(init_val_fn,  H5F_ACC_RDONLY, H5P_DEFAULT); //reads the hdf 5 file with the spatially varying uptake in it
    dataset_id = H5Dopen2(file_id, "V_DA", H5P_DEFAULT);
    
    dcpl = H5Dget_create_plist (dataset_id);
    
    dataspace = H5Dget_space (dataset_id);
    
    status = H5Dread(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT, Read_buffer);

    
    status = H5Pclose(dcpl);
    status = H5Sclose(dataspace);
    status =H5Dclose(dataset_id);
    status = H5Fclose(file_id);
    if (status<0) printf("There was a problem closing %s \n",init_val_fn);
    
    
    //set inital values for spatially varying uptake-> since the uptake doesn't change the initial value is used for all timesteps, if this needs to be changed it can also get newer values in the advance_T loop
    //set inital values for spatially varying uptake-> since the uptake doesn't change the initial value is used for all timesteps, if this needs to be changed it can also get newer values in the advance_T loop
    for(n=0;n<1;n++){   //Set just the DA uptake to a value
        for(i=1;i<Nx+1;i++){
            for(j=1;j<Ny+1;j++){
                for(k=1;k<Nz+1;k++){  
                    V_spatial[n][i][j][k]=Read_buffer[Nx*Ny*(i-1)+Nx*(j-1)+(k-1)];       

                            }
                        }
                    }
                }
    
    for(n=1;n<N_comp;n++){   //Set just the DA uptake to a value
        for(i=1;i<Nx+1;i++){
            for(j=1;j<Ny+1;j++){
                for(k=1;k<Nz+1;k++){  
                    V_spatial[n][i][j][k]=0.0;       

                            }
                        }
                    }
                }
    
    
    
}
else {
    if (strcmp(V_max_flat_switch,"flat") != 0) {
            if(strcmp(V_max_flat_switch,"one_file") == 0) { // in case of the one_file option the non-homogenous uptake will just be read from this one file
                memset(filename,0,sizeof(filename));
                sprintf(filename, "./Resources/Axon_l_in_c_one.h5");    //only add the length in cube of the active axons to the uptake
                
                file_id = H5Fopen(filename,  H5F_ACC_RDONLY, H5P_DEFAULT); //reads the hdf 5 file with the spatially varying uptake in it
                dataset_id = H5Dopen2(file_id, "Length_in_cell", H5P_DEFAULT);
                
                dcpl = H5Dget_create_plist (dataset_id);
                
                
                dataspace = H5Dget_space (dataset_id);
                
                status = H5Dread(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT, Read_buffer);

                
                status = H5Pclose(dcpl);
                status = H5Sclose(dataspace);
                status =H5Dclose(dataset_id);
                status = H5Fclose(file_id);
                if (status<0) printf("There was a problem closing %s \n",filename);
                
                
                
                
                //Write out the L in c used for possible future reference 
                Write_L_in_C(Read_buffer, Nx, Ny, Nz);
                
                
                //set inital values for spatially varying uptake-> since the uptake doesn't change the initial value is used for all timesteps, if this needs to be changed it can also get newer values in the advance_T loop
                //set inital values for spatially varying uptake-> since the uptake doesn't change the initial value is used for all timesteps, if this needs to be changed it can also get newer values in the advance_T loop
                for(n=0;n<1;n++){   //Set just the DA uptake to a value
                    for(i=1;i<Nx+1;i++){
                        for(j=1;j<Ny+1;j++){
                            for(k=1;k<Nz+1;k++){  
                                V_spatial[n][i][j][k]=Read_buffer[Nx*Ny*(i-1)+Nx*(j-1)+(k-1)]*V_max_corrected;       

                                        }
                                    }
                                }
                            }
                            
                for(n=1;n<N_comp;n++){   //the uptake for non DA is set to 0
                    for(i=1;i<Nx+1;i++){
                        for(j=1;j<Ny+1;j++){
                            for(k=1;k<Nz+1;k++){  
                                V_spatial[n][i][j][k]=0.0; //Read_buffer[Nx*Ny*(i-1)+Nx*(j-1)+(k-1)]*V_max_corrected;       

                                        }
                                    }
                                }
                            }
                    
                    
            }
            else{
            //if the 'flat' or 'one_file' option is not specified the V_max will be taken from the appropriate hdf5 files (sum of the length in cube files of the used axons!)
                Accumulate_buffer=(double*) calloc(Nx*Ny*Nz,sizeof(double));    //Buffer to accumulate the multiple axon files into
                for(i=0;i<Nr_active_axons;i++) {
                    //set the correct filename
                    memset(filename,0,sizeof(filename));
                    sprintf(filename, "./Resources/Axon_l_in_c_nr_%.5d.h5", which_axons[i]);    //only add the length in cube of the active axons to the uptake

                    
                    file_id = H5Fopen(filename,  H5F_ACC_RDONLY, H5P_DEFAULT); //reads the hdf 5 file with the spatially varying uptake in it
                    dataset_id = H5Dopen2(file_id, "Length_in_cell", H5P_DEFAULT);
                    
                    dcpl = H5Dget_create_plist (dataset_id);
                    
                    
                    dataspace = H5Dget_space (dataset_id);
                    
                    status = H5Dread(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT, Read_buffer);
                    for(j=0;j<Nx*Ny*Nz;j++) Accumulate_buffer[j]+=Read_buffer[j];
                    

                    
                    status = H5Pclose(dcpl);
                    status = H5Sclose(dataspace);
                    status =H5Dclose(dataset_id);
                    status = H5Fclose(file_id);
                    if (status<0) printf("There was a problem closing %s \n",filename);

                }
                

                //Since there have been some problems this is a hotfix for the accumulate buffer
                for(j=0;j<Nx*Ny*Nz;j++){
                     if (Accumulate_buffer[j]<0) Accumulate_buffer[j]=0;
                }
                // end of hotfix
                
                
                //Write out the L in c used for possible future reference                 
                Write_L_in_C(Accumulate_buffer, Nx, Ny, Nz);



                //set inital values for spatially varying uptake-> since the uptake doesn't change the initial value is used for all timesteps, if this needs to be changed it can also get newer values in the advance_T loop
                //set inital values for spatially varying uptake-> since the uptake doesn't change the initial value is used for all timesteps, if this needs to be changed it can also get newer values in the advance_T loop
                for(n=0;n<1;n++){   //Set just the DA uptake to a value
                    for(i=1;i<Nx+1;i++){
                        for(j=1;j<Ny+1;j++){
                            for(k=1;k<Nz+1;k++){  
                                    V_spatial[n][i][j][k]=Accumulate_buffer[Nx*Ny*(i-1)+Nx*(j-1)+(k-1)]*V_max_corrected;       

                                    }
                                }
                            }
                        }
                            
                for(n=1;n<N_comp;n++){   //the uptake for non DA is set to 0
                    for(i=1;i<Nx+1;i++){
                        for(j=1;j<Ny+1;j++){
                            for(k=1;k<Nz+1;k++){  
                                    V_spatial[n][i][j][k]=0.0; //Read_buffer[Nx*Ny*(i-1)+Nx*(j-1)+(k-1)]*V_max_corrected;       

                                    }
                                }
                            }
                        }


                free(Accumulate_buffer);
        }
    }
    else {
        // Here the uptake in the flat case is handled
        //DA uptake
        for(n=0;n<1;n++){
            for(i=1;i<Nx+1;i++){
                for(j=1;j<Ny+1;j++){
                    for(k=1;k<Nz+1;k++){  
                        V_spatial[n][i][j][k]=V_max;       

                                }
                            }
                        }
                    }
        //Uptake for receptors... 0 since receptors are not taken up            
        for(n=1;n<N_comp;n++){
            for(i=1;i<Nx+1;i++){
                for(j=1;j<Ny+1;j++){
                    for(k=1;k<Nz+1;k++){  
                        V_spatial[n][i][j][k]=0.0;       

                                }
                            }
                        }
                    }
        
        
    }
}
/* ***************************************************** Initial values and uptake values being set/read here DONE ****************************************** */




            
/*------------------------------------------------------------ Periodization -------------------------------------------- */            
//periodize the initial conditions
for(n=0;n<N_comp;n++){
//Fill the faces of the cube periodically (edges and corners are going to be ffilled further down)
//x direction face   
for(j=1;j<Ny_calc+1;j++){
    for(k=1;k<Nz_calc+1;k++){
        Conc_now[n][0][j][k]=Conc_now[n][Nx_conc-2][j][k]; //Double periodic filling of the conc array 
        Conc_now[n][Nx_conc-1][j][k]=Conc_now[n][1][j][k];
        Conc_now_half[n][0][j][k]=Conc_now_half[n][Nx_conc-2][j][k]; //Double periodic filling of the conc array 
        Conc_now_half[n][Nx_conc-1][j][k]=Conc_now_half[n][1][j][k];
        
        V_spatial[n][0][j][k]=V_spatial[n][Nx_conc-2][j][k]; //Double periodic filling of the V_spatial array 
        V_spatial[n][Nx_conc-1][j][k]=V_spatial[n][1][j][k];
            }
        }
                    
//y direction face
for(i=1;i<Nx_calc+1;i++){
    for(k=1;k<Nz_calc+1;k++){
        Conc_now[n][i][0][k]=Conc_now[n][i][Ny_conc-2][k]; //Double periodic filling of the conc array 
        Conc_now[n][i][Ny_conc-1][k]=Conc_now[n][i][1][k];
        Conc_now_half[n][i][0][k]=Conc_now_half[n][i][Ny_conc-2][k]; //Double periodic filling of the conc array 
        Conc_now_half[n][i][Ny_conc-1][k]=Conc_now_half[n][i][1][k];
        
        V_spatial[n][i][0][k]=V_spatial[n][i][Ny_conc-2][k]; //Double periodic filling of the V_spatial array 
        V_spatial[n][i][Ny_conc-1][k]=V_spatial[n][i][1][k];
                }
            }
//z direction face
for(i=1;i<Nx_calc+1;i++){
    for(j=1;j<Ny_calc+1;j++){
        Conc_now[n][i][j][0]=Conc_now[n][i][j][Nz_conc-2]; //Double periodic filling of the conc array 
        Conc_now[n][i][j][Nz_conc-1]=Conc_now[n][i][j][1];
        Conc_now_half[n][i][j][0]=Conc_now_half[n][i][j][Nz_conc-2]; //Double periodic filling of the conc array 
        Conc_now_half[n][i][j][Nz_conc-1]=Conc_now_half[n][i][j][1];
        
        V_spatial[n][i][j][0]=V_spatial[n][i][j][Nz_conc-2]; //Double periodic filling of the V_spatial array
        V_spatial[n][i][j][Nz_conc-1]=V_spatial[n][i][j][1];
                                
            }
        }
        
//now we fill the edges periodically   
//along x -direction ... these are 4 edges running along the y-axis but without the corners     
for(i=1;i<Nx_calc+1;i++){
    Conc_now[n][i][0][0]=Conc_now[n][i][Ny_conc-2][Nz_conc-2];
    Conc_now[n][i][Ny_conc-1][0]=Conc_now[n][i][1][Nz_conc-2];
    Conc_now[n][i][0][Nz_conc-1]=Conc_now[n][i][Ny_conc-2][1];
    Conc_now[n][i][Ny_conc-1][Nz_conc-1]=Conc_now[n][i][1][1];
    
    Conc_now_half[n][i][0][0]=Conc_now_half[n][i][Ny_conc-2][Nz_conc-2];
    Conc_now_half[n][i][Ny_conc-1][0]=Conc_now_half[n][i][1][Nz_conc-2];
    Conc_now_half[n][i][0][Nz_conc-1]=Conc_now_half[n][i][Ny_conc-2][1];
    Conc_now_half[n][i][Ny_conc-1][Nz_conc-1]=Conc_now_half[n][i][1][1];
    
    V_spatial[n][i][0][0]=V_spatial[n][i][Ny_conc-2][Nz_conc-2];
    V_spatial[n][i][Ny_conc-1][0]=V_spatial[n][i][1][Nz_conc-2];
    V_spatial[n][i][0][Nz_conc-1]=V_spatial[n][i][Ny_conc-2][1];
    V_spatial[n][i][Ny_conc-1][Nz_conc-1]=V_spatial[n][i][1][1];
    
    }
                        
//along y -direction ... these are 4 edges running along the y-axis but without the corners
for(i=1;i<Ny_calc+1;i++){
    Conc_now[n][0][i][0]=Conc_now[n][Nx_conc-2][i][Nz_conc-2];
    Conc_now[n][Nx_conc-1][i][0]=Conc_now[n][1][i][Nz_conc-2];
    Conc_now[n][0][i][Nz_conc-1]=Conc_now[n][Nx_conc-2][i][1];
    Conc_now[n][Nx_conc-1][i][Nz_conc-1]=Conc_now[n][1][i][1];
    
    Conc_now_half[n][0][i][0]=Conc_now_half[n][Nx_conc-2][i][Nz_conc-2];
    Conc_now_half[n][Nx_conc-1][i][0]=Conc_now_half[n][1][i][Nz_conc-2];
    Conc_now_half[n][0][i][Nz_conc-1]=Conc_now_half[n][Nx_conc-2][i][1];
    Conc_now_half[n][Nx_conc-1][i][Nz_conc-1]=Conc_now_half[n][1][i][1];
    
    V_spatial[n][0][i][0]=V_spatial[n][Nx_conc-2][i][Nz_conc-2];
    V_spatial[n][Nx_conc-1][i][0]=V_spatial[n][1][i][Nz_conc-2];
    V_spatial[n][0][i][Nz_conc-1]=V_spatial[n][Nx_conc-2][i][1];
    V_spatial[n][Nx_conc-1][i][Nz_conc-1]=V_spatial[n][1][i][1];
    
    }
                    
//along z -direction ... these are 4 edges running along the x-axis but without the corners
for(i=0;i<Nz_calc+1;i++){
    Conc_now[n][0][0][i]=Conc_now[n][Nx_conc-2][Ny_conc-2][i];
    Conc_now[n][Nx_conc-1][0][i]=Conc_now[n][1][Ny_conc-2][i];
    Conc_now[n][0][Ny_conc-1][i]=Conc_now[n][Nx_conc-2][1][i];
    Conc_now[n][Nx_conc-1][Ny_conc-1][i]=Conc_now[n][1][1][i];
    
    Conc_now_half[n][0][0][i]=Conc_now_half[n][Nx_conc-2][Ny_conc-2][i];
    Conc_now_half[n][Nx_conc-1][0][i]=Conc_now_half[n][1][Ny_conc-2][i];
    Conc_now_half[n][0][Ny_conc-1][i]=Conc_now_half[n][Nx_conc-2][1][i];
    Conc_now_half[n][Nx_conc-1][Ny_conc-1][i]=Conc_now_half[n][1][1][i];
    
    V_spatial[n][0][0][i]=V_spatial[n][Nx_conc-2][Ny_conc-2][i];
    V_spatial[n][Nx_conc-1][0][i]=V_spatial[n][1][Ny_conc-2][i];
    V_spatial[n][0][Ny_conc-1][i]=V_spatial[n][Nx_conc-2][1][i];
    V_spatial[n][Nx_conc-1][Ny_conc-1][i]=V_spatial[n][1][1][i];
    }        

//Now only the corners!
Conc_now[n][0][0][0]=Conc_now[n][Nx_conc-2][Ny_conc-2][Nz_conc-2];
Conc_now[n][Nx_conc-1][0][0]=Conc_now[n][1][Ny_conc-2][Nz_conc-2];
Conc_now[n][0][Ny_conc-1][0]=Conc_now[n][Nx_conc-2][1][Nz_conc-2];
Conc_now[n][0][0][Nz_conc-1]=Conc_now[n][Nx_conc-2][Ny_conc-2][1];
Conc_now[n][Nx_conc-1][Ny_conc-1][0]=Conc_now[n][1][1][Nz_conc-2];
Conc_now[n][Nx_conc-1][0][Nz_conc-1]=Conc_now[n][1][Ny_conc-2][1];
Conc_now[n][0][Ny_conc-1][Nz_conc-1]=Conc_now[n][Nx_conc-2][1][1];
Conc_now[n][Nx_conc-1][Ny_conc-1][Nz_conc-1]=Conc_now[n][1][1][1];

Conc_now_half[n][0][0][0]=Conc_now_half[n][Nx_conc-2][Ny_conc-2][Nz_conc-2];
Conc_now_half[n][Nx_conc-1][0][0]=Conc_now_half[n][1][Ny_conc-2][Nz_conc-2];
Conc_now_half[n][0][Ny_conc-1][0]=Conc_now_half[n][Nx_conc-2][1][Nz_conc-2];
Conc_now_half[n][0][0][Nz_conc-1]=Conc_now_half[n][Nx_conc-2][Ny_conc-2][1];
Conc_now_half[n][Nx_conc-1][Ny_conc-1][0]=Conc_now_half[n][1][1][Nz_conc-2];
Conc_now_half[n][Nx_conc-1][0][Nz_conc-1]=Conc_now_half[n][1][Ny_conc-2][1];
Conc_now_half[n][0][Ny_conc-1][Nz_conc-1]=Conc_now_half[n][Nx_conc-2][1][1];
Conc_now_half[n][Nx_conc-1][Ny_conc-1][Nz_conc-1]=Conc_now_half[n][1][1][1];

V_spatial[n][0][0][0]=V_spatial[n][Nx_conc-2][Ny_conc-2][Nz_conc-2];
V_spatial[n][Nx_conc-1][0][0]=V_spatial[n][1][Ny_conc-2][Nz_conc-2];
V_spatial[n][0][Ny_conc-1][0]=V_spatial[n][Nx_conc-2][1][Nz_conc-2];
V_spatial[n][0][0][Nz_conc-1]=V_spatial[n][Nx_conc-2][Ny_conc-2][1];
V_spatial[n][Nx_conc-1][Ny_conc-1][0]=V_spatial[n][1][1][Nz_conc-2];
V_spatial[n][Nx_conc-1][0][Nz_conc-1]=V_spatial[n][1][Ny_conc-2][1];
V_spatial[n][0][Ny_conc-1][Nz_conc-1]=V_spatial[n][Nx_conc-2][1][1];
V_spatial[n][Nx_conc-1][Ny_conc-1][Nz_conc-1]=V_spatial[n][1][1][1];


            
}
/*------------------------------------------------------------ Periodization ENDS -------------------------------------------- */ 

if(restart_flag==0){
    //Write out Initial conditions!
    file_nr=0;
    Write_DA_diff( Conc_now, Write_buffer, DATASETNAMES, N_comp, Nx, Ny, Nz, file_nr); 
    file_nr++;

    //also write an xdmf file with the right info to accompany the hdf5 file
    memset(fn_xdmf,0,sizeof(fn_xdmf));   
    sprintf(fn_xdmf, "DA_diff_nr_00000.xdmf");
    Write_xdmf(fn_xdmf, filename, N_comp, DATASETNAMES,Nx,Ny,Nz);




    //Write out coordinates of all used synapses on all used axons
    sprintf(Axons_used_diff_code_fn, "Axons_used_diff_code.dat");
    FILE* f = fopen(Axons_used_diff_code_fn, "w");
    fprintf(f,"Axon_nr  Synapse_nr  x_coord y_coord z_coord Spiketrain_used\n");
    for(i=0;i<Nr_active_axons;i++){
        for(j=0;j<nr_synapses[which_axons[i]];j++){
            fprintf(f,"%d   %d  %d  %d  %d  %d\n",i,j,Axons_synapses[XVAR][which_axons[i]][j],Axons_synapses[YVAR][which_axons[i]][j],Axons_synapses[ZVAR][which_axons[i]][j],which_axons[i]);
            }
        }
    fclose(f);



    it_count=0;
    mean_file_nr=1;
    chk_file_nr=1;
    t_start=0.0;    //if not restarted start from 0
    if(T_chunk>0){  //if T_chunk is 0 or negative, T_max just stays T_max
        if(Tmax>T_chunk) Tmax=T_chunk;    //just run to the T_chunk endpoint not to the T_max endpoint
    }
}
else{   //on restart we write the initial conditions of the restart (with -file_nr) a new axons_used_restart file and set the spiketrains to the state of the old simulation
    file_id = H5Fopen(init_val_fn,  H5F_ACC_RDONLY, H5P_DEFAULT);   //open checkpoint file

    //Set next_axon_fire to checkpoint
    dataset_id = H5Dopen2(file_id, "Next_axon_fire", H5P_DEFAULT);
    dcpl = H5Dget_create_plist (dataset_id);
    dataspace = H5Dget_space (dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT, Next_axon_fire);
    status = H5Pclose(dcpl);
    status = H5Sclose(dataspace);
    status =H5Dclose(dataset_id);

    //Set which_axons to checkpoint
    dataset_id = H5Dopen2(file_id, "which_axons", H5P_DEFAULT);
    dcpl = H5Dget_create_plist (dataset_id);
    dataspace = H5Dget_space (dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, dataspace, H5P_DEFAULT, which_axons);
    status = H5Pclose(dcpl);
    status = H5Sclose(dataspace);
    status =H5Dclose(dataset_id);

    //Set which_spike to checkpoint
    dataset_id = H5Dopen2(file_id, "which_spike", H5P_DEFAULT);
    dcpl = H5Dget_create_plist (dataset_id);
    dataspace = H5Dget_space (dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, dataspace, H5P_DEFAULT, which_spike);
    status = H5Pclose(dcpl);
    status = H5Sclose(dataspace);
    status =H5Dclose(dataset_id);

    //Read the old Mean values
    for(n=0;n<N_comp;n++){ 
        //Now we also have to read and set the Mean_D1,D2,DA values. 
        strcpy(temp_datasetname, "Mean_");
        strcat(temp_datasetname, DATASETNAMES[n]);

        dataset_id = H5Dopen2(file_id, temp_datasetname, H5P_DEFAULT);
        dcpl = H5Dget_create_plist (dataset_id);
        dataspace = H5Dget_space (dataset_id);
        status = H5Dread(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT, Read_buffer);
        status = H5Pclose(dcpl);
        status = H5Sclose(dataspace);
        status =H5Dclose(dataset_id);

        for(i=1;i<Nx+1;i++){
            for(j=1;j<Ny+1;j++){
                for(k=1;k<Nz+1;k++){  
                    Conc_running_mean[n][i][j][k]=Read_buffer[Nx*Ny*(i-1)+Nx*(j-1)+(k-1)];       

                        }
                    }
                }
    }

    //Read the file_nr mean_file_nr_it_count and current t
    //read_file_nr
    attr = H5Aopen(file_id, "file_nr", H5P_DEFAULT);
    status = H5Aread(attr, H5T_NATIVE_INT, &file_nr);
    H5Aclose(attr);

    //read_mean_file_nr
    attr = H5Aopen(file_id, "mean_file_nr", H5P_DEFAULT);
    status = H5Aread(attr, H5T_NATIVE_INT, &mean_file_nr);
    H5Aclose(attr);
    
    //read_chk_file_nr
    attr = H5Aopen(file_id, "chk_file_nr", H5P_DEFAULT);
    status = H5Aread(attr, H5T_NATIVE_INT, &chk_file_nr);
    H5Aclose(attr);

    //read_it_count
    attr = H5Aopen(file_id, "it_count", H5P_DEFAULT);
    status = H5Aread(attr, H5T_NATIVE_INT, &it_count);
    H5Aclose(attr);

    //read_t
    attr = H5Aopen(file_id, "t", H5P_DEFAULT);
    status = H5Aread(attr, H5T_NATIVE_DOUBLE, &t_start);
    H5Aclose(attr);
    //close checkpoint file (reading the checkpoint is over here)
    status = H5Fclose(file_id);
    if (status<0) printf("There was a problem closing %s \n",init_val_fn);

    if(T_chunk>0){  //if T_chunk is 0 or negative, T_max just stays T_max
        if (Tmax>(t_start+T_chunk)) Tmax=t_start+T_chunk; //T_max is reduced to t_start+t_chunk if T_max is farther away from T+start then the given chunk time
    }
    
    
    
    
    /* Now we write out some restart things*/
    //Write out restart Initial conditions, to show that it is not a normal file the file_nr will be -file_nr and should be equal to another file, the DA_Diff file at the time of writing the checkpoint
    Write_DA_diff( Conc_now, Write_buffer, DATASETNAMES, N_comp, Nx, Ny, Nz, -1*file_nr);  //Writes the current Conc_now into a DA_Diff_XXXXX.hdf5 and accompanying xdmf with XXXXX the file number

    //Write out coordinates of all used synapses on all used axons
    sprintf(Axons_used_diff_code_fn, "Axons_used_diff_code_restart_%.5d.dat",file_nr);
    FILE* f = fopen(Axons_used_diff_code_fn, "w");
    fprintf(f,"Axon_nr  Synapse_nr  x_coord y_coord z_coord Spiketrain_used\n");
    for(i=0;i<Nr_active_axons;i++){
        for(j=0;j<nr_synapses[which_axons[i]];j++){
            fprintf(f,"%d   %d  %d  %d  %d  %d\n",i,j,Axons_synapses[XVAR][which_axons[i]][j],Axons_synapses[YVAR][which_axons[i]][j],Axons_synapses[ZVAR][which_axons[i]][j],which_axons[i]);
            }
        }
    fclose(f);
    file_nr++;
    

}

struct Fct_param F_param={V_max,K_m,k_unspecific,k_on_D1,k_on_D2,k_off_D1,k_off_D2,C_D1_max,C_D2_max}; //Set parameters for uptake










for(t=t_start;t<Tmax+1e-10;t+=dt){  //work loop with timesteps
    
    // !!! This is the full timestep !!!
        //Here the RHS is filled according to eq.38, without boundaries yet 
        Calc_stencil(N_comp,Nx_calc,Ny_calc,Nz_calc,Conc_now,RHS_1,&Dcoeff_full);
        Calc_fct_grid(N_comp,Nx_conc,Ny_conc,Nz_conc,Conc_now,fct_grid_now,&F_param,V_spatial); //Calculate the function value on all grid points
        Add_reaction_to_RHS(N_comp,Nx_calc,Ny_calc,Nz_calc,RHS_1,fct_grid_now,&f_coeff_full,dt);   // Here we add the reaction terms to RHS like in equation 38
        Advance_t(N_comp,Nx_calc,Ny_calc,Nz_calc,RHS_1,RHS_reorg,RHS_iter,&Dsys_full,fct_grid_now,Conc_now,Conc_next,Jacobi,Diff_Mat_iter,U,dt,&F_param,V_spatial);
        //Write solution in Conc_now

        
    
    // !!! This is two times the Half timestep (use it for the richardson extrapolation)
        for(l=0;l<2;l++){
            
            Calc_stencil(N_comp,Nx_calc,Ny_calc,Nz_calc,Conc_now_half,RHS_1,&Dcoeff_half);
            Calc_fct_grid(N_comp,Nx_conc,Ny_conc,Nz_conc,Conc_now_half,fct_grid_now,&F_param,V_spatial); //Calculate the function value on all grid points (for both of the half timesteps)
            Add_reaction_to_RHS(N_comp,Nx_calc,Ny_calc,Nz_calc,RHS_1,fct_grid_now,&f_coeff_half,dt*0.5);   // Here we add the reaction terms to RHS like in equation 38
            Advance_t(N_comp,Nx_calc,Ny_calc,Nz_calc,RHS_1,RHS_reorg,RHS_iter,&Dsys_half,fct_grid_now,Conc_now_half,Conc_next,Jacobi,Diff_Mat_iter,U,0.5*dt,&F_param,V_spatial);
            
            
        }
    
    
        for(n=0;n<N_comp;n++){
                for(i=0;i<Nx_conc;i++){
                    for(j=0;j<Ny_conc;j++){
                        for(k=0;k<Nz_conc;k++){
                              
                            // Richardson extrapolation see equation 41
                            
                            
                            temp=(4.0*Conc_now_half[n][i][j][k]-Conc_now[n][i][j][k])/3.0;
                            //check if larger than maximum of value (receptors), or if smaller than 0 (no negative concentrations)
                            if (temp>Component_max[n]) temp=Component_max[n];
                            if (temp<0) temp=0;
                            
                            Conc_now[n][i][j][k]=temp;
                            Conc_now_half[n][i][j][k]=temp;
                            Conc_running_mean[n][i][j][k]+=temp;                          
                        
                        }
                    }
                }
            }
    
        
        //Synaptic input goes here... that should be put in a function too!
        
        for(i=0;i<Nr_active_axons;i++){
            Next_axon_fire[i]-=dt;  //subtract timestep from Axon Fire time
            if(Next_axon_fire[i]<0){    //If time to next_axon fire time <=0 then the axon spikes, and all attached synapses release Dopamine 
                Curr_Rel_prob=Rel_prob;
                /* The PPF/PPD of the DA release lives in the following lines*/
                /* PPF and PPD utilizes quite a few ifs ... that should be ok since the Spiking part doesn't seem to be the part where a lot of time is spent*/
                
                
                if (strcmp(Spiking_switch,"PPF_NAC_VM") == 0){  //in case of PPF like in ventromedial NAC (See Cragg. 2003)
                    if (which_spike[which_axons[i]]!=0) {  //only apply PPF/PPD on spikes that re not the first spike
                        temp_neuron_number=which_axons[i];
                        ISI_to_last_spike=spiketrains[temp_neuron_number][which_spike[which_axons[i]]]-spiketrains[temp_neuron_number][which_spike[which_axons[i]]-1]; //time since last spike
                        Curr_Rel_prob=Rel_prob*PPF_NAC_VM_factor(ISI_to_last_spike*1000.0);   //Multiplies the Rel_probability with PPF_NAC_VM factor from the appropriate function
                        //if (i==2) printf("spike_nr=%d , Since last spike= %.8f , Curr_Rel_prob= %.8f \n",i,ISI_to_last_spike,Curr_Rel_prob);
                        }
                    
                } 
                
                if (strcmp(Spiking_switch,"PPD_PUT_DL") == 0){  //in case of PPD like in dorsolateral Putamen (See Cragg. 2003)
                    if (which_spike[which_axons[i]]!=0) {  //only apply PPF/PPD on spikes that re not the first spike
                        temp_neuron_number=which_axons[i];
                        ISI_to_last_spike=spiketrains[temp_neuron_number][which_spike[which_axons[i]]]-spiketrains[temp_neuron_number][which_spike[which_axons[i]]-1]; //time since last spike
                        Curr_Rel_prob=Rel_prob*PPF_PUT_DL_factor(ISI_to_last_spike*1000.0);   //Multiplies the Rel_probability with PPF_NAC_VM factor from the appropriate function
                        //if (i==2) printf("spike_nr=%d , Since last spike= %.8f , Curr_Rel_prob= %.8f \n",i,ISI_to_last_spike,Curr_Rel_prob);
                    }
                    
                } 
                    
                //if no PPF/PPF flags are set Curr_Rel_prob. is not modified and no PPF/PPD is happening
                
                /* End of PPF/PPD living space*/
            
                for(j=0;j<nr_synapses[which_axons[i]];j++){
                    rnd_nr=drand48();
                    if (rnd_nr<Curr_Rel_prob){   //Dopamine is only released with a probability Curr_rel_prob (Rel_prob modulated by PPF/PPD above)
                        x_syn=Axons_synapses[XVAR][which_axons[i]][j];
                        y_syn=Axons_synapses[YVAR][which_axons[i]][j];
                        z_syn=Axons_synapses[ZVAR][which_axons[i]][j];
                        
                        //spiking with normally distributed quantal size
                        This_Quant=-1.0;
                        while (This_Quant<0) {
                            rnd_nr=Box_Mueller_1_nr(); //normally distributed random number
                            This_Quant=(rnd_nr*Quant_std)+Quant;
                        }
                        
                        Conc_now_half[0][x_syn][y_syn][z_syn]+=(This_Quant/(hx*hy*hz*alpha*1.0e-15*Avogadro))/1.0e-6;
                        Conc_now[0][x_syn][y_syn][z_syn]+=(This_Quant/(hx*hy*hz*alpha*1.0e-15*Avogadro))/1.0e-6;
                        }
                    }
                    
                    which_spike[which_axons[i]]++;   //tracks at which spike of the spiketrain we are right now
                    if ((which_spike[which_axons[i]]==Max_nr_spikes) || (spiketrains[which_axons[i]][which_spike[which_axons[i]]]<0.001*dt) ){  //checks if we are at the end of a spiketrain... the spiketrains that are shorter than the longest have 0's in their array after they run out so next_spike<0.001*dt should evaluate true
                        Next_axon_fire[i]=1.0e80; 
                        printf("Spiketrain ran out of DATA! %d  %d  %d\n",N_spiketrains,which_spike[which_axons[i]],temp_neuron_number);
                    }
                    else Next_axon_fire[i]=spiketrains[which_axons[i]][which_spike[which_axons[i]]]-spiketrains[which_axons[i]][which_spike[which_axons[i]]-1];   //next_axon fire time is difference between current and next spike
                    //printf("Axon %d fired at %f. Next spike time %f in %f ms \n",which_axons[i],t,spiketrains[temp_neuron_number][which_spike[i]],Next_axon_fire[i]);    
                        
                    
                    }
            
            }
		

		it_count++;     //counts the timestep number ... this should be done before the writeout since the writeout checks how often we already have advanced... if this is done after writeout then writeout will always have an offset by 1 timestep
        

        
        if (it_count%mean_write_out_n==0){
            //Write out the current running mean
            Write_running_mean( Conc_running_mean, Write_buffer, DATASETNAMES, N_comp, Nx, Ny, Nz, mean_file_nr, mean_write_out_n); //Write out the average
            //Set the running average to 0 for the next running average chunk of write_out_mean seconds
            for(n=0;n<N_comp;n++){
                for(i=1;i<Nx+1;i++){
                    for(j=1;j<Ny+1;j++){
                        for(k=1;k<Nz+1;k++){  
                             Conc_running_mean[n][i][j][k]=0.0;
                        }
                    }
                }
            }
            mean_file_nr++;
            
        }
        
        //Write a chk every chk iterations, the chk will have the nr of the file_nr to be identifiable with DA_Diff files
        if (it_count%write_out_chk_n==0) {
            Write_Checkpoint(Conc_now, V_spatial, Conc_running_mean, Write_buffer,Next_axon_fire, which_axons, which_spike, DATASETNAMES, N_comp, Nx, Ny, Nz, chk_file_nr, file_nr, mean_file_nr, it_count, t+dt, Nr_active_axons, N_spiketrains);
            chk_file_nr++;
        }
        
        if(t>output_t_wait) {//wait until the dead time is over!
            if (it_count%write_out_n==0){
                
                Write_DA_diff( Conc_now, Write_buffer, DATASETNAMES, N_comp, Nx, Ny, Nz, file_nr); 
                file_nr++;
            }
        }
        
        
        
    } //end of the work loop


//Cleanup, deallocating memory and so forth!
deallocate_Conc(Conc_now,N_comp,Nx_conc,Ny_conc,Nz_conc);
deallocate_Conc(fct_grid_now,N_comp,Nx_conc,Ny_conc,Nz_conc);
deallocate_Conc(V_spatial,N_comp,Nx_conc,Ny_conc,Nz_conc);
deallocate_Conc(Conc_now_half,N_comp,Nx_conc,Ny_conc,Nz_conc);
deallocate_Conc(Conc_running_mean,N_comp,Nx_conc,Ny_conc,Nz_conc);
deallocate_Conc(Conc_next,N_comp,Nx_conc,Ny_conc,Nz_conc);
deallocate_D(D,N_comp);
deallocate_D(r,N_comp);
deallocate_D(r_half,N_comp);
deallocate_Diff_Mat(Diff_Mat,N_comp,Nx_reduced,Ny_reduced,Nz_reduced);
deallocate_Diff_Mat(Diff_Mat_half,N_comp,Nx_reduced,Ny_reduced,Nz_reduced);
deallocate_Jacobi(Jacobi,N_comp,Nx_conc,Ny_conc,Nz_conc);
deallocate_IPIV(IPIV,N_comp,Nx_calc,Ny_calc,Nz_calc);
deallocate_IPIV(IPIV_half,N_comp,Nx_calc,Ny_calc,Nz_calc);
deallocate_Conc(RHS_1,N_comp,Nx_calc,Ny_calc,Nz_calc);
deallocate_Conc(RHS_reorg,N_comp,Nx_calc,Ny_calc,Nz_calc);
deallocate_RHS_iter(RHS_iter,Nx_calc,Ny_calc,Nz_calc*N_comp);
free(ax);
free(ay);
free(az);
free(ayz);
free(axy);
free(axz);
free(axyz);

free(ax_half);
free(ay_half);
free(az_half);
free(ayz_half);
free(axy_half);
free(axz_half);
free(axyz_half);

free(fx);
free(fy);
free(fz);
free(fyz);
free(fxy);
free(fxz);
free(fxyz);

free(fx_half);
free(fy_half);
free(fz_half);
free(fyz_half);
free(fxy_half);
free(fxz_half);
free(fxyz_half);


//Free Axons_synapses memory
for(i=0;i<3;i++){
    for(j=0;j<Nr_axons;j++){
        free(Axons_synapses[i][j]);
        }
    free(Axons_synapses[i]);
    }
free(Axons_synapses);
free(Next_axon_fire);
free(which_axons);
free(which_spike);

free(nr_synapses);
free(length_in_cube);


for(i=0;i<N_comp;i++){
        free(x_periodic_sol[i]);
        free(y_periodic_sol[i]);
        free(z_periodic_sol[i]);
    }
free(x_periodic_sol);
free(y_periodic_sol);
free(z_periodic_sol);

for(i=0;i<N_comp;i++){
        free(x_periodic_sol_half[i]);
        free(y_periodic_sol_half[i]);
        free(z_periodic_sol_half[i]);
    }
free(x_periodic_sol_half);
free(y_periodic_sol_half);
free(z_periodic_sol_half);





free(Diff_Mat_iter);

for(i=0;i<N_spiketrains;i++) free(spiketrains[i]);
free(spiketrains);

for(i=0;i<N_comp;i++){
    free(DATASETNAMES[i]);
    }    
free(DATASETNAMES);
free(IPIV_iter);
free(U);

free(Write_buffer);
free(Read_buffer);

free(D_DA);
free(init_vals);
free(Component_max);
}
