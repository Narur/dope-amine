/*
 * 
 * 
 * This file DA_Diff_IO_utility.c is the file containing all functions for 
 * input/output used by the diffusion code DOPE-AMINE.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */

#include "DA_Diff_IO_utility.h"


int Write_Checkpoint( double**** Conc_now, double**** V_spatial, double**** Conc_running_mean, double * Write_buffer,double* Next_axon_fire, int* which_axons, int* which_spike, char** DATASETNAMES, int N_comp_out, int Nx, int Ny, int Nz,int chk_file_nr, int file_nr, int mean_file_nr, int it_count, double t, int Nr_active_axons, int N_spiketrains)
{
    //Writes out the DA_Diff files principle outputfiles. Also writes the accompanying xdmf file
    
    char filename[200];
    char V_datasetname[200];
    //Set some hdf5 parameters
    hsize_t dims[1] = {Nx*Ny*Nz};
    hsize_t chunk[1] = {Nx*Ny*Nz};
    
    
    hid_t           file, dataset,dcpl;    /* Handles */
    hid_t           dataspace,datatype,aid2;
    hid_t           attr;
    herr_t          status;
    int n,i,j,k;


    memset(filename,0,sizeof(filename));
    sprintf(filename, "Checkpoint_diff_nr_%.5d.chk", chk_file_nr);


    file = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    datatype  = H5Tcopy(H5T_NATIVE_DOUBLE);
    dataspace = H5Screate_simple (1, dims, NULL);
    aid2      = H5Screate(H5S_SCALAR);

    dcpl = H5Pcreate(H5P_DATASET_CREATE);
    status = H5Pset_shuffle (dcpl);
    status = H5Pset_deflate (dcpl, 9);
    status = H5Pset_chunk (dcpl, 1, chunk);
    


    /* Write file_nr as an attribute */
    attr = H5Acreate2(file, "file_nr", H5T_NATIVE_INT, aid2, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attr, H5T_NATIVE_INT, &file_nr);
    H5Aclose(attr);
    
    /* Write mean_file_nr as an attribute */
    attr = H5Acreate2(file, "mean_file_nr", H5T_NATIVE_INT, aid2, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attr, H5T_NATIVE_INT, &mean_file_nr);
    H5Aclose(attr);
    
    /* Write mean_file_nr as an attribute */
    attr = H5Acreate2(file, "chk_file_nr", H5T_NATIVE_INT, aid2, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attr, H5T_NATIVE_INT, &chk_file_nr);
    H5Aclose(attr);
    
    /* Write it_count as an attribute */
    attr = H5Acreate2(file, "it_count", H5T_NATIVE_INT, aid2, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attr, H5T_NATIVE_INT, &it_count);
    H5Aclose(attr);
    
     /* Write t as an attribute */
    attr = H5Acreate2(file, "t", H5T_NATIVE_DOUBLE, aid2, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attr, H5T_NATIVE_DOUBLE, &t);
    H5Aclose(attr);

    status = H5Sclose(aid2);

    /* *********** The Checkpoint requires the Conc_now values for everything  */
    for(n=0;n<N_comp_out;n++){  //Write one dataset for each component.
        for(i=0;i<Nx;i++){ //The indices here only write out the inner cells, since the boundary cells will be "wrapped around"
            for(j=0;j<Ny;j++){
                for(k=0;k<Nz;k++){  
                    Write_buffer[Nx*Ny*i+Ny*j+k]=Conc_now[n][i+1][j+1][k+1];    //Write Conc_now because it has the receptor activation values in case they are calculated                       
                            
                            }
                        }
                    }

        dataset = H5Dcreate (file, DATASETNAMES[n], H5T_NATIVE_DOUBLE, dataspace, H5P_DEFAULT, dcpl,H5P_DEFAULT);
        status = H5Dwrite (dataset, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT,Write_buffer);
        status = H5Dclose (dataset);
    }
    
    /* The V_spatial array is written out too, here it is just one component because the 1_comp code only requires one component.*/
    for(n=0;n<1;n++){ 
        for(i=0;i<Nx;i++){ //The indices here only write out the inner cells, since the boundary cells will be "wrapped around"
            for(j=0;j<Ny;j++){
                for(k=0;k<Nz;k++){  
                    Write_buffer[Nx*Ny*i+Ny*j+k]=V_spatial[n][i+1][j+1][k+1];    //Write Conc_now because it has the receptor activation values in case they are calculated                       
                            
                            }
                        }
                    }

        strcpy(V_datasetname, "V_");
        strcat(V_datasetname, DATASETNAMES[n]);
        
        dataset = H5Dcreate (file, V_datasetname, H5T_NATIVE_DOUBLE, dataspace, H5P_DEFAULT, dcpl,H5P_DEFAULT);
        status = H5Dwrite (dataset, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT,Write_buffer);
        status = H5Dclose (dataset);
    }

    /* Write out the conc_running_mean so that on the checkpoint the running mean can be continued*/
    for(n=0;n<N_comp_out;n++){  //Write one dataset for each component.
        for(i=0;i<Nx;i++){ //The indices here only write out the inner cells, since the boundary cells will be "wrapped around"
            for(j=0;j<Ny;j++){
                for(k=0;k<Nz;k++){  
                    Write_buffer[Nx*Ny*i+Ny*j+k]=Conc_running_mean[n][i+1][j+1][k+1];    //Write Conc_now because it has the receptor activation values in case they are calculated                       
                            
                            }
                        }
                    }

        strcpy(V_datasetname, "Mean_");
        strcat(V_datasetname, DATASETNAMES[n]);

        dataset = H5Dcreate (file, V_datasetname, H5T_NATIVE_DOUBLE, dataspace, H5P_DEFAULT, dcpl,H5P_DEFAULT);
        status = H5Dwrite (dataset, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT,Write_buffer);
        status = H5Dclose (dataset);
    }
    status = H5Pclose(dcpl);
    status = H5Sclose(dataspace);
    
    
    
    
    /* We also require the Next_axon_fire, which_axon and which_spike arrays*/
    
    /* This part writes the Next_axon_fire and which_axons*/ 
    hsize_t chunk_axons[1] = {Nr_active_axons};
    dataspace = H5Screate_simple (1, chunk_axons, NULL);
    
    dcpl = H5Pcreate(H5P_DATASET_CREATE);
    status = H5Pset_shuffle(dcpl);
    status = H5Pset_deflate(dcpl, 9);
    status = H5Pset_chunk(dcpl, 1, chunk_axons);
    
    
    dataset = H5Dcreate (file, "Next_axon_fire", H5T_NATIVE_DOUBLE, dataspace, H5P_DEFAULT, dcpl,H5P_DEFAULT);
    status = H5Dwrite (dataset, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT,Next_axon_fire);
    status = H5Dclose (dataset);
    
        
    dataset = H5Dcreate (file, "which_axons", H5T_NATIVE_INT, dataspace, H5P_DEFAULT, dcpl,H5P_DEFAULT);
    status = H5Dwrite (dataset, H5T_NATIVE_INT, H5S_ALL, dataspace, H5P_DEFAULT,which_axons);
    status = H5Dclose (dataset);
    
    status = H5Sclose(dataspace);
    status = H5Pclose(dcpl);
    
    
    hsize_t chunk_spikes[1] = {N_spiketrains};
    dataspace = H5Screate_simple (1, chunk_spikes, NULL);
    
    dcpl = H5Pcreate(H5P_DATASET_CREATE);
    status = H5Pset_shuffle(dcpl);
    status = H5Pset_deflate(dcpl, 9);
    status = H5Pset_chunk(dcpl, 1, chunk_spikes);   //just change the chunk size but keep the H5P list
    
    dataset = H5Dcreate (file, "which_spike", H5T_NATIVE_INT, dataspace, H5P_DEFAULT, dcpl,H5P_DEFAULT);
    status = H5Dwrite (dataset, H5T_NATIVE_INT, H5S_ALL, dataspace, H5P_DEFAULT,which_spike);
    status = H5Dclose (dataset);
    
    status = H5Tclose(datatype);
    status = H5Sclose(dataspace);
    status = H5Pclose(dcpl);
    
    status = H5Fclose(file);
    if (status<0) printf("There was a problem closing %s \n",filename);
    
    
        
    return 0;
}




int Write_DA_diff( double**** Conc_now, double * Write_buffer, char** DATASETNAMES, int N_comp_out, int Nx, int Ny, int Nz, int file_nr){
    //Writes out the DA_Diff files principle outputfiles. Also writes the accompanying xdmf file
    
    char filename[200], fn_xdmf[200];
    //Set some hdf5 parameters
    hsize_t dims[1] = {Nx*Ny*Nz};
    hsize_t chunk[1] = {Nx*Ny*Nz};
    
    hid_t           file, dataset,dcpl;    /* Handles */
    hid_t           dataspace,datatype;
    herr_t          status;
    int n,i,j,k;


    memset(filename,0,sizeof(filename));
    sprintf(filename, "DA_diff_nr_%.5d.h5", file_nr);


    file = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    datatype  = H5Tcopy(H5T_NATIVE_DOUBLE);
    dataspace = H5Screate_simple (1, dims, NULL);

    dcpl = H5Pcreate(H5P_DATASET_CREATE);
    status = H5Pset_shuffle (dcpl);
    status = H5Pset_deflate (dcpl, 9);
    status = H5Pset_chunk (dcpl, 1, chunk);


    for(n=0;n<N_comp_out;n++){  //Write one dataset for each component.
        for(i=0;i<Nx;i++){ //The indices here only write out the inner cells, since the boundary cells will be "wrapped around"
            for(j=0;j<Ny;j++){
                for(k=0;k<Nz;k++){  
                    Write_buffer[Nx*Ny*i+Ny*j+k]=Conc_now[n][i+1][j+1][k+1];    //Write Conc_now because it has the receptor activation values in case they are calculated                       
                            
                            }
                        }
                    }

        dataset = H5Dcreate (file, DATASETNAMES[n], H5T_NATIVE_DOUBLE, dataspace, H5P_DEFAULT, dcpl,H5P_DEFAULT);
        status = H5Dwrite (dataset, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT,Write_buffer);
        status = H5Dclose (dataset);
    }
    
    status = H5Tclose(datatype);
    status = H5Pclose(dcpl);
    status = H5Sclose(dataspace);
    status = H5Fclose(file);
    if (status<0) printf("There was a problem closing %s \n",filename);
    
    //also write an xdmf file with the right info to accompany the hdf5 file
    memset(fn_xdmf,0,sizeof(fn_xdmf));   
    sprintf(fn_xdmf, "DA_diff_nr_%.5d.xdmf", file_nr);
    Write_xdmf(fn_xdmf, filename, N_comp_out, DATASETNAMES,Nx,Ny,Nz);
    
    return 0;
}



int Write_L_in_C(double* buffer, int Nx, int Ny, int Nz){
    //Writes out the Length in Cell into a hdf5 file and accompanying xdmf ... this could probably be merged with the Write_DA_Diff
    char filename[200], fn_xdmf[200];
    char **TEMP_SETNAMES;

    //Set some hdf5 parameters
    hsize_t dims[1] = {Nx*Ny*Nz};
    hsize_t chunk[1] = {Nx*Ny*Nz};
    
    hid_t           file, dataset,dcpl;    /* Handles */
    hid_t           dataspace,datatype;
    herr_t          status;

    
    //Write out the L in c used for possible future reference 
    memset(filename,0,sizeof(filename));
    sprintf(filename, "L_in_C_used.h5");

    file = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    datatype  = H5Tcopy(H5T_NATIVE_DOUBLE);
    dataspace = H5Screate_simple (1, dims, NULL);

    dcpl = H5Pcreate (H5P_DATASET_CREATE);
    status = H5Pset_shuffle (dcpl);
    status = H5Pset_deflate (dcpl, 9);
    status = H5Pset_chunk (dcpl, 1, chunk);
                
    dataset = H5Dcreate (file, "Length_in_cell", H5T_NATIVE_DOUBLE, dataspace, H5P_DEFAULT, dcpl,H5P_DEFAULT);
    status = H5Dwrite (dataset, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT,buffer);
    status = H5Dclose (dataset);
    
    status = H5Tclose(datatype);
    status = H5Pclose (dcpl);

    status = H5Sclose (dataspace);
    status = H5Fclose (file);
    if (status<0) printf("There was a problem closing %s \n",filename);
    
    
    //also write an xdmf file with the right info
    TEMP_SETNAMES=(char**) calloc(1, sizeof(char*));     //Malloc the TEMP_SETNAMES for the Length in Cell array
    TEMP_SETNAMES[0]=(char*) calloc(256,sizeof(char));
    sprintf(TEMP_SETNAMES[0], "Length_in_cell");  
    
    memset(fn_xdmf,0,sizeof(fn_xdmf));   
    sprintf(fn_xdmf, "L_in_C_used.xdmf");
    
    Write_xdmf(fn_xdmf, filename, 1, TEMP_SETNAMES,Nx,Ny,Nz);
    
    free(TEMP_SETNAMES[0]);
    free(TEMP_SETNAMES);
    
    
return 0;    
}



int Write_running_mean( double**** conc_running_mean, double * Write_buffer, char** DATASETNAMES, int N_comp_out, int Nx, int Ny, int Nz, int file_nr, int nr_steps){
    //Writes out the DA_Diff files principle outputfiles. Also writes the accompanying xdmf file
    
    char filename[200], fn_xdmf[200];
    //Set some hdf5 parameters
    hsize_t dims[1] = {Nx*Ny*Nz};
    hsize_t chunk[1] = {Nx*Ny*Nz};
    
    hid_t           file, dataset,dcpl;    /* Handles */
    hid_t           dataspace,datatype;
    herr_t          status;
    int n,i,j,k;


    memset(filename,0,sizeof(filename));
    sprintf(filename, "Mean_diff_nr_%.5d.h5", file_nr);


    file = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    datatype  = H5Tcopy(H5T_NATIVE_DOUBLE);
    dataspace = H5Screate_simple (1, dims, NULL);

    dcpl = H5Pcreate(H5P_DATASET_CREATE);
    status = H5Pset_shuffle (dcpl);
    status = H5Pset_deflate (dcpl, 9);
    status = H5Pset_chunk (dcpl, 1, chunk);


    for(n=0;n<N_comp_out;n++){  //Write one dataset for each component.
        for(i=0;i<Nx;i++){ //The indices here only write out the inner cells, since the boundary cells will be "wrapped around"
            for(j=0;j<Ny;j++){
                for(k=0;k<Nz;k++){  
                    Write_buffer[Nx*Ny*i+Ny*j+k]=conc_running_mean[n][i+1][j+1][k+1]/nr_steps;    //Write Conc_now because it has the receptor activation values in case they are calculated                       
                            
                            }
                        }
                    }

        dataset = H5Dcreate (file, DATASETNAMES[n], H5T_NATIVE_DOUBLE, dataspace, H5P_DEFAULT, dcpl,H5P_DEFAULT);
        status = H5Dwrite (dataset, H5T_NATIVE_DOUBLE, H5S_ALL, dataspace, H5P_DEFAULT,Write_buffer);
        status = H5Dclose (dataset);
    }
    
    status = H5Tclose(datatype);
    status = H5Pclose(dcpl);
    status = H5Sclose(dataspace);
    status = H5Fclose(file);
    if (status<0) printf("There was a problem closing %s \n",filename);
    
    //also write an xdmf file with the right info to accompany the hdf5 file
    memset(fn_xdmf,0,sizeof(fn_xdmf));   
    sprintf(fn_xdmf, "Mean_diff_nr_%.5d.xdmf", file_nr);
    Write_xdmf(fn_xdmf, filename, N_comp_out, DATASETNAMES,Nx,Ny,Nz);
    
    return 0;
}

int Write_xdmf(char* fn, char* fn_of_h5, int N_comp, char ** DATASETNAMES, int Nx, int Ny, int Nz){
//This function writes a companion xdmf file for our hdf5 files.
int i;

FILE* f = fopen(fn, "w");
fprintf(f,"<?xml version=\"1.0\" ?>\n");
fprintf(f,"<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n");
fprintf(f,"<Xdmf Version=\"3.0\" xmlns:xi=\"[http://www.w3.org/2001/XInclude]\">\n");
fprintf(f," <Domain>\n");
fprintf(f,"  <Grid Name=\"%s\" GridType=\"Uniform\">\n",DATASETNAMES[0]);
fprintf(f,"    <Topology TopologyType=\"3DCoRectMesh\" Dimensions=\"%d %d %d\"/>\n",Nx+1,Ny+1,Nz+1);
fprintf(f,"    <Geometry GeometryType=\"ORIGIN_DXDYDZ\">\n");
fprintf(f,"         <DataItem Name=\"Origin\" Dimensions=\"3\" NumberType=\"Float\" Precision=\"4\" Format=\"XML\">\n");
fprintf(f,"             0 0 0\n");
fprintf(f,"         </DataItem>\n");
fprintf(f,"         <DataItem Name=\"Spacing\" Dimensions=\"3\" NumberType=\"Float\" Precision=\"4\" Format=\"XML\">\n");
fprintf(f,"             1 1 1\n");
fprintf(f,"         </DataItem>\n");
fprintf(f,"    </Geometry>\n");

for(i=0;i<N_comp;i++){  //Write an entry in the xdmf file for each dataset in the hdf5 
fprintf(f,"    <Attribute Name=\"%s\" AttributeType=\"Scalar\" Center=\"Cell\">\n",DATASETNAMES[i]);
fprintf(f,"	  <DataItem Dimensions=\"%d %d %d\" NumberType=\"Float\" Precision=\"8\" Format=\"HDF\">\n",Nx,Ny,Nz);
fprintf(f,"		 %s:/%s\n",fn_of_h5,DATASETNAMES[i]);
fprintf(f,"	  </DataItem>\n");
fprintf(f,"    </Attribute>\n");
}

fprintf(f,"\n");
fprintf(f,"  </Grid>\n");
fprintf(f," </Domain>\n");
fprintf(f,"</Xdmf>\n");


fclose(f);      
return 0;
}


