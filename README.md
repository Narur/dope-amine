Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.



This set of programs ( DOPE-AMINE ) calculates diffusion of neurotransmitters in the brain and their binding to receptors. 
It uses the assumption that the brain is a porous medium and tracks neurotransmitters diffusing through the extracellular space. 
In its essence it solves a reaction diffusion system with an arbitrary set of components.

The Method implemented here is the method described in <https://doi.org/10.1016/S0377-0427(02)00889-0> with periodic boundary conditions.

Results and work performed with this code, and more explanations regarding the science can be found in the thesis during which this code was developed: <https://etheses.whiterose.ac.uk/24350/>
  
After compiling with the makefile, the Setup_simulation.py script can be used to set up and subsequently run a simulation.  
The Default Parameters are for Dopamine diffusion in the striatum, and binding of dopamine to its receptors. 

The src files in this repo are:
DA_Diff_Rea_3D_MM_Bio_synapse_hdf5_suite.c   		The main solver, for the full reaction kinetics system.
DA_Diff_Rea_3D_MM_Bio_synapse_hdf5_suite_1_comp.c	The main solver, but only solving the diffusion equation (1-component -> Just the DA diffusion, no binding)
Make_Axons_uptake_dist_suite_v3.cpp			A code that generates artificial axons, to determine uptake and synapse positions for the diffusion codes
Make_spiketrains_all_with_CV_calc.py			A python script generating spiketrains for the dopaminergic neurons
Setup_simulation.py					A python script that reads the Default.ini and sets up a full simulation run.
Default.ini						Default initialization file for the Setup script, can be modified with runtime parameters or in the file
Ini_spec.ini						File for verifying if the parameters supplied to Setup_simulation are valid parameters.
Readme.md 						This readme.
Default_200mum.ini              More Default.ini (size 200 mum)
Default_100mum.ini              More Default.ini ... How can their be more than one default.ini ? That seems assinine!
COPYING                         THE GPL LICENSE
