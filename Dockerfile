# Pull base image.
FROM ubuntu:18.04
 
LABEL maintainer="Lars Hunger <Lars.Hunger.314@gmail.com>"

#Install Packages for project to build and run importantly hdf5, lapack, blas and cgal
RUN apt-get update && apt-get install -y -qq apt-utils make build-essential python-pip libc-dev libhdf5-serial-dev libcgal-dev libblas-dev liblapack-dev && rm -rf /var/lib/apt/lists/*

#Install python packages for the Scripts that control the simulation
RUN pip install numpy configobj docopt ast

#Copy and build the simulation
COPY . /dope-amin/
RUN make -C /dope-amin/ | tee make-cmd.log

