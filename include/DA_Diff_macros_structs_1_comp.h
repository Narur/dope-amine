/*
 * 
 * 
 * This file DA_Diff_macros_struct_1_comp.h is the header file containing all the macros and struct definitions used
 * in the 1 component solver of the diffusion code DOPE-AMINE only.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */



#ifndef DA_DIFF_MACROS_STRUCTS_1_COMP
#define DA_DIFF_MACROS_STRUCTS_1_COMP 1
struct D_system { double **r; double **** Diff_Mat; int *** IPIV; int Max_iterations; double **x_periodic_sol; double **y_periodic_sol; double **z_periodic_sol; double Component_max;};

#endif
