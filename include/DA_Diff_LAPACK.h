/*
 * 
 * 
 * This file DA_Diff_LAPACL.h is the header file containing all functions 
 * that are used for the LAPACK interface of the diffusion code DOPE-AMINE.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */



#ifndef DA_DIFF_LAPACK
#define DA_DIFF_LAPACK 1

void dgesv_(int * N, int* NRHS, double* A, int* LDA, int* IPIV, double* B, int* LDB, int* info);
//compute the solution to a real system of linear equations  A * X = B,

void dgbtrs_(char* TRANS, int* N, int* KL,int* KU,int* NRHS,double* AB, int* ldab,int* IPIV, double* B, int* ldb, int* info);
// solve a system of linear equations  A * X = B or A'* X = B with a general band matrix A using the LU factoriza-tion computed by DGBTRF

void dgbtrf_(int* M,int* N,int* KL,int* KU,double* AB, int* ldab,int* IPIV,int* info); 
// compute an LU factorization of a real m-by-n bandmatrix A using partial pivoting with row interchanges

void dgttrs_( char* TRANS, int* N, int* NRHS, double* DL, double* D, double* DU, double* DU2, int* IPIV, double* B, int* LDB, int* info   );
// DGTTRS solves one of the systems of equations A*X = B  or  A'*X = B, with a tridiagonal matrix A using the LU factorization computed by DGTTRF.

void dgttrf_(int * N, double* DL, double* D, double* DU, double* DU2, int* IPIV, int* info);
//DGTTRF - compute an LU factorization of a real tridiagonal matrix A using elimination with partial pivoting and row interchanges




#endif 
