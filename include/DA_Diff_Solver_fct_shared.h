/*
 * 
 * 
 * This file DA_Diff_Solver_fct_shared.h is the header file containing all functions 
 * that are used by both the 1 component and the multi-component
 * solver used by the diffusion code DOPE-AMINE.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */



#ifndef DA_DIFF_SOLVER_FCT_SHARED
#define DA_DIFF_SOLVER_FCT_SHARED 1

int Calc_stencil(int N_comp, int Nx_calc, int Ny_calc, int Nz_calc,double **** Conc_now, double **** RHS_1,struct Diff_coeff * Dcoeff);
// Calculates the RHS of equation 38 by using the Conc_array. The RHS is written into the RHS_1 array

int Add_reaction_to_RHS(int N_comp,int Nx_calc,int Ny_calc,int Nz_calc,double **** RHS_1,double ****fct_grid,struct Diff_coeff * f_coeff,double dt);
//Adds the reaction term to the RHS calculated by Calc_stencil.

double Box_Mueller_1_nr();
// returns a normally distributed number between 0 and 1

int Fill_Diff_Mat(double ** Diff_Mat,int N, double r,double h);
// Fills the diffusion matrix according to the used numerical method discusses in the Gu paper.

double PNext(double Rate);
//Deprecated: Has been used before the Spiketrains where delivered via file.

double PPF_NAC_VM_factor(double ISI_since_last_spike);
//Used as an implementation of Paired Pulse Facilitation, use of this has not been tested.

double PPF_PUT_DL_factor(double ISI_since_last_spike);
//Used as an implementation of Paired Pulse Facilitation, use of this has not been tested.



#endif 
