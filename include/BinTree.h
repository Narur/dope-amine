/*
 * 
 * 
 * This file BinTree.h is the header file containing the declaration for the BinTree class.  
 * BinTree is used by the Make_Axons code to create synthethical axon geometries and to 
 * calculate spatially varying uptake and synapse positions. This uptake and synapse positions can then be used by the diffusion code
 * DOPE-AMIN, however the BinTree and TreeNode class can also be used independently 
 * of the Diffusion code.  
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */
 
#ifndef BINTREE
#define BINTREE 1

#include <vector>
#include "TreeNode.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/point_generators_3.h>
#include <CGAL/algorithm.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/convex_hull_3.h>
#include <CGAL/centroid.h>
#include <CGAL/Cartesian_d.h>
#include <CGAL/MP_Float.h>
#include <CGAL/Approximate_min_ellipsoid_d.h>
#include <CGAL/Approximate_min_ellipsoid_d_traits_3.h>
#include <CGAL/squared_distance_3.h>
#include <CGAL/Segment_3.h>
#include <CGAL/Plane_3.h>

#define XVAR 0
#define YVAR 1
#define ZVAR 2

struct Spatial_Tree_coeff { double branching_angle_mean; double branching_angle_std; double kink_angle_mean; double kink_angle_std; double p_change_dir; double L_angle_mean; double L_angle_std; int Nr_axons; double size_exclusion; double dist_synapses;};

typedef CGAL::Exact_predicates_inexact_constructions_kernel  K;
typedef CGAL::Polyhedron_3<K>                     Polyhedron_3;
typedef K::Segment_3                              Segment_3;

// define point creator
typedef K::Point_3                                Point_3;
typedef K::Plane_3                                Plane_3;
typedef Polyhedron_3::Vertex_iterator Vertex_iterator;
typedef Polyhedron_3::Point_iterator Point_iterator;

typedef CGAL::MP_Float                                         ET;
typedef CGAL::Approximate_min_ellipsoid_d_traits_3<K, ET> Traits;
typedef Traits::Point                                          Point;
typedef CGAL::Approximate_min_ellipsoid_d<Traits> AME;





class BinTree
{
    public:
        std::vector<TreeNode*> nodes;	//The nodes that beng to the tree
        std::vector<double> bifurcation; 
        std::vector<int> nr_strahl_segments; 
        std::vector<int> length_hist; 
        
        std::vector<double> Synapse_x_pos; //The x positions of all the synapses on the trees
        std::vector<double> Synapse_y_pos; //The y positions of all the synapses on the trees  
        std::vector<double> Synapse_z_pos; //The z positions of all the synapses on the trees  
        
        //this is kind of a hack and perhaps shoukd be beautified
        std::vector<int> In_cube_nodes; //which nodes are in the cube and can have a synapse in the cube. One mention for each segment in the node
        std::vector<int> In_cube_segment;  //which segments of the node are in the cube so that segment_for_synapse[i] and nodes_for_synapse[i] identify a segment of a node that is partially inside the cube

        
        
        double avg_bifurcation_ratio;	//The average bifurcation ratio over all strahl levels
        int height;				
        int nr_branch_points;	//nr of branch points in the Tree
        int magnitude;			//how many nodes are in the tree
        int max_strahl_level;
        int Nr_synapses_attached;	
        double max_length;
        double avg_collateral_length;
        double total_collateral_Length;
        double length_in_cube;


        BinTree(double p_br, double p_el, int max_nr_branches); //constructor that creates a topological tree with a branching probability p_br and elongation probability p_el. p_stop is calculated by 1-p_el-p_br. The tree generation is also terminated after max_nr_branches is reached
        ~BinTree();        
        
        
        int Spread_uptake(double cube_start[3], double*** cube, double resolution, int cube_size);  //spreads the length of the axons onto the computing cube for DA_Diffusion 
        int Attach_synapses(struct  Spatial_Tree_coeff * coeff,double cube_start[3], double*** cube, double resolution, int cube_size); // Attaches synapses to the tree and removes dead zones around the synapses from the uptake_cube
        int Remake(double p_br, double p_el, int max_nr_branches); //make a new topological tree 
        int addnode(int par,int id, int sib); //creates a Treenode with par,id and sibling and adds it to the end of the nodes vector of the bianry tree
        int print_nodes(char* fn);  //print all nodes into filename fn
        int print_3D_pos(); //writes out the 3D position data of the tree 
        int print_3D_pos_py_readable(char* fn); //writes out the 3D position data of the tree to be read by python
        int Calc_levels();	//Calculate the level on which each node is. Starting with 0 at the root
        int Calc_strahl();   //This calculates the strahl level for each node, this assumes that all the nodes already got their isleave parameter set to true if they are a leave ... this is done in Calc_levels which should preclude this routine
        int Instrument();   // Instruments the given tree by calling Calc_levels and Calc_stahl_level
        int Calc_bifurcation(); // Calcs the nr of segments with the same strahl level and the bifurcation ratio for all levels... this is somewhat inefficient but should be fast enough
        int fill_length_hist(); //fills the length_hist array but Calc_Strahl needs to be run before else it will get confused with max_length
        int Make_3D_tree(struct Spatial_Tree_coeff * coeff); //The Tree after construction is just a topological tree. This function gives it spatial structure.
        int Reset_3D();  //Resets the 3D tree to the tree that was generated after Make_tree (all 3D growing info is reset... ). With this function different 3D trees can be grown out of the same topolgical tree.
};

#endif
