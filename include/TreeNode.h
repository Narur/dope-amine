/*
 * 
 * 
 * This file TreeNode.h is the header file containing the declaration for the TreeNode class. 
 * The TreeNode class is used to save TreeNodes that are used by the BinTree class. 
 * BinTree is used by the Make_Axons code to create synthethical axon geometries that are used by the diffusion code DOPE-AMIN
 * to calculate spatially varying uptake and synapse positions. However the BinTree and TreeNode class can also be used independently 
 * of the Diffusion code.  
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */
 

 
#ifndef TREENODE
#define TREENODE 1

#include <vector> //vector needs to be included so the header works on its own

class TreeNode
{
      
   public:
      int parent;   // parent of node
      int my_id;  //node_number
      int sibling;   // id of sibling
      int children[2];  //id of the children
      int strahl_level;
      int level;
      bool isleave;		//is the current node a leave
      int length_nr;    //how many segments this node has 
      double length;    //length of the combined node segments
      double branch_turning_angle;  //Saves the branch turning angle for each node. That describes the geometry of the branching.
      double branching_angle[2];    //Saves the branching angle for each node. That describes the geometry of the branching.
      
      
      bool strahl_segment_not_counted;
      TreeNode(int par,int id,int sib);
      
      std::vector<double> x_pos;  //The position info for each branch part in the tree
      std::vector<double> y_pos;
      std::vector<double> z_pos;

      std::vector<double> x_dir;  //each segment has a position (starting position) and then also a direction into which the segment is growing... that will be used later in the distribute
      std::vector<double> y_dir;  //function to spread the DA axons onto the computing grid. The direction vector will be 1 shorter than the position vector since the final growing direction 
      std::vector<double> z_dir;  // for the last segment will be decided when a new segment grows out of it on the next node.

      std::vector<double> last_dir;   //saves the vector direction of the vector going into the last node      
      
      
      

};

#endif
