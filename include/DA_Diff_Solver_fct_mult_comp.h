/*
 * 
 * 
 * This file DA_Diff_Solver_fct_mult_comp.h is the header file containing all functions 
 * that are used by the multi component solver of the diffusion code DOPE-AMINE only.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */



#ifndef DA_DIFF_SOLVER_FCT_MULT_COMP
#define DA_DIFF_SOLVER_FCT_MULT_COMP 1

int Advance_t(int N_comp,int Nx_calc,int Ny_calc,int Nz_calc,double ****RHS_1,double **** RHS_reorg, double*** RHS_iter, struct D_system * Dsys,double **** fct_grid,double ****Conc_now,double ****Conc_next,double ****Jacobi, double * Diff_Mat_iter,double * U,double dt,struct Fct_param * F_param,double ****V_spatial);
//Step the simulation state one step of size dt into the future.

int Block_vec_vec_product(double *U,double *vty,double *result,int m,int n);
//This function writes the result off the block_vector*vector product into the array result. U is a block vector with size m*n where vty is a vector with length m the result will be a vector of length n  

int Calc_fct_grid(int N_comp, int Nx, int Ny, int Nz,double **** Conc_now, double **** fct_grid,struct Fct_param * F_param, double **** V_spatial);
//Calculates the fct (the reaction part e.g. uptake and interactions of the system) for each cell directly 

int Create_RHS_w_Fct(int N_comp,int Nx_calc,int Ny_calc,int Nz_calc,double **** RHS_1, double *** RHS_iter,double **** Conc_now,double **** fct_grid,double **** Jacobi,double dt);
//Creates the Righthandside for the Function parts in the iterations. The first iteration uses the fct grid directly, but following iterations use this fct.

int Fill_Diff_Mat_iter(int i,int j,int N,double * Diff_Mat_iter,double **** Conc_now,double**** Jacobi,double **r,double dt, int Nr_comp);
//Fill the diffusion matrix during the iteration, using Jacobi terms etc. 

int Fill_Jacobi(int N_comp,int Nx,int Ny,int Nz,double **** Conc_now,double **** Jacobi,struct Fct_param * F_param,double ****V_spatial);
//Fill the Jacobi matrix. This depends on the interactions in the system.

int Fill_block_vector_U(int i,int j,int N,double * U,double **** Conc_now,double**** Jacobi,double **r,double dt, int Nr_comp);
// Fills the block vector U according to the SWM formula. That means we will have block A1 in the first block off this vector and Block Cn in the last block ... the rest will be 0*/

int Transpose_matrix(double * matrix,double* matrix_temp,int linelength,int ldab);
//Transposes the matrix supplied. 


#endif
