/*
 * 
 * 
 * This file DA_Diff_alloc_shared.h is the header file containing all function definitions for 
 * allocations shared by both the one and multiple component solver of the diffusion code DOPE-AMINE.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */



#ifndef DA_DIFF_ALLOC_SHARED
#define DA_DIFF_ALLOC_SHARED 1

double**** allocate_Conc(int N_comp, int Nx, int Ny, int Nz);
//allocate a 4D array containing e.g. concentrations for all components

int deallocate_Conc(double**** Conc,int N_comp,int Nx,int Ny,int Nz);
//deallocate the given Conc array

double** allocate_D(int N_comp);
//alloc a vector of diffusion coefficients for each component.

int deallocate_D(double** D,int N_comp);
//deallocates diffusion coefficients

double**** allocate_Diff_Mat(int N_comp, int Nx, int Ny, int Nz);
//allocate a Diffusion Matrix for the solver

int deallocate_Diff_Mat(double **** Diff_Mat,int N_comp,int Nx,int Ny,int Nz);
//deallocate a diffusion matrix

int*** allocate_IPIV(int N_comp,int Nx,int Ny,int Nz);
//allocate a Pivoting array for the LAPACK solver 

int deallocate_IPIV(int *** IPIV,int N_comp,int Nx,int Ny,int Nz);
//deallocate Pivoting array

double**** allocate_Jacobi(int N_comp,int Nx, int Ny, int Nz);
//allocate an array that keeps the Jacobi matrices for all components

int deallocate_Jacobi(double**** Jacobi,int N_comp,int Nx,int Ny,int Nz);
//deallocate jacobi Matrix array

double*** allocate_RHS_iter(int Nx,int Ny,int Nz);
//allocate a RHS array for iteration, only used by multi-comp solver

int deallocate_RHS_iter(double*** RHS_1,int Nx,int Ny,int Nz);
//deallocate RHS_iter array

#endif
