/*
 * 
 * 
 * This file DA_Diff_IO_utility.h is the header file containing all function definitions for 
 * input/output used by the diffusion code DOPE-AMINE.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */



#ifndef DA_DIFF_IO
#define DA_DIFF_IO 1

#include <hdf5.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int Write_Checkpoint( double**** Conc_now, double**** V_spatial, double**** Conc_running_mean, double * Write_buffer,double* Next_axon_fire, int* which_axons, int* which_spike, char** DATASETNAMES, int N_comp_out, int Nx, int Ny, int Nz,int chk_file_nr, int file_nr, int mean_file_nr, int it_count, double t, int Nr_active_axons, int N_spiketrains);
//Writes a Checkpoint as a HDF5 file, from which the simulation can be restarted. Contains the full state of the simulation: -Which file nrs are currently written -All the Dopamine and receptor occupied concentrations -Where in the provided spiketrains the simulation currently is in. These files can be used to restart the simulation. 

int Write_DA_diff( double**** Conc_now, double * Write_buffer, char** DATASETNAMES, int N_comp_out, int Nx, int Ny, int Nz, int file_nr);
//Writes out a hdf5 file containing the information in Conc_now (usually DA concentration and occupied receptor concentration). The file contains the N_comp datasets of size Nx*Ny*Nz contained in Conc_now. 

int Write_L_in_C(double* buffer, int Nx, int Ny, int Nz);
//Writes the Data in buffer into an hdf5 file containing a dataset of size NX*Ny*Nz and creates an accompanying xdmf file. So that data visualisation programs can use the created dataset. (Used usually for L_in_C data) 

int Write_running_mean( double**** conc_running_mean, double * Write_buffer, char** DATASETNAMES, int N_comp_out, int Nx, int Ny, int Nz, int file_nr, int nr_steps);
//Writes out the running mean of the simulation as a hdf5 file. It calculates the mean from the conc_running_mean array and the nr_steps. This could probably be combined with Writes_DA_Diff in a next iteration of code beautification. Also writes an accompanying xdmf file.

int Write_xdmf(char* fn, char* fn_of_h5, int N_comp, char ** DATASETNAMES, int Nx, int Ny, int Nz);
//Writes an xdmf file with name fn to accompany an hdf5 file with name fn_of_h5 So that the hdf5 file can be read by visualisation programs like e.g. paraview. 


#endif 
