/*
 * 
 * 
 * This file Make_Axon_utility_fcts.h is the header file containing the declaration for the Utility functions of Make_Axons code.
 * These functions are also used by the BinTree class.  
 * These functions are part of the Make_axons utility of the DOPE-AMIN code suite. 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */



#ifndef MAKE_AXON_UTILITY_FCTS
#define MAKE_AXON_UTILITY_FCTS 1

#include <vector>



double Box_Mueller_1_nr(); /* returns a normally distributed number between 0 and 1 */

std::vector<double> Rotate_vec(std::vector<double> vec,std::vector<double> axis, double angle); /* Rotates vec by an angle angle (in degrees) around the axis axis. vec and axis should be vectors with 3 elements ... if not sth. is wrong*/

std::vector<double> Find_perpendicular(std::vector<double> vec); /* Calculates a unit vector perpendicular to the unit vector vec and returns it*/

std::vector<double> shear_out(std::vector<double> vec,double angle); /* shears a vector out of the direction it moves in*/ 

#endif
