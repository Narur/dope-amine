/*
 * 
 * 
 * This file DA_Diff_macros_struct.h is the header file containing all the macros and struct definitions used
 * in the diffusion code DOPE-AMINE.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */



#ifndef DA_DIFF_MACROS_STRUCTS
#define DA_DIFF_MACROS_STRUCTS 1





#define XVAR 0
#define YVAR 1
#define ZVAR 2

#define DIAG 0
#define DIAG_L 1
#define DIAG_U 2
#define DIAG_U2 3
#define TRUE 1
#define FALSE 0

struct Diff_coeff { double *ax; double *ay; double *az; ; double *axy; double *ayz; double *axz; double *axyz;};
struct Fct_param {double V_max; double K_m; double k_unspecific; double k_on_D1; double k_on_D2; double k_off_D1; double k_off_D2; double C_D1_max; double C_D2_max;};     //depends on the chosen function

#endif
