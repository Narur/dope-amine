/*
 * 
 * 
 * This file DA_Diff_Solver_fct_1_comp.h is the header file containing all functions 
 * that are used by the 1 component solver of the diffusion code DOPE-AMINE only.
 * 
 * 
Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


 * 

 * 
 * 
 * */



#ifndef DA_DIFF_SOLVER_FCT_1_COMP
#define DA_DIFF_SOLVER_FCT_1_COMP 1

int Create_RHS_w_Fct(int N_comp,int Nx_calc,int Ny_calc,int Nz_calc,double **** RHS_1, double **** RHS_reorg,double **** Conc_now,double **** fct_grid,double **** Jacobi,double dt);
//Creates the Righthandside for the Function parts in the iterations. The first iteration uses the fct grid directly, but following iterations use this fct.

int Calc_fct_grid(int N_comp, int Nx, int Ny, int Nz,double **** Conc_now, double **** fct_grid,struct Fct_param * F_param, double **** V_spatial);
//Calculates the fct (the reaction part e.g. uptake and interactions of the system) for each cell directly 

int Copy_DA_to_RK_scratch(double ****A, double ****B,int time_id, int Nx, int Ny, int Nz, int offset);
//Copies the DA concentration to the RK matrix that will use it later. time_id is either 0,1,2 for t=n,t=n+0.5 ot t=n+1 respectively. Offset is used in case Conc array is with periodic boundaries

int Do_RK4_for_recep_activation(double ****DA_timepoints,double ****Conc_now,double dt,int N_comp_out, int Nx, int Ny, int Nz,struct Fct_param* F_param);
//Calculates the receptor activation from the DA concentration using a simple Runge-Kutta 4th order method. This approximation assumes that the Receptors are not influencing the DA concentration significantly.

int Fill_Diff_Mat_iter(int i,int j,int n,int N,double ** Diff_Mat_iter,double **** Conc_now,double**** Jacobi,double **r,double dt);
//Fill the diffusion matrix during the iteration, using Jacobi terms etc. 

int Fill_Jacobi(int N_comp,int Nx,int Ny,int Nz,double **** Conc_now,double **** Jacobi,struct Fct_param * F_param,double ****V_spatial);
//Fill the Jacobi matrix. This depends on the interactions in the system.

int Advance_t(int N_comp,int Nx_calc,int Ny_calc,int Nz_calc,double ****RHS_1,double **** RHS_reorg, struct D_system * Dsys,double **** fct_grid,double ****Conc_now,double ****Conc_next,double ****Jacobi, double ** Diff_Mat_iter,double dt,struct Fct_param * F_param,double ****V_spatial);
//Steps the state of the simulation one step dt into the future. 

#endif
