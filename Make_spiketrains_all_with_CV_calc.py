"""
This script is part of the DOPE-AMINE neurotransmittr diffusion code 

Usage:
This script will generate spiketrains to be used as input for the Dopaminergic Axons in the simulation.
It will use the parameters set in the Default.ini however all parameters can be passed as options to overwrite parameters
in the initialization file. This script will be called from the Setup simulation script (so you usually shouldn't need to call this
on your own). If you want to use a spiketrain generated from another source provide the option for that in the Setup_simulation script
so that the spiketrain generation will be skipped.



Copyright (C) 2018 Lars Hunger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.




"""












import numpy as np
import math as m
import sys
from ast import literal_eval as make_tuple
from configobj import ConfigObj
from docopt import docopt

# ALL times are in units of second









def make_event_times_irregular_low_burst(shape,rate,t_max):

#here we make an irregular SNC spiketrain that is low bursting ... For ease of modeling we decided that low-bursting means no-bursting here since
#depending on the analysis the spikes in bursts are around 10% or below.
#creates a list with arriving event times We use a gamma distribution for the event times to try and model the resting time.

    t=0
    event_times=[]
    beta=1.0/(rate*shape)

    while True:
        t=t+np.random.gamma(shape,beta)
        if t<t_max:
            event_times.append(t)
        else:
            break
    return np.asarray(event_times)



def make_event_times_regular_low_burst(width,rate,t_max):

#here we make an regular SNC spiketrain that is low bursting ... For ease of modeling we decided that low-bursting means no-bursting here since
#depending on the analysis the spikes in bursts are around 10% or below.
#creates a list with arriving event times we use a normal distribution around an average value for modeling spike times
    event_times=[]
    mean_ISI=1.0/rate

    #here we pull the first spiketime and then have it spiking until
    t=np.random.uniform(low=0.0,high=mean_ISI*5.0)  #uniformly pull the first time by 5* the mean_ISI to make the neurons uncorrelated
    while True:     #here we spike until we have a time larger than 5*mean_ISI ... then we are in the interval and start the spiking... so that we don't have a long pause at the beginning of the spiketrain for most trains
        if t<mean_ISI*5.0:
            t=t+np.random.normal(loc=mean_ISI-0.002, scale=width)
        else:
            break

    t=t-5.0*mean_ISI


    event_times.append(t)



    while True:
        t=t+0.002+np.random.normal(loc=mean_ISI-0.002, scale=width)
        if t<t_max:
            event_times.append(t)
        else:
            break

    return np.asarray(event_times)


def make_event_times_high_burst(shape,rate,rate_bursts,intra_burst_rate,target_spikes_in_burst,t_max):

#here we make an irregular SNC spiketrain that is low bursting ... For ease of modeling we decided that low-bursting means no-bursting here since
#depending on the analysis the spikes in bursts are around 10% or below.
#creates a list with arriving event times We use a gamma distribution for the event times to try and model the resting time.
    spikes_all=0
    spikes_in_bursts=0
    t=0
    t_burst=0
    event_times=[]

    ISI_burst=1.0/intra_burst_rate
    mod_rate=0.9*(rate-(rate_bursts*target_spikes_in_burst)) #modified rate because we have to reduce the non burst firing to get a final accumulated firing rate of rate. Some spikes are being jumped so the modified rate has to be slightly higher (multiplicator with 1.1)
    beta=1.0/(mod_rate*shape)

    lambd_rate_bursts=1.0/rate_bursts
    t_burst=np.random.exponential(lambd_rate_bursts)
    t=np.random.gamma(shape,beta)
    while min(t,t_burst)>t_max:
        #print "Hey"
        t_burst=np.random.exponential(lambd_rate_bursts)
        t=np.random.gamma(shape,beta)



    while True:
        if t_burst<t:    #burst happens
            t_spike=t_burst
            nr_spikes_in_burst=0
            while nr_spikes_in_burst<2:
                #nr_spikes_in_burst=np.around(np.random.normal(loc=2.7, scale=0.45)).astype('int') #the actual distribution is std=0.5 ... but the rounding makes the distribution wider ... so here we choose 0.45
                nr_spikes_in_burst=np.random.poisson(target_spikes_in_burst)

            for i in range(nr_spikes_in_burst):
                event_times.append(t_spike)
                spikes_in_bursts+=1
                spikes_all+=1
                t_old=t_spike
                #print ISI_burst,intra_burst_rate
                t_spike=t_spike+np.random.normal(loc=ISI_burst,scale=(ISI_burst/4.0)) #ALL intraburst ISI's are like clockwork

            t_burst=t_burst+np.random.exponential(lambd_rate_bursts) #when is the next burst going to be
            #if t_old>t: #in case the burst overtakes the next normal spiketime
            t=t_spike+np.random.gamma(shape,beta)



        if t<t_burst:    #normal spike happens
            event_times.append(t)
            spikes_all+=1
            t=t+np.random.gamma(shape,beta)


        if min(t,t_burst)>t_max:
            break;


    #print spikes_all,spikes_in_bursts, spikes_in_bursts/float(spikes_all)

    while event_times[-1]>t_max:
        del event_times[-1]

    return spikes_in_bursts/float(spikes_all),np.asarray(event_times)


def autocorrelation(data,bin_size,t_max):
    #Calculates the autocorrelogram for a spiketrain. It uses a given bin_size, and divides the interval 0..t_max into bins of this size
    nr_bins=np.floor(t_max/bin_size+0.5)

    Signal=np.zeros(nr_bins)
    Signal[np.floor(data/bin_size).astype('int')] = 1.
    Autocorr=np.correlate(Signal,Signal,mode='full')


    return np.asarray(Autocorr)


def one_burst(offset,max_burst_jitter,ISI,target_spikes_in_burst,nr):
    #Makes nr bursts each starts between offset and offset+with max_burst_jitter with an ISI provided in a list and target_spikes_in_burst also
    #provided as a list
    bursts=[] #
    
    for i in range(nr):
            t=offset+np.random.uniform(low=0.0,high=max_burst_jitter*0.001) #max_burst jitter is in ms but everything else is in s
            event_times=[]
            nr_spikes_in_burst=0

            while nr_spikes_in_burst<2:
                nr_spikes_in_burst=np.random.poisson(target_spikes_in_burst[i]) #Pick nr_spikes from a poisson distribution, length at least 2

            #creates the burst
            for j in range(nr_spikes_in_burst):
                t=t+np.random.normal(loc=ISI[i],scale=(ISI[i]/4.0)) #ALL intraburst ISI's are fairly regular
                event_times.append(t)

            #append the burst to the list of bursts
            bursts.append(event_times)
    
    return np.asarray(bursts)
    #now we actually make the bursts

def add_all_reward_events(trains,nr_irregular,nr_regular,nr_bursting,config):
    events=make_tuple(config['spiketrain_parameters']['events'])
    if np.ndim(events)<2:
        events=(events,)    #If we don't do this the indexing doesn't work out for too short tuples... yes the spike generation is a mess :(
        
    
    event_mode=config['spiketrain_parameters']['events_mode']    #event mode is same or random... controls if in bursts and bursts with pause the same neurons burst or the percentages are chosen every time at random (random not implemented yet)
    event_margin=config['spiketrain_parameters']['event_margin']    #the margin of how many ms around the event all spikes will be removed. if the last spike in a rewarded burst happens at 1134 ms and the first at 900ms then bevor the burst is inserted all spikes between (900-event_margin[0]) and (1134+event_margin[1]) will be removed  
    event_margin=[x/1000.0 for x in event_margin]    #all units used in the burst generator are in s, so we have to transform the margins into s too
    
    reward_bursting_fraction=config['spiketrain_parameters']['reward_bursting_fraction']
    reward_spikes_in_burst=config['spiketrain_parameters']['reward_spikes_in_burst']
    reward_intraburst_freq=config['spiketrain_parameters']['reward_intraburst_freq']
    max_event_jitter=config['spiketrain_parameters']['max_event_jitter']

    bwp_spikes_in_burst=config['spiketrain_parameters']['bwp_spikes_in_burst']  #since in tobler 2003 they say the "false alarms" bursts are "weak" we use, for the sake of simplicity, the values of the bursts for the bursting neurons here
    bwp_intraburst_freq=config['spiketrain_parameters']['bwp_intraburst_freq']
    bwp_bursting_fraction=config['spiketrain_parameters']['bwp_frac_burst_burstpause_pause'][0]                              
    bwp_burst_pause_fraction=config['spiketrain_parameters']['bwp_frac_burst_burstpause_pause'][1]
    bwp_pause_fraction=config['spiketrain_parameters']['bwp_frac_burst_burstpause_pause'][2]
    bwp_pause_duration=config['spiketrain_parameters']['bwp_pause_duration']
    
    print bwp_bursting_fraction,bwp_burst_pause_fraction,bwp_pause_fraction


    bursting_ids=None
    bwp_ids=None
    bwp_bursting_ids=None
    bwp_burst_pause_ids=None
    bwp_pause_ids=None
    
    
    for j in range(len(events)):
        if events[j][0]=='b':   #add a burst if the event is a burst
            if event_mode=='same':
                if not bursting_ids:  #when no list of neurons has been made yet
                    #Figure out how many neurons of each type are bursting... rounded mathematically
                    nr_bursting_irr=int(round(nr_irregular*reward_bursting_fraction,0))
                    nr_bursting_reg=int(round(nr_regular*reward_bursting_fraction,0))
                    nr_bursting_bur=int(round(nr_bursting*reward_bursting_fraction,0))
                    #choose as bursting neurons always the first neurons... since it doesn't matter here but makes it easier to work with it later
                    bursting_irr_ids=range(0,nr_bursting_irr)   #IDS of irregular neurons, aka locations in spiketrains
                    bursting_reg_ids=range(nr_irregular,nr_irregular+nr_bursting_reg)
                    bursting_bur_ids=range(nr_irregular+nr_regular,nr_irregular+nr_regular+nr_bursting_bur)
                    bursting_ids=bursting_irr_ids+bursting_reg_ids+bursting_bur_ids

                    #make the ISI, target spikes in burst lists for the neurons ... since they are neuron specific
                    ISI_bursting=[]
                    event_target_spikes_in_burst=[]
                    for i in range(len(bursting_ids)):
                        #intraburst frequency for every spiking neuron
                        temp_intraburst_freq=-1.0
                        while temp_intraburst_freq<10.0:
                            temp_intraburst_freq=np.random.normal(loc=reward_intraburst_freq[0],scale=reward_intraburst_freq[1])
                        ISI_temp=1.0/temp_intraburst_freq
                        ISI_bursting.append(ISI_temp)
                        
                        #spikes in burst for every spiking neuron
                        temp_spikes_in_burst=-1.0
                        while temp_spikes_in_burst<1:
                                temp_spikes_in_burst=np.random.normal(loc=reward_spikes_in_burst[0],scale=reward_spikes_in_burst[1])
                        event_target_spikes_in_burst.append(temp_spikes_in_burst)


            
            else:
                print "SORRY NO OTHER MODE THEN THE SAME BURST MODE IS IMPLEMENTED BECAUSE OF SUPREME LAZYNESS"
                sys.exit(1)


            burst_spike_times=one_burst(events[j][1],max_event_jitter,ISI_bursting,event_target_spikes_in_burst,len(bursting_ids))  #add one burst to all active neurons at offset given by events[i][1] which will have the burst time in seconds
            #print ISI_bursting
            #now we have the burst spike times in an array, now we will cut all spikes from the burst times of the spiketrain and replace them with the burst
            for i in range(len(bursting_ids)):
                #print burst_spike_times[i]
                trains[bursting_ids[i]]= [x for x in trains[bursting_ids[i]] if not((np.min(burst_spike_times[i])-event_margin[0])<=x<=(np.max(burst_spike_times[i])+event_margin[1]))] #remove all times accompassed by the burst and 10 ms around it from the spiketrain
                trains[bursting_ids[i]]=np.insert(trains[bursting_ids[i]],np.argmax(trains[bursting_ids[i]]>(np.max(burst_spike_times[i])+event_margin[1])),burst_spike_times[i])    #insert the burst
                    





        elif events[j][0]=='bwp': #add aburst with pause in case of burst with pause
             if event_mode=='same':
                 if not bwp_ids:  #when no list of neurons has been made yet
                    # neuron types for bwp are bursting, bursting followed by depression and just depression (like in Tobler2003 Coding of predicted reward ommission etc)
                    # since in the Tobler paper, they say the activation is "weak" for not rewarded stimuli, we use the parameters for the normal burst
                    # instead of the parameters for the rewarding bursts here  
                    #Figure out how many neurons of each type are bursting/bursting and pausing and/just pausing... rounded mathematically
                    nr_bwp_bursting_irr=int(round(nr_irregular*bwp_bursting_fraction,0))
                    nr_bwp_bursting_reg=int(round(nr_regular*bwp_bursting_fraction,0))
                    nr_bwp_bursting_bur=int(round(nr_bursting*bwp_bursting_fraction,0))
                    
                    nr_bwp_burst_pause_irr=int(round(nr_irregular*bwp_burst_pause_fraction,0))
                    nr_bwp_burst_pause_reg=int(round(nr_regular*bwp_burst_pause_fraction,0))
                    nr_bwp_burst_pause_bur=int(round(nr_bursting*bwp_burst_pause_fraction,0))
                    
                    nr_bwp_pause_irr=int(round(nr_irregular*bwp_pause_fraction,0))
                    nr_bwp_pause_reg=int(round(nr_regular*bwp_pause_fraction,0))
                    nr_bwp_pause_bur=int(round(nr_bursting*bwp_pause_fraction,0))
                    
                    #in case the rounding leads to more neurons with bwp pattern than overall irregular neurons we reduce the pausing neurons to fit
                    if ((nr_bwp_bursting_irr+nr_bwp_burst_pause_irr+nr_bwp_pause_irr)>nr_irregular):
                        nr_bwp_pause_irr=nr_irregular-(nr_bwp_burst_irr+nr_bwp_burst_pause_irr)
                        
                    #in case the rounding leads to more neurons with bwp pattern than overall regular neurons we reduce the pausing neurons to fit
                    if ((nr_bwp_bursting_reg+nr_bwp_burst_pause_reg+nr_bwp_pause_reg)>nr_regular):
                        nr_bwp_pause_reg=nr_regular-(nr_bwp_burst_reg+nr_bwp_burst_pause_reg)
                        
                    #in case the rounding leads to more neurons with bwp pattern than overall (non-event)-bursting neurons we reduce the pausing neurons to fit
                    if ((nr_bwp_bursting_bur+nr_bwp_burst_pause_bur+nr_bwp_pause_bur)>nr_bursting):
                        nr_bwp_pause_bur=nr_bursting-(nr_bwp_burst_bur+nr_bwp_burst_pause_bur)
                    
                    
                    #choose as bursting neurons always the first neurons... since it doesn't matter here but makes it easier to work with it later
                    bwp_bursting_irr_ids=range(0,nr_bwp_bursting_irr)   #IDS of irregular neurons, aka locations in spiketrains
                    bwp_bursting_reg_ids=range(nr_irregular,nr_irregular+nr_bwp_bursting_reg)
                    bwp_bursting_bur_ids=range(nr_irregular+nr_regular,nr_irregular+nr_regular+nr_bwp_bursting_bur)
                    bwp_bursting_ids=bwp_bursting_irr_ids+bwp_bursting_reg_ids+bwp_bursting_bur_ids
            
                    #choose as bursting neurons always the first neurons... since it doesn't matter here but makes it easier to work with it later
                    bwp_burst_pause_irr_ids=range(nr_bwp_bursting_irr,nr_bwp_bursting_irr+nr_bwp_burst_pause_irr)   #IDS of irregular neurons, aka locations in spiketrains
                    bwp_burst_pause_reg_ids=range(nr_irregular+nr_bwp_bursting_reg,nr_irregular+nr_bwp_bursting_reg+nr_bwp_burst_pause_reg)
                    bwp_burst_pause_bur_ids=range(nr_irregular+nr_regular+nr_bwp_bursting_bur,nr_irregular+nr_regular+nr_bwp_bursting_bur+nr_bwp_burst_pause_bur)
                    bwp_burst_pause_ids=bwp_burst_pause_irr_ids+bwp_burst_pause_reg_ids+bwp_burst_pause_bur_ids
                    
                    #choose as bursting neurons always the first neurons... since it doesn't matter here but makes it easier to work with it later
                    bwp_pause_irr_ids=range(nr_bwp_bursting_irr+nr_bwp_burst_pause_irr,nr_bwp_bursting_irr+nr_bwp_burst_pause_irr+nr_bwp_pause_irr)   #IDS of irregular neurons with pause, aka locations in spiketrains
                    bwp_pause_reg_ids=range(nr_irregular+nr_bwp_bursting_reg+nr_bwp_burst_pause_reg,nr_irregular+nr_bwp_bursting_reg+nr_bwp_burst_pause_reg+nr_bwp_pause_reg)
                    bwp_pause_bur_ids=range(nr_irregular+nr_regular+nr_bwp_bursting_bur+nr_bwp_burst_pause_bur,nr_irregular+nr_regular+nr_bwp_bursting_bur+nr_bwp_burst_pause_bur+nr_bwp_pause_bur)
                    bwp_pause_ids=bwp_pause_irr_ids+bwp_pause_reg_ids+bwp_pause_bur_ids
                    
                    bwp_ids=bwp_bursting_ids+bwp_burst_pause_ids+bwp_pause_ids #mostly this is done to make a bwp_ids so that it remembers next time that this has been gernerated
                    
                    #make the ISI, target spikes in burst lists for the neurons that just burst at the event ... since they are neuron specific
                    bwp_ISI_bursting=[]
                    bwp_burst_target_spikes_in_burst=[]
                    for i in range(len(bwp_bursting_ids)):
                        #intraburst frequency for every spiking neuron
                        temp_intraburst_freq=-1.0
                        while temp_intraburst_freq<10.0:
                            temp_intraburst_freq=np.random.normal(loc=bwp_intraburst_freq[0],scale=bwp_intraburst_freq[1])
                        ISI_temp=1.0/temp_intraburst_freq
                        bwp_ISI_bursting.append(ISI_temp)
                        
                        #spikes in burst for every spiking neuron
                        temp_spikes_in_burst=-1.0
                        while temp_spikes_in_burst<1:
                                temp_spikes_in_burst=np.random.normal(loc=bwp_spikes_in_burst[0],scale=bwp_spikes_in_burst[1])
                        bwp_burst_target_spikes_in_burst.append(temp_spikes_in_burst)
                        
                    #make the ISI, target spikes in burst lists for the neurons that burst at the event followed by a pause... since they are neuron specific
                    bwp_ISI_burst_pause=[]
                    bwp_burst_pause_target_spikes_in_burst=[]
                    for i in range(len(bwp_burst_pause_ids)):
                        #intraburst frequency for every spiking neuron
                        temp_intraburst_freq=-1.0
                        while temp_intraburst_freq<10.0:
                            temp_intraburst_freq=np.random.normal(loc=bwp_intraburst_freq[0],scale=bwp_intraburst_freq[1])
                        ISI_temp=1.0/temp_intraburst_freq
                        bwp_ISI_burst_pause.append(ISI_temp)
                        
                        #spikes in burst for every spiking neuron
                        temp_spikes_in_burst=-1.0
                        while temp_spikes_in_burst<1:
                                temp_spikes_in_burst=np.random.normal(loc=bwp_spikes_in_burst[0],scale=bwp_spikes_in_burst[1])
                        bwp_burst_pause_target_spikes_in_burst.append(temp_spikes_in_burst)
                        
                    
             #add bursts to spiketrains that burst in the bwp pattern
             bwp_burst_spike_times=one_burst(events[j][1],max_event_jitter,bwp_ISI_bursting,bwp_burst_target_spikes_in_burst,len(bwp_bursting_ids))  #add one burst to all active neurons at offset given by events[i][1] which will have the burst time in seconds
             #now we have the burst spike times in an array, now we will cut all spikes from the burst times of the spiketrain and replace them with the burst
             for i in range(len(bwp_bursting_ids)):
                 #print bwp_burst_spike_times[i]
                 trains[bwp_bursting_ids[i]]= [x for x in trains[bwp_bursting_ids[i]] if not((np.min(bwp_burst_spike_times[i])-event_margin[0])<=x<=(np.max(bwp_burst_spike_times[i])+event_margin[1]))] #remove all times accompassed by the burst and 10 ms around it from the spiketrain
                 trains[bwp_bursting_ids[i]]=np.insert(trains[bwp_bursting_ids[i]],np.argmax(trains[bwp_bursting_ids[i]]>(np.max(bwp_burst_spike_times[i])+event_margin[1])),bwp_burst_spike_times[i])    #insert the burst
                    
             #add burst_pause to spiketrains that burst_pause in the bwp pattern
             bwp_burst_pause_spike_times=one_burst(events[j][1],max_event_jitter,bwp_ISI_burst_pause,bwp_burst_pause_target_spikes_in_burst,len(bwp_burst_pause_ids))  #add one burst to all active neurons at offset given by events[i][1] which will have the burst time in seconds
             #now we have the burst spike times in an array, now we will cut all spikes from the burst times of the spiketrain and replace them with the burst
             for i in range(len(bwp_burst_pause_ids)):
                 #print bwp_burst_pause_spike_times[i]
                 #Remove the spikes in the positions where the burst is supposed to go
                 trains[bwp_burst_pause_ids[i]]= [x for x in trains[bwp_burst_pause_ids[i]] if not((np.min(bwp_burst_pause_spike_times[i])-event_margin[0])<=x<=(np.max(bwp_burst_pause_spike_times[i])+event_margin[1]))] #remove all times accompassed by the burst and 10 ms around it from the spiketrain
                 #Insert the burst
                 trains[bwp_burst_pause_ids[i]]=np.insert(trains[bwp_burst_pause_ids[i]],np.argmax(trains[bwp_burst_pause_ids[i]]>(np.max(bwp_burst_pause_spike_times[i])+event_margin[1])),bwp_burst_pause_spike_times[i])    #insert the burst
                 #Remove all the spikes after the burst for the duration of the pause!
                 pause_duration=-1
                 while pause_duration<0:
                        pause_duration=0.001*np.random.normal(loc=bwp_pause_duration[0],scale=bwp_pause_duration[1])  #also have to transform pause duration from ms to s 
                 trains[bwp_burst_pause_ids[i]]= [x for x in trains[bwp_burst_pause_ids[i]] if not((np.max(bwp_burst_pause_spike_times[i]))<x<=(np.max(bwp_burst_pause_spike_times[i])+pause_duration+event_margin[1]))] #remove all times accompassed by the pause from the spiketrain
             
             for i in range(len(bwp_pause_ids)):
                 #Remove all the spikes for the duration of the pause!
                 #The pause length is normal distributed >0
                 pause_duration=-1.0
                 while pause_duration<0:
                        pause_duration=0.001*np.random.normal(loc=bwp_pause_duration[0],scale=bwp_pause_duration[1])    #also have to transform pause duration from ms to s 
                 #the pause begins at the event time+jitter
                 pause_begin=events[j][1]+np.random.uniform(low=0.0,high=max_event_jitter*0.001)
                 pause_end=pause_begin+pause_duration
                 #print pause_begin, pause_end, pause_duration, bwp_pause_duration
                 #Here we remove all the spikes between beginning and end of pause
                 trains[bwp_pause_ids[i]]= [x for x in trains[bwp_pause_ids[i]] if not ((pause_begin-event_margin[0])<=x<=(pause_end+event_margin[1]))] #remove all times accompassed by the pause from the spiketrain
             

             #print bwp_pause_ids,bwp_bursting_ids,bwp_burst_pause_ids
             #print bwp_pause_irr_ids,bwp_pause_reg_ids,bwp_pause_bur_ids 
             #print bwp_burst_pause_irr_ids,bwp_burst_pause_reg_ids,bwp_burst_pause_bur_ids 
             #print bwp_bursting_irr_ids,bwp_bursting_reg_ids,bwp_bursting_bur_ids 

            
            
            
        else:
            print "SORRY THE BURST WITH PAUSE MODE IS NOT YET IMPLEMENTED BECAUSE OF SUPREME LAZYNESS"
            sys.exit(1)

    return bursting_ids,bwp_bursting_ids,bwp_burst_pause_ids,bwp_pause_ids

def main(config=None):
    #Here we will make the spiketrains that are usually used for our simulations


    if config==None:
        print 'No Config found, Please Start the Spiketrain Generator through the Setup_simulation script or provide a valid configObj.'
        sys.exit(1)

    if (config['spiketrain_parameters']['seed_st']!='none'):
        np.random.seed(config['spiketrain_parameters']['seed_st'])



    #times are in seconds
    rate_regular=config['spiketrain_parameters']['rate_regular']    #mean rate and width of rate distribution
    CV_regular=config['spiketrain_parameters']['CV_regular']    #avg CV for regular neurons and width of CV distribution -> made it more regular to overestimate effects


    rate_irregular=config['spiketrain_parameters']['rate_irregular']    #mean and width
    shape_irregular=config['spiketrain_parameters']['shape_irregular']   #low and high value of shape parameter. This parameter is uniformly distributed


    rate_bursting=config['spiketrain_parameters']['rate_bursting'] #this is chosen so that the firing rate is the same on average... because of the different generation
    shape_bursting=config['spiketrain_parameters']['shape_bursting']    #low and high value of shape parameter. This parameter is uniformly distributed
    rate_of_bursts=config['spiketrain_parameters']['rate_of_bursts']  #rate of the bursts and width
    spikes_in_burst=config['spiketrain_parameters']['spikes_in_burst']
    intraburst_freq=config['spiketrain_parameters']['intraburst_freq']


    irregular_frac=config['spiketrain_parameters']['frac_irr_reg_bur'][0]
    regular_frac=config['spiketrain_parameters']['frac_irr_reg_bur'][1]
    bursting_frac=config['spiketrain_parameters']['frac_irr_reg_bur'][2]
    nr_trains=config['general_parameters']['nr_axons']
    t_max=config['general_parameters']['max_sim_time'] #t_max in seconds

    spiketrain_fn=config['spiketrain_parameters']['spiketrain_fn']
    axons_used_fn=config['general_parameters']['axons_used_fn']

    #Fraction for regular and bursting will be rounded down, while fraction for irregular will be rounded up, so that we have a given number of trains
    nr_regular_trains=int(nr_trains*regular_frac)
    nr_bursting_trains=int(nr_trains*bursting_frac)
    nr_irregular_trains=nr_trains-(nr_regular_trains+nr_bursting_trains)#int(nr_trains*irregular_frac)

    spiketrains=[]



    for i in range(0,nr_irregular_trains):   #irregular neurons
        rate=-1.0
        while rate<0:
            rate=np.random.normal(loc=rate_irregular[0], scale=rate_irregular[1])

        shape=np.random.uniform(low=shape_irregular[0],high=shape_irregular[1])

        spikes=make_event_times_irregular_low_burst(shape,rate,t_max)
        spiketrains.append(spikes)




    for i in range(nr_irregular_trains,nr_irregular_trains+nr_regular_trains):  #regular neurons

        target_CV=-1.0
        while target_CV<0:
            target_CV=np.random.normal(loc=CV_regular[0], scale=CV_regular[1]) #pull the CV for this realization of the spiketrain

        rate=-1.0
        while rate<0:
            rate=np.random.normal(loc=rate_regular[0], scale=rate_regular[1])

        width=(target_CV*(1.0/rate))    #width of the normal distribution of the ISI dist.

        spikes=make_event_times_regular_low_burst(width,rate,t_max)
        spiketrains.append(spikes)





    burst_per_cent_all=np.zeros(nr_bursting_trains)
    for i in range(nr_irregular_trains+nr_regular_trains,nr_trains):   #bursting neurons
        mod_rate=-1.0
        while mod_rate<0:
            rate=-1.0
            while rate<0:
                rate=np.random.normal(loc=rate_bursting[0], scale=1.1*rate_bursting[1]) #to match the std of the rate this needs to be increased here since the rate (not in bursts) will be reduced

            current_rate_of_bursts=-1.0
            while current_rate_of_bursts<0:
                current_rate_of_bursts=np.random.normal(loc=rate_of_bursts[0],scale=rate_of_bursts[1])

            current_spikes_in_burst=-1.0
            while current_spikes_in_burst<1:
                current_spikes_in_burst=np.random.normal(loc=spikes_in_burst[0],scale=spikes_in_burst[1])
            mod_rate=1.1*(rate-(current_rate_of_bursts*current_spikes_in_burst))

        shape=np.random.uniform(low=shape_bursting[0],high=shape_bursting[1])

        current_intraburst_freq=-1.0
        while current_intraburst_freq<10.0:
            current_intraburst_freq=np.random.normal(loc=intraburst_freq[0],scale=intraburst_freq[1])


        #print mod_rate,current_rate_of_bursts
        burst_per_cent,spikes=make_event_times_high_burst(shape,rate,current_rate_of_bursts,current_intraburst_freq,current_spikes_in_burst,t_max)
        burst_per_cent_all[i-(nr_irregular_trains+nr_regular_trains)]=burst_per_cent

        spiketrains.append(spikes)



    if (nr_bursting_trains>0):
        burst_per_cent_mean=np.mean(burst_per_cent_all)
        burst_per_cent_std=np.std(burst_per_cent_all)
    #print CV_mean,CV_std,burst_per_cent_mean,burst_per_cent_std,rate_mean,rate_std


    #Now we either add bursts or burst with_pause patterns
    if config['spiketrain_parameters']['events']!='no_events':
        #print config['spiketrain_parameters']['reward_events']
        #events=make_tuple(config['spiketrain_parameters']['reward_events'])
        #print len(events)
        bursting_ids, bwp_bursting_ids,bwp_burst_pause_ids,bwp_pause_ids=add_all_reward_events(spiketrains,nr_irregular_trains,nr_regular_trains,nr_bursting_trains,config)




    f = open(spiketrain_fn,"w") #Makes a new file spiketrain fn
    max_length_one_train=0  #this is for figuring out which spiketrain has the most spikes, this will be needed by the diffusion code for reading the spiketrain
    #base analysis (writeout is done later so that the files can be read easier by the Diffusion code)
    CV_ireg_all=np.zeros(nr_irregular_trains)
    rate_ireg_all=np.zeros(nr_irregular_trains)
    for i in range(nr_irregular_trains):
        if len(spiketrains[i])>1:  #checks if this spiketrains actually has more than one spike
            ISI_list=np.diff(spiketrains[i])
            mean_interval=np.mean(ISI_list)
            std_interval=np.std(ISI_list)
            CV=std_interval/(mean_interval)
            CV_ireg_all[i]=CV
            rate_ireg_all[i]=len(spiketrains[i])/float(t_max)
            if (len(spiketrains[i])>max_length_one_train):
                max_length_one_train=len(spiketrains[i])



    #base analysis+writeout regular neurons
    CV_reg_all=np.zeros(nr_regular_trains)
    rate_reg_all=np.zeros(nr_regular_trains)
    for i in range(nr_irregular_trains,nr_irregular_trains+nr_regular_trains):
        if len(spiketrains[i])>1:
            ISI_list=np.diff(spiketrains[i])
            mean_interval=np.mean(ISI_list)
            std_interval=np.std(ISI_list)
            CV=std_interval/(mean_interval)
            CV_reg_all[i-nr_irregular_trains]=CV
            rate_reg_all[i-nr_irregular_trains]=len(spiketrains[i])/float(t_max)
            if (len(spiketrains[i])>max_length_one_train):
                max_length_one_train=len(spiketrains[i])



    #base analysis+writeout bursting neurons
    CV_bur_all=np.zeros(nr_bursting_trains)
    rate_bur_all=np.zeros(nr_bursting_trains)
    for i in range(nr_irregular_trains+nr_regular_trains,nr_trains):

        if len(spiketrains[i])>1:
            ISI_list=np.diff(spiketrains[i])
            mean_interval=np.mean(ISI_list)
            std_interval=np.std(ISI_list)
            CV=std_interval/(mean_interval)
            CV_bur_all[i-(nr_regular_trains+nr_irregular_trains)]=CV
            rate_bur_all[i-(nr_regular_trains+nr_irregular_trains)]=len(spiketrains[i])/float(t_max)
            if (len(spiketrains[i])>max_length_one_train):
                max_length_one_train=len(spiketrains[i])
        



    if (nr_irregular_trains>0):
        s="irregular: "+"0 "+str(nr_irregular_trains-1)+" CV:"+str(np.mean(CV_ireg_all))+","+str(np.std(CV_ireg_all))+" rate: "+str(np.mean(rate_ireg_all))+","+str(np.std(rate_ireg_all))+"\n"
    else:
        s="no irregular trains \n"
    f.write(s)
    
    if (nr_regular_trains>0):
        s="regular: "+str(nr_irregular_trains)+" "+str(nr_irregular_trains+nr_regular_trains-1)+" CV:"+str(np.mean(CV_reg_all))+","+str(np.std(CV_reg_all))+" rate: "+str(np.mean(rate_reg_all))+","+str(np.std(rate_reg_all))+"\n"
    else:
        s="no regular trains \n"
    
    f.write(s)
    
    if (nr_bursting_trains>0):
        s="bursting: "+str(nr_irregular_trains+nr_regular_trains)+" "+str(nr_trains-1)+" CV:"+str(np.mean(CV_bur_all))+","+str(np.std(CV_bur_all))+" rate: "+str(np.mean(rate_bur_all))+","+str(np.std(rate_bur_all))+" burst%:"+str(burst_per_cent_mean)+","+str(burst_per_cent_std)+"\n"
    else:
        s="no bursting trains \n"
    f.write(s)
    
    #When there are events we print the neurons that are firing
    if config['spiketrain_parameters']['events']!='no_events':
        s="The used event string is: "+config['spiketrain_parameters']['events']+"\n"
        f.write(s)
        s="Bursting neuron IDs for bursting event: "+str(bursting_ids)+"\n"
        f.write(s)
        s="Bursting neuron IDs for bwp event: "+str(bwp_bursting_ids)+"\n"
        f.write(s)
        s="Burst-pause neuron IDs for bwp event: "+str(bwp_burst_pause_ids)+"\n"
        f.write(s)
        s="Pausing neuron IDs for bwp event: "+str(bwp_pause_ids)+"\n"
        f.write(s)
    else:
        s="The used event string is: "+config['spiketrain_parameters']['events']+"\n"
        f.write(s)
        s="Bursting neuron IDs for bursting event: [] \n"
        f.write(s)
        s="Bursting neuron IDs for bwp event: [] \n"
        f.write(s)
        s="Burst-pause neuron IDs for bwp event: [] \n"
        f.write(s)
        s="Pausing neuron IDs for bwp event: [] \n"
        f.write(s)
     
    s="Preliminary Information ends here! Now the actual spiketrain data starts! \n" 
    f.write(s) 
    
    s=str(len(spiketrains))+" "+str(max_length_one_train)+"\n"   
    f.write(s)
    
    
    
    
    
    
    
    
    st_ids=""    
    for i in range(nr_irregular_trains):
        st_ids=st_ids+str(i)+" "
        for j in range(len(spiketrains[i])):
            s=str(i)+" "+str(spiketrains[i][j]*1000.0)+"\n"
            f.write(s)

    for i in range(nr_irregular_trains,nr_irregular_trains+nr_regular_trains):
        st_ids=st_ids+str(i)+" "
        for j in range(len(spiketrains[i])):
            s=str(i)+" "+str(spiketrains[i][j]*1000.0)+"\n"
            f.write(s)
            
    for i in range(nr_irregular_trains+nr_regular_trains,nr_trains):
        st_ids=st_ids+str(i)+" "
        for j in range(len(spiketrains[i])):
            s=str(i)+" "+str(spiketrains[i][j]*1000.0)+"\n"
            f.write(s)



    f.close()
    st_ids=st_ids+"\n"
    
    f = open(axons_used_fn,"w")
    s=str(len(spiketrains))+"\n"
    f.write(s)
    f.write(st_ids)
        
    f.close()


if __name__ == '__main__':
    main()
